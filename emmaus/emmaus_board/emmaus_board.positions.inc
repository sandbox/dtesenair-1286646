<?php 
// $Id:  
/*
 * @file
 * Administrative functions for the Emmaus Board of Directors module
 */
 
   
  
/*************************************************************************************************************/
/*** Table/List builder function(s) to display existing positions ********************************************/
/*************************************************************************************************************/
function emmaus_board_positions_list() { 
	//Set the tag line
	$output = '<h3><p>Current position definitions for the ' . variable_get('emmaus_community_name') . ' Board of Directors.</p></h3>';
  $output .= '<p>From this page, you can add, delete, or modify a board position.</p>';
	//Define the table header
	$header = array(
    array(
      'data' => 'Edit', 
      'width'=>'40' 
      ),
    array(
      'data' => 'Weight', 
      'width'=>'40',
      'sort'=>'asc', 
      ),
    array(
      'data' => 'Position Name', 
      'width'=>'25%' 
      ),
		array(
		  'data' => 'Description', 
      ),
	);
	//Query the database
  $result = db_select('emmaus_board_positions', 'p')
		->fields('p')
  	//->extend('PagerDefault')				//extend the query to include a pager
  	//->limit(20)										//Set the number of items per page
  	//->extend('TableSort')					//Extend the query to include table sorting
  	//->orderByHeader($header)			//define sort column
    ->orderBy('weight', 'asc')
    ->orderBy('position_name', 'asc')
		->execute();
	
	//Loop through the result and create an array of rows
	$rows = array();
	foreach ($result as $row) {		
		//create links
		$edit_link = '<a href="'. base_path() . 'admin/config/emmaus/board/positions/' . $row->position_id . '/edit"><img border="0" alt="Edit" title="Edit this board assignment" src="'. base_path() . drupal_get_path('module','emmaus_board') . '/images/edit.png"></a>';
		$rows[] = array($edit_link, $row->weight, $row->position_name, $row->description);
	}
	//output the table
	$tbl_vars = array(  'header'=>$header,	'rows'=>$rows,	'attributes'=>array(),	'caption' => $result->rowCount() . ' row(s) returned.',		'colgroups'=>array(),		'sticky'=>TRUE, 	'empty'=>'No rows returned from query' 	);
	$output .= theme_table($tbl_vars);
	//output a link to add a new assignment
	//$output .= '<a href="/admin/config/emmaus/board/positions/add"><img src="/'. drupal_get_path('module','emmaus_community') . '/images/add.png">Add a new board position definition</a>';

	//output the pager
	//$output .= theme('pager');
	//Return the output
	return $output;
}









/*************************************************************************************************************/
/*** Form builder function(s) to add a new position **********************************************************/
/*************************************************************************************************************/
function emmaus_board_positions_add_form() {

	// Define the form elements
  $form['emmaus_board_position_add'] = array( 
    '#type' => 'item', 			
    '#title' => t('Add new board position.'),  
    );
  $form['position_name'] = array( 
    '#type' => 'textfield',	
    '#title' => t('Position Name'),					
    '#required' => TRUE,  
    '#size' => 50, 
    '#maxlength' => 50, 	
    '#description' => t('Enter a name of this position.')   		
    );
  $form['description'] = array( 
    '#type' => 'textfield',	
      '#title' => t('Position Description'),	
      '#required' => FALSE, 
      '#size' => 50, 
      '#maxlength' => 255, 
      '#description' => t('Enter a short description of this position.')   		
      );
  $form['weight'] = array( 
    '#type' => 'weight',     
    '#title' => t('Weight'),          
    '#delta' => 10,   
    '#description' => t('Optional. In the menu, the heavier items will sink and the lighter items will be positioned nearer the top.'),
  );  
	//Submit button
  $form['submit'] = array( '#type' => 'submit', '#value' => 'Submit', );
	//Return the form
  return $form;
}
/****************************/
/*** ADD Validate handler ***/
/****************************/
function emmaus_board_positions_add_form_validate($form, &$form_state) {
}
/**************************/
/*** ADD Submit handler ***/
/**************************/
function emmaus_board_positions_add_form_submit($form, &$form_state) {
	//Generate the insert query
  db_insert('emmaus_board_positions')
      ->fields(array(
						'position_name' => $form_state['values']['position_name'],
            'description' => $form_state['values']['description'],
            'weight' => $form_state['values']['weight'],
					)
				)
      ->execute();
	//Set a confirmation message
  drupal_set_message(t('The position <em>@name</em> has been added to the Board of Directors.', array('@name' => $form_state['values']['position_name'])));
	//Redirect the form
	$form_state['redirect'] = 'admin/config/emmaus/board/positions';
}




/*************************************************************************************************************/
/*** EDIT: Form builder - function(s) to edit an existing board member entry *********************************/
/*************************************************************************************************************/
function emmaus_board_positions_edit_form($form, &$form_state, $params) {
	//Check to see if we should display the confirmation form
	if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Delete') {
		// Define form elements again, prior to final submission
		$form['position_id']		= array( '#type' => 'textfield', '#title' => t('Position ID'), 		'#default_value' => $form_state['values']['position_id']   	);
	  $form['position_name']	= array( '#type' => 'textfield', '#title' => t('Position Name'), 	'#default_value' => $form_state['values']['position_name']   );
		//Display the confirmation form
		return confirm_form($form, t('Are you sure that you want to delete the @name position from the board?', array('@name'=>$form_state['values']['position_name'])), 'admin/emmaus/board/settings/positions', 'This action can not be undone.  Please proceed with caution.', 'Remove', 'Don\'t Remove'); //Had better luck leaving off last param 'name'  
	}	//confirmation form
	
	//Query the database for the record to edit
  $select = db_select('emmaus_board_positions', 'p' );
  $select->addField( 'p', 'position_id' );
  $select->addField( 'p', 'position_name' );
  $select->addField( 'p', 'description' );
	$select->condition( 'p.position_id',$params );
	//execute the query
	$result = $select->execute();

	//Make sure only one entry was returned.  If not, display an error
	//TODO:  Make a prettier form when an error is returned.  Perhaps add a link to the list page.
	if ( !$result->rowcount() == 1 ) {
		drupal_set_message('Unable to locate the position specified in the URL.  Please try your query again.','error');
		return '';
	}

	//One result confirmed.  Set that row into a variable
	foreach ($result as $row) {	$position = $row; }

	// Define all the form elements
  $form['position_id']		=	array( '#type' => 'hidden', 	 '#title' => t('ID'), 				'#default_value' => $position->position_id, );
  $form['position_name']	= array( '#type' => 'textfield', '#title' => t('Position Name'),  			'#required' => TRUE,  '#size' => 50, '#maxlength' => 50,  '#default_value' => $position->position_name,  '#description' => t('The name of this position.') );
  $form['description']	= array( '#type' => 'textfield', '#title' => t('Position Description'),	'#required' => FALSE, '#size' => 80, '#maxlength' => 255, '#default_value' => $position->description, '#description' => t('(Optional) A short description of the position.') );
	//Define the Submit buttons
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit'), );
  $form['delete'] = array( '#type' => 'submit', '#value' => t('Delete'), );
  $form['cancel'] = array( '#type' => 'submit', '#value' => t('Cancel'), );
	//Return the form
 	return $form;
}
/***************************/
/* EDIT: Validate handler  */
/***************************/
function emmaus_board_positions_edit_form_validate($form, &$form_state) {
}
/************************/
/* EDIT: Submit handler */
/************************/
function emmaus_board_positions_edit_form_submit($form, &$form_state) {
	//Delete button has been clicked initially.  Set a flag and rebuild the form.
	if (!isset($form_state['storage']['confirm']) && $form_state['values']['op']=='Delete' ) {
		$form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form      
		$form_state['rebuild'] = TRUE; // along with this    
	}
	//Deletion has been confirmed.  Process the queries.
	elseif (isset($form_state['storage']['confirm']) ) {		
		//Remove the position value from all board assignments
		$num_updated = db_update('emmaus_board_assignments')  
			->fields(	array( 'position_name'=>NULL	))  
			->condition('position_name', $form_state['values']['position_name'] )  
			->execute();
		var_dump($num_updated);
		drupal_set_message( t('@number Board of Directors assignments were modified.'), array('@number' => $num_updated     ), 'status'  );
		//drupal_set_message( t('Board of Directors assignments were modified.'), 'status'  );
		//Delete the position row
		$num_deleted = db_delete('emmaus_board_positions')
			->condition('position_id', $form_state['values']['position_id'])
			->execute();
		drupal_set_message( t('<em>@name</em> position was deleted', array('@name' => $form_state['values']['position_name'])), 'status'  );
	}

	//Submit button was pressed.  Write the record.
	elseif ($form_state['values']['op']=='Submit') {
		drupal_write_record('emmaus_board_positions', $form_state['values'], 'position_id');
		drupal_set_message(t('@name position has been updated', array('@name' => $form_state['values']['position_name'])));
	}
	//Redirect the form for all functions, including the cancel
	$form_state['redirect'] = 'admin/config/emmaus/board/positions';
}



