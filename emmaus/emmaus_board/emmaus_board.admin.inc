<?php 
// $Id:  
/*
 * @file
 * Administrative functions for the Emmaus Board of Directors module
 */
 


/**
 * Form builder function to configure settings for the board module
 */
function emmaus_board_admin_settings() {
	// define checkbox for 'allow anonymous' setting
	$form['emmaus_board_enable'] = array(
		'#type' => 'checkbox',
		'#title' => t('Enable Board of Directors Management'),
		'#description' => t('Enable management and configuration of your community\'s Board of Directors..'),
		'#default_value' => variable_get('emmaus_board_enable', 0),
	);

	//Set the function that will be called on submit
	//$form['#submit'][] = 'emmaus_board_enable_submit';
	//Return the form
	return system_settings_form($form);
}




function emmaus_board_get_positions() {
	//Query the database to get selection options for positions and jobs
	$positions_result = db_query("SELECT * FROM {emmaus_board_positions}");
	foreach ($positions_result as $row) {		 	
		$positions[$row->position_id] = t($row->position_name);  
	}
	return $positions;
}



function emmaus_board_get_jobs() {
	//Query the database to get selection options for positions and jobs
	$jobs_result = db_query("SELECT * FROM {emmaus_board_jobs}");
	foreach ($jobs_result as $row) {		 	
		$jobs[$row->job_id] = t($row->job_name);  
	}
	return $jobs;
}