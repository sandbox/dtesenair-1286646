<?php 
// $Id$
  

/*************************************************************************************************************/
/*** Table/List builder function(s) to display existing board members ****************************************/
/*************************************************************************************************************/
function emmaus_board_assignments_list() { 
	//Set the tag line
	$output = '<h3><p>Current members of the ' . variable_get('emmaus_community_name') . ' Board of Directors.</p></h3>';
	//Define the table header
	$header = array(
		array("data" => "", 'width'=>'30' ),		//edit
		//array("data" => ""),		//delete
		array("data" => "", 'width'=>'30'),		//contact
		array("data" => "Name" ),
		array("data" => "Position"),
		array("data" => "Job"),
		//array("data" => "Term Start", 'field'=>'term_start'),
		array("data" => "Term End"),
		array("data" => "Voting?"),
	);
  //Get the current timestamp
  $current_timestamp = date_timestamp_get(new DateTime());
	//Query the database
  $select = db_select('emmaus_board_assignments', 'a');
  $select->leftJoin('emmaus_board_jobs', 'j', 'a.job_name = j.job_name');
  $select->leftJoin('emmaus_community_profile', 'p', 'a.profile_id = p.profile_id');
  $select->fields('a')
      ->fields('j', array( 'weight'))
      ->fields('p', array( 'first_name', 'last_name', 'is_clergy'))
      ->condition('a.term_start', $current_timestamp, '<' )
      ->condition('a.term_end', $current_timestamp, '>')
      ->orderBy('a.is_voting', 'desc')
      ->orderBy('j.weight', 'asc');
	$myresult = $select->execute();
	//Loop through the result and create an array of rows
	$rows = array();
	foreach ($myresult as $row) {		
		//create links
		$edit_link = '<a href="' . base_path() . 'emmaus/board/assignments/' . $row->assignment_id . '/edit"><img border="0" alt="Edit" title="View this board assignment" src="' . base_path() . drupal_get_path('module','emmaus_board') . '/images/edit.png"></a>';
		//$delete_link = '<a href="/admin/emmaus/board/delete/' . $row->board_id . '"><img border="0" alt="Del" title="Delete this board assignment" src="/'. drupal_get_path('module','emmaus_board') . '/images/drop.png"></a>';
		$contact_link = '<a href="'. base_path() . 'user/' . $row->profile_id . '/contact"><img border="0" alt="Contact" title="Contact this board memeber" src="' . base_path() . drupal_get_path('module','emmaus_board') . '/images/envelope_12.jpg"></a>';
		$name_link = '<a href="/user/' . $row->profile_id . '">' . $row->first_name . ' ' . $row->last_name . '</a>';
		//Convert is_clergy integer to image		
		$clergy = array('data'=>'-','align'=>'center');
		if ( $row->is_clergy == 1 ) {
			$clergy = array( 'data' => '<img border="0" alt="Yes" src="' . base_path() . drupal_get_path('module','emmaus_board') . '/images/check.png">', 'align'=>'center' );
		}
		//Convert is_voting integer to image
		$voting = array('data'=>'-','align'=>'center');
		if ( $row->is_voting == 1 ) {
			$voting = array('data'=>'<img border="0" alt="Yes" src="' . base_path() . drupal_get_path('module','emmaus_board') . '/images/check.png">', 'align'=>'center');
		}
		//Format date displays
		$term_start = format_date( $row->term_start, 'custom', 'M d, Y');
		$term_end = format_date( $row->term_end, 'custom', 'M d, Y');
		//Generate the rows array
		//$rows[] = array($edit_link, /*$delete_link,*/ $contact_link, $name_link, $row->position_name, $row->job_name, $term_start, $term_end, $clergy, $voting);
		$rows[] = array($edit_link, /*$delete_link,*/ $contact_link, $name_link, $row->position_name, $row->job_name, $term_end, $voting);
	}
	//output the table
	$tbl_vars = array(  
		'header'=>$header,	
		'rows'=>$rows,	
		'attributes'=>array(),	
		'caption' => '',		
		//'caption' => $myresult->rowCount() . ' row(s) returned.',		
				'colgroups'=>array(),		
		'sticky'=>TRUE, 	
		'empty'=>'No rows returned from query' 	
		);
	$output .= theme_table($tbl_vars);
	//output a link to add a new assignment
	//$output .= '<a href="/emmaus/board/assignments/add"><img src="/'. drupal_get_path('module','emmaus_community') . '/images/add.png">Add a new board member assignment</a>';

	//output the pager
	//$output .= theme('pager');
  
	//Return the output
	return $output;
}

 






/*************************************************************************************************************/
/*** EDIT: Form builder - function(s) to edit an existing board member entry *********************************/
/*************************************************************************************************************/
function emmaus_board_assignments_form($form, &$form_state, $assignment=NULL) {
	//Check to see if we should display the confirmation form
	if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Delete') {
		// Define form elements again, prior to final submission
		$form['assignment_id']	= array( '#type' => 'textfield', '#title' => t('ID'), 				'#default_value' => $form_state['values']['assignment_id']   );
	  $form['full_name']			= array( '#type' => 'textfield', '#title' => t('Full Name'), 	'#default_value' => $form_state['values']['full_name']   );
		//Display the confirmation form
		return confirm_form($form, t('Are you sure that you want to remove @name from the board?', array('@name'=>$form_state['values']['full_name'])), 'admin/emmaus/board', 'This action can not be undone.  Please proceed with caution.', 'Remove', 'Don\'t Remove'); //Had better luck leaving off last param 'name'  
	}	//confirmation form

	
	if ($assignment == NULL ) { drupal_set_title('Add Board Assignment');}	
	//dpm($assignment);

	//Call the functions that return the jobs and positions array so we can poopulate the select boxes
	$jobs = emmaus_board_get_jobs();
	$positions=emmaus_board_get_positions();
  

	// Define all the form elements
  $form['assignment_id'] =	array(
    '#type' => 'hidden',
    '#title' => t('ID'),
    '#default_value' => $assignment['assignment_id'], 
    );
    
  if ($assignment != NULL )   {
    $form['caption'] = array( 
  	  '#type' => 'item',			 	
  	  '#title' => t('NOTE:  Be very careful when editing existing board member assignments as this will affect the historical records and "experience" of the member.  <br/>If you are unsure, please click the "Cancel" button at the bottom of the form to return to the board listing page.')
      );
  }
  if ($assignment == null ) {  
    $form['full_name'] = array( 
      '#type' => 'textfield',  
      '#title' => t('Name'),            
      '#required' => TRUE,  
      '#size' => 50, 
      '#maxlength' => 50,  
      '#description' => t('Type the first few letters of the person whom you are assigning.  Names MUST be chosen from the list.'),     
      '#autocomplete_path' => 'emmaus/community/profile/autocomplete',  
      );
  } else {  
    $form['full_name'] = array( 
      '#type' => 'textfield', 	
      '#title' => t('Full Name'), 		  
      '#required' => TRUE, 
      '#size' => 50, 
      '#maxlength' => 50, 	
      '#default_value' => $assignment['full_name'], 		
      '#description' => t('Read only.  This field can not be modified from this form.'), 	
      '#disabled'=> TRUE, 
      );
  }
  
  $form['position_name'] = array( 
    '#type' => 'select', 	 	
    '#title' => t('Position'),			  
    '#required' => FALSE, 
    '#options' => $positions, 					
    '#default_value' => $assignment['position_name'], 
    '#description' => t('What position does this board member fill?')   								
    );
  $form['job_name'] = array( 
    '#type' => 'select', 	 	
    '#title' => t('Job/Function'),   	
    '#required' => FALSE,	
    '#options' => $jobs, 			  				
    '#default_value' => $assignment['job_name'] , 		
    '#description' => t('What function does this board member perform?')								
    );
  $form['term_start'] = array( 
    '#type' => 'date_popup',
    '#date_format'=>'F d, Y',
    '#title' => t('Term Start'),
    '#required' => TRUE,
    '#default_value' => isset($assignment['term_start']) ? date('Y-m-d', $assignment['term_start']) : NULL ,
    //($var > 2 ? true : false)
    
    '#date_label_position' => 'within',                                                         
    );
  $form['term_end'] = array( 
    '#type' => 'date_popup',
    '#date_format'=>'F d, Y',
    '#title' => t('Term End'),
    '#required' => TRUE,
    '#default_value' => isset($assignment['term_end']) ? date('Y-m-d', $assignment['term_end']) : NULL ,
    '#date_label_position' => 'within',                                                         
    );
	$form['is_voting'] = array( 
	 '#type' => 'checkbox',  	
	 '#title' => t('Voting member?'), 	
	 '#required' => FALSE, 																		
	 '#default_value' => $assignment['is_voting'],																																				
   );
	//Define the Submit buttons
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit'), );
  $form['delete'] = array( '#type' => 'submit', '#value' => t('Delete'), );
  $form['cancel'] = array( '#type' => 'submit', '#value' => t('Cancel'), );
	//Return the form
 	return $form;
}
/***************************/
/* EDIT: Validate handler  */
/***************************/
function emmaus_board_assignments_form_validate($form, &$form_state) {
	if ($form_state['values']['op']=='Submit') {
    //drupal_set_message('<pre>' . print_r($form_state['values'], TRUE) . '</pre>');

		//Validate Term End
    $term_start = strtotime($form_state['values']['term_start']);
    $term_end = strtotime($form_state['values']['term_end']);
		if ($term_start >= $term_end ) {
			form_set_error('term_end', '\'Term End\' must be AFTER \'Term Start\'');
		}
    
    //Make sure selected name can be found in the 'emmaus_profile' table
    $select = db_select('emmaus_community_profile', 'p')    
      ->fields('p', array('full_name') )    
      ->condition('full_name', $form_state['values']['full_name'] )
      ->execute();
    if ( !$select->rowCount()==1 ) {
      form_set_error('full_name', 'Could not locate the selected profile.');
    }
    
	}

}

/************************/
/* EDIT: Submit handler */
/************************/
function emmaus_board_assignments_form_submit($form, &$form_state) {
	//delete button has been clicked initially.
	if (!isset($form_state['storage']['confirm']) && $form_state['values']['op']=='Delete' ) {
		$form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form      
		$form_state['rebuild'] = TRUE; // along with this    
	}
	//Deletion has been confirmed
	elseif (isset($form_state['storage']['confirm']) ) {		
		$num_deleted = db_delete('emmaus_board_assignments')
			->condition('assignment_id', $form_state['values']['assignment_id'])
			->execute();
		drupal_set_message( t('Board of Directors table updated. (@name deleted!) ', array('@name' => $form_state['values']['full_name'])), 'warning'  );
	}
  
	//Write the record
	elseif ($form_state['values']['op']=='Submit') {
		//Job & Positions
		if ( $form_state['values']['position_name'] == ''  ) { $form_state['values']['position_name'] = NULL; }
		if ( $form_state['values']['job_name'] == '' ) { $form_state['values']['job_name'] = NULL; }
		
		//write the record
		if ($form_state['values']['assignment_id'] == NULL ) { //adding a new record
      //extract the profile id
      $profile_id = db_select('emmaus_community_profile', 'p')
          ->condition('p.full_name' , $form_state['values']['full_name'] )
          ->fields('p', array('profile_id'))
          ->execute()
          ->fetchField();      
				//dpm ($profile_id);
    
      //Generate the insert query
      db_insert('emmaus_board_assignments')
        ->fields(array(
          'profile_id'    => $profile_id,
          'position_name' => $form_state['values']['position_name'],
          'job_name'      => $form_state['values']['job_name'],
          'term_start'    => strtotime($form_state['values']['term_start']),
          'term_end'      => strtotime($form_state['values']['term_end']),
          'is_voting'     => $form_state['values']['is_voting'],
          )
        )
        ->execute();
     
      
      //set a confirmation message
      drupal_set_message(t('Board assignment for @name added.', array('@name' => $form_state['values']['full_name'])));
          
		
    } else {
  		//drupal_write_record('emmaus_board_assignments', $form_state['values'], 'assignment_id');
  		$update = db_merge('emmaus_board_assignments')  
  			->key(array('assignment_id' => $form_state['values']['assignment_id'])  )
  			->fields( array( 
  					'position_name' => $form_state['values']['position_name'],
  					'job_name' 			=> $form_state['values']['job_name'],
  					'term_start'	 	=> strtotime($form_state['values']['term_start']),
  					'term_end' 			=> strtotime($form_state['values']['term_end']),
  					'is_voting' 		=> $form_state['values']['is_voting'],
  					))
  			->execute();
      //set a confirmation message
      drupal_set_message(t('Board assignment for @name updated.', array('@name' => $form_state['values']['full_name'])));
    }


	}
	//Redirect the form for all functions, including the cancel
	$form_state['redirect'] = 'emmaus/board';
}











