<?php 
// $Id:  
/*
 * @file
 * Administrative functions for the Emmaus Board of Directors module
 */
 
   
  
/*************************************************************************************************************/
/*** Table/List builder function(s) to display existing jobs ********************************************/
/*************************************************************************************************************/
function emmaus_board_jobs_list() { 
	//Set the tag line
	$output = '<h3><p>Current job definitions for the ' . variable_get('emmaus_community_name') . ' Board of Directors.</p></h3>';
  $output .= '<p>From this page, you can add, delete, or modify a board job.</p>';
	//Define the table header
	$header = array(
		'edit' => array(
		  'data' => 'Edit', 
		  'width'=>'40' 
      ),
    'weight' => array(
      'data' => t('Weight'), 
      'width'=>'40' 
      ),
		'name' => array(
		  'data' => t('Job Name'), 
		  'width'=>'25%' 
      ),
    'descr' => array(
      'data' => t('Description'), 
      ),
	);
	//Query the database
  $result = db_select('emmaus_board_jobs', 'j')
		->fields('j')
  	//->extend('PagerDefault')			//extend the query to include a pager
  	//->limit(50)										//Set the number of items per page
  	//->extend('TableSort')					//Extend the query to include table sorting
  	//->orderByHeader($header)			//define sort column
  	->orderBy('weight', 'asc')
    ->orderBy('job_name', 'asc')
		->execute();
	
	//Loop through the result and create an array of rows
	$rows = array();
	foreach ($result as $row) {		
		//create links
		$edit_link = '<a href="' . base_path() . 'admin/config/emmaus/board/jobs/' . $row->job_id . '/edit"><img border="0" alt="Edit" title="Edit this board assignment" src="'. base_path() . drupal_get_path('module','emmaus_board') . '/images/edit.png"></a>';
		$rows[] = array($edit_link, $row->weight, $row->job_name, $row->description);
	}
	//output the table
	$tbl_vars = array(  'header'=>$header,	'rows'=>$rows,	'attributes'=>array(),	'caption' => $result->rowCount() . ' row(s) returned.',		'colgroups'=>array(),		'sticky'=>TRUE, 	'empty'=>'No rows returned from query' 	);
	$output .= theme_table($tbl_vars);
	//output a link to add a new assignment
	//$output .= '<a href="/admin/emmaus/board/settings/jobs/add"><img src="/'. drupal_get_path('module','emmaus_community') . '/images/add.png">Add a new board job definition</a>';

	//output the pager
	$output .= theme('pager');
	//Return the output
	return $output;
}









/*************************************************************************************************************/
/*** Form builder function(s) to add a new job **********************************************************/
/*************************************************************************************************************/
function emmaus_board_jobs_add_form() {

	// Define the form elements
  $form['emmaus_board_job_add'] = array( 
    '#type' => 'item', 			
    '#title' => t('Add new Board of Directors job definition.'),  
    );
  $form['job_name']	= array( 
    '#type' => 'textfield',	
    '#title' => t('Job Name'),
    '#required' => TRUE,  
    '#size' => 50, 
    '#maxlength' => 50, 	
    '#description' => t('Enter a name of this job.')   		);
  $form['description'] = array( 
    '#type' => 'textfield',  
    '#title' => t('Job Description'), 
    '#required' => FALSE, 
    '#size' => 50, 
    '#maxlength' => 255, 
    '#description' => t('Enter a short description of this job.')       
    );
  $form['weight'] = array( 
    '#type' => 'weight',     
    '#title' => t('Weight'),          
    '#delta' => 10,   
    '#description' => t('Optional.
In the menu, the heavier items will sink and the lighter items will be
positioned nearer the top.'),
  );	

  //Submit button
  $form['submit'] = array( '#type' => 'submit', '#value' => 'Submit', );
	//Return the form
  return $form;
}
/****************************/
/*** ADD Validate handler ***/
/****************************/
function emmaus_board_jobs_add_form_validate($form, &$form_state) {
}
/**************************/
/*** ADD Submit handler ***/
/**************************/
function emmaus_board_jobs_add_form_submit($form, &$form_state) {
	//Generate the insert query
  db_insert('emmaus_board_jobs')
      ->fields(array(
            'job_name' => $form_state['values']['job_name'],
            'weight' => $form_state['values']['weight'],
						'description' => $form_state['values']['description'],
					)
				)
      ->execute();
	//Set a confirmation message
  drupal_set_message(t('The job <em>@name</em> has been added to the Board of Directors.', array('@name' => $form_state['values']['job_name'])));
	//Redirect the form
	$form_state['redirect'] = 'admin/config/emmaus/board/jobs';
}




/*************************************************************************************************************/
/*** EDIT: Form builder - function(s) to edit an existing board member entry *********************************/
/*************************************************************************************************************/
function emmaus_board_jobs_edit_form($form, &$form_state, $params) {
	//Check to see if we should display the confirmation form
	if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Delete') {
		// Define form elements again, prior to final submission
		$form['job_id']		= array( '#type' => 'textfield', '#title' => t('Job ID'), 		'#default_value' => $form_state['values']['job_id']   	);
	  $form['job_name']	= array( '#type' => 'textfield', '#title' => t('Job Name'), 	'#default_value' => $form_state['values']['job_name']   );
    
		//Display the confirmation form
		return confirm_form($form, t('Are you sure that you want to delete the @name job from the board?', array('@name'=>$form_state['values']['job_name'])), 'admin/config/emmaus/board/jobs', 'This action can not be undone.  Please proceed with caution.', 'Remove', 'Don\'t Remove'); //Had better luck leaving off last param 'name'  
	}	//confirmation form
	
	//Query the database for the record to edit
  $select = db_select('emmaus_board_jobs', 'j' );
  $select->fields( 'j' );
	$select->condition( 'j.job_id',$params );
	//execute the query
	$result = $select->execute();

	//Make sure only one entry was returned.  If not, display an error
	//TODO:  Make a prettier form when an error is returned.  Perhaps add a link to the list page.
	if ( !$result->rowcount() == 1 ) {
		drupal_set_message('Unable to locate the job specified in the URL.  Please try your query again.','error');
		return '';
	}

	//One result confirmed.  Set that row into a variable
	foreach ($result as $row) {	$job = $row; }

	// Define all the form elements
  $form['emmaus_board_job_add'] = array( 
    '#type' => 'item', 			
    '#title' => t('Edit an existing Board of Directors job definition.'),  
    );
  $form['job_id']	=	array( 
    '#type' => 'hidden', 	 
    '#title' => t('ID'), 				
    '#default_value' => $job->job_id, 
    );
  $form['job_name']	= array( 
    '#type' => 'textfield', 
    '#title' => t('Job Name'),  			
    '#required' => TRUE,  
    '#size' => 50, 
    '#maxlength' => 50,  
    '#default_value' => $job->job_name,  
    '#description' => t('The name of this job.') 
    );
  $form['description'] = array( 
    '#type' => 'textfield', 
    '#title' => t('Job Description'),	
    '#required' => FALSE, 
    '#size' => 80, 
    '#maxlength' => 255, 
    '#default_value' => $job->description, 
    '#description' => t('Optional. A short description of the duties this job performs.') 
    );
  $form['weight'] = array( 
    '#type' => 'weight',     
    '#title' => t('Weight'),          
    '#default_value' => $job->weight,    
    '#delta' => 20,   
    '#default_value' => $job->weight, 
    '#description' => t('Optional. In the menu, the heavier items will sink and the lighter items will be positioned nearer the top.')
    );
	//Define the Submit buttons
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit'), 																										);
  $form['delete'] = array( '#type' => 'submit', '#value' => t('Delete'), 	);
  $form['cancel'] = array( '#type' => 'submit', '#value' => t('Cancel'), 	);
	//Return the form
 	return $form;
}
/***************************/
/* EDIT: Validate handler  */
/***************************/
function emmaus_board_jobs_edit_form_validate($form, &$form_state) {
}
/************************/
/* EDIT: Submit handler */
/************************/
function emmaus_board_jobs_edit_form_submit($form, &$form_state) {
	//Delete button has been clicked initially.  Set a flag and rebuild the form.
	if (!isset($form_state['storage']['confirm']) && $form_state['values']['op']=='Delete' ) {
		$form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form      
		$form_state['rebuild'] = TRUE; // along with this    
	}
  
	//Deletion has been confirmed.  Process the queries.
	elseif (isset($form_state['storage']['confirm']) ) {		
		//Remove the job value from all board assignments
		$num_updated = db_update('emmaus_board_assignments')  
			->fields(	array( 'job_name'=>NULL	))  
			->condition('job_name', $form_state['values']['job_name'] )  
			->execute();
		drupal_set_message( $num_updated . t(' Board of Directors assignments were modified.'), 'status'  );
		//Delete the job row
		$num_deleted = db_delete('emmaus_board_jobs')
			->condition('job_id', $form_state['values']['job_id'])
			->execute();
		drupal_set_message( t('<em>@name</em> job was deleted', array('@name' => $form_state['values']['job_name'])), 'status'  );
	}

	//Submit button was pressed.  Write the record.
	elseif ($form_state['values']['op']=='Submit') {
    //drupal_set_message('<pre>' . print_r($form_state['values'], TRUE) . '</pre>');

		drupal_write_record('emmaus_board_jobs', $form_state['values'], 'job_id');
		drupal_set_message(t('@name job has been updated', array('@name' => $form_state['values']['job_name'])));
	}
	//Redirect the form for all functions, including the cancel
	$form_state['redirect'] = 'admin/config/emmaus/board/jobs';
}




