<?php
// $Id:




/**
 * Implements hook_entity_info
 */
function emmaus_weekend_entity_info() {
 $info = array();

  $info['emmaus_weekend'] = array(
    'label' => t('Emmaus Weekend'),
    'plural label' => t('Emmaus Weekends'),
    'base table' => 'emmaus_weekend_weekends',
    'entity keys' => array(
      'id' => 'weekend_id',
      'label' => 'weekend_name',
    ),
    'module'=>'emmaus_weekend',
    'enity class'=>'Entity',
    'controller class'=>'EntityAPIController',
    'views controller class' => 'EntityDefaultViewsController',
  );
  
  return $info;
}


/**
 * Implements hook_entity_property_info().
 */
function emmaus_weekend_entity_property_info() {

  $info = array();

  $info['emmaus_weekend']['properties']['id'] = array(
    'description' => t('The ID of the weekend'),
    'label' => t('Weekend ID'),
    'type' => 'integer',
    'schema field' => 'weekend_id',
  );
  $info['emmaus_weekend']['properties']['weekend_number'] = array(
    'label' => t('Weekend Number'),
    'description' => t('Weekend Number Property'),
    'type' => 'text',
    'schema field' => 'weekend_number',
  );
  $info['emmaus_weekend']['properties']['weekend_type'] = array(
    'label' => t('Weekend Type'),
    'description' => t('Weekend Type Property'),
    'type' => 'text',
    'schema field' => 'weekend_type',
  );
  $info['emmaus_weekend']['properties']['weekend_name'] = array(
    'label' => t('Weekend Name'),
    'description' => t('Weekend Name Property'),
    'type' => 'text',
    'schema field' => 'weekend_name',
  );
  $info['emmaus_weekend']['properties']['description'] = array(
    'label' => t('Description'),
    'description' => t('Description Property'),
    'type' => 'text',
    'schema field' => 'description',
  );
  $info['emmaus_weekend']['properties']['location'] = array(
    'label' => t('Location'),
    'description' => t('Location Property'),
    'type' => 'text',
    'schema field' => 'location',
  );  
  $info['emmaus_weekend']['properties']['address'] = array(
    'label' => t('Address'),
    'description' => t('Address Property'),
    'type' => 'text',
    'schema field' => 'address',
  );
  $info['emmaus_weekend']['properties']['start_date'] = array(
    'label' => t('Start Date'),
    'description' => t('Start Date Property'),
    'type' => 'date',
    'schema field' => 'start_date',
  );
  $info['emmaus_weekend']['properties']['gender'] = array(
    'label' => t('Gender'),
    'description' => t('Gender Property'),
    'type' => 'text',
    'schema field' => 'gender',
  );
  $info['emmaus_weekend']['properties']['active'] = array(
    'label' => t('Active?'),
    'description' => t('Active Property'),
    'type' => 'boolean',
    'schema field' => 'active',
  );
  $info['emmaus_weekend']['properties']['open'] = array(
    'label' => t('Open?'),
    'description' => t('Open Property'),
    'type' => 'boolean',
    'schema field' => 'open',
  );
  
  
  return $info;
}





/**
 * Implements hook_help().
 */
function emmaus_weekend_help($path, $arg) {
  switch ($path) {
    case 'admin/help#emmaus_weekend':
      $output  = '<h3>' . t('About') . '</h3>';
      $output .= '<p>'  . t('Provides a set of pages to manage emmaus weekends') . '</p>';
      $output .= '<h3>' . t('Settings') . '</h3>';
      $output .= '<dl>';
      $output .= '<dt>' . t('Maximum alias and component length') . '</dt>';
      $output .= '<dd>' . t('The <strong>maximum alias length</strong> and <strong>maximum component length</strong> values default to 100 and have a limit of @max from Pathauto. This length is limited by the length of the "alias" column of the url_alias database table. The default database schema for this column is @max. If you set a length that is equal to that of the one set in the "alias" column it will cause problems in situations where the system needs to append additional words to the aliased URL. For example, URLs generated for feeds will have "/feed" added to the end. You should enter a value that is the length of the "alias" column minus the length of any strings that might get added to the end of the URL. The length of strings that might get added to the end of your URLs depends on which modules you have enabled and on your Pathauto settings. The recommended and default value is 100.', array('@max' => 100)) . '</dd>';
      $output .= '</dl>';
      return $output;
  }
}


/**
 * Implements hook_permission().
 */
function emmaus_weekend_permission() {
  return array(
    'administer emmaus weekend' => array(
      'title' => t('Administer Weekends'),
      'description' => t('Add/modify/delete weekends for your Emmaus community.'),
      ),
    'view emmaus weekend' => array(
      'title' => t('View Weekends'),
      'description' => t('View weekends for your Emmaus community.'),
      ),
   );
}



/**
 * Implementation of hook_menu().
 */
function emmaus_weekend_menu() {
  
  $items['emmaus/weekend/add'] = array(
    'title' => 'Add a new weekend',
    'type' => MENU_LOCAL_ACTION,
    'access callback' => 'user_access',
    'access arguments' => array('administer emmaus weekend'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('emmaus_weekend_add'),
    'file' => 'emmaus_weekend.weekends.inc',
    'weight' => 0,
  );
  
  $items['emmaus/weekend/%_emmaus_weekend'] = array(
    //'title callback' => 'emmaus_weekend_name',
    //'title arguments' => array(2),
	  'title'=>'View Weekend',       //I set a hard title, along with a title callback because the breadcrumb breaks in 2nd level MENU_LOCAL_TASK items
	  'description' => 'View weekend details' ,
	  'access callback' => 'user_access',
	  'access arguments' => array('view emmaus weekend'),
	  'page callback' => 'emmaus_weekend_view',
	  'page arguments' => array(2),
	  'file' => 'emmaus_weekend.weekends.inc',
	  'type' => MENU_NORMAL_ITEM,
  );
  $items['emmaus/weekend/%_emmaus_weekend/view'] = array(
  'title' => 'View',
	'type' => MENU_DEFAULT_LOCAL_TASK,
	'access callback' => 'user_access',
	'access arguments' => array('view emmaus weekend'),
	'weight' => 0,
  );
  $items['emmaus/weekend/%/edit'] = array(
		'title' => 'Edit',
		'type' => MENU_LOCAL_TASK,
		'access callback' => 'user_access',
		'access arguments' => array('administer emmaus weekend'),
		'page callback' => 'drupal_get_form',
		'page arguments' => array('emmaus_weekend_edit',2),
		'file' => 'emmaus_weekend.weekends.inc',
		'weight' => 10,
  );
  $items['emmaus/weekend/%/edit/details'] = array(
    'title' => 'Weekend Details',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'access callback' => 'user_access',
    'access arguments' => array('administer emmaus weekend'),
    'weight' => 0,
  );


  //Menu items for weekend administration
  $items['admin/config/emmaus/weekend'] = array(
		'title' => 'Weekend Settings',
		'description' => 'Change the global settings for Emmaus weekend management.',
		'page callback' => 'drupal_get_form',
		'page arguments' => array('emmaus_weekend_settings'),
		'access arguments' => array('administer emmaus weekend'),
		'type' => MENU_NORMAL_ITEM,
		'file' => 'emmaus_weekend.admin.inc',
		'weight' => 40,
  );
  $items['admin/config/emmaus/weekend/settings'] = array(
		'title' => 'Weekend Defaults',
		'type' => MENU_DEFAULT_LOCAL_TASK,
		'access arguments' => array('administer emmaus weekend'),
		'weight' => 0,
  );
  //Weekend Types
  $items['admin/config/emmaus/weekend/types'] = array(
		'title' => 'Weekend Types',
		'type' => MENU_LOCAL_TASK,
		'access callback' => 'user_access',
		'access arguments' => array('administer emmaus weekend'),
		'page callback' => 'emmaus_weekend_types_admin',
		'file' => 'emmaus_weekend.types.inc',
		'weight' => 20,
  );
  $items['admin/config/emmaus/weekend/types/add'] = array(
		'title' => 'Add a new weekend type definition',
		'type' => MENU_LOCAL_ACTION,
		'access callback' => 'user_access',
		'access arguments' => array('administer emmaus weekend'),
		'page callback' => 'drupal_get_form',
		'page arguments' => array('emmaus_weekend_types_add'),
		'file' => 'emmaus_weekend.types.inc',
		'weight' => 20,
  );
  $items['admin/config/emmaus/weekend/types/%/edit'] = array(
		'title' => 'Edit Weekend Type',
		'type' => MENU_NORMAL_ITEM,
		'access callback' => 'user_access',
		'access arguments' => array('administer emmaus weekend'),
		'page callback' => 'drupal_get_form',
		'page arguments' => array('emmaus_weekend_types_edit',5),
		'file' => 'emmaus_weekend.types.inc',
		'weight' => 10,
  );



  //Registering a path for the weekend_name autocomplete function
  $items['emmaus/weekend/weekends/autocomplete'] = array(
    'description' => 'Path callback for the weekend name autocomplete function',
    'page callback' => 'emmaus_weekend_weekends_autocomplete',
    'access callback' => 'user_access',
    'access arguments' => array('view emmaus weekend'),
    'type' => MENU_CALLBACK,
    'file' => 'emmaus_weekend.weekends.inc',
  );
  //Registering a path for the location autocomplete function
  $items['emmaus/weekend/locations/autocomplete'] = array(
    'description' => 'Path callback for the weekend locations autocomplete function',
    'page callback' => 'emmaus_weekend_locations_autocomplete',
    'access callback' => 'user_access',
    'access arguments' => array('administer emmaus weekend'),
    'type' => MENU_CALLBACK,
    'file' => 'emmaus_weekend.weekends.inc',
  );

  return $items;

}




/*********************************************************************************************/
/** BEGIN CUSTOM HELPER FUNCTIONS ************************************************************/
/*********************************************************************************************/

function _emmaus_weekend_load($weekend_number) {
  //Query the database for the specified weekend
  $weekend = db_select('emmaus_weekend_weekends','w')
    ->fields('w') 
    ->condition('weekend_number' , $weekend_number)     //define a conditional query
    ->execute()
    ->fetchAssoc();
  return $weekend;
}


/** 
 * Helper function to return an array of weekend types
 */
function _emmaus_weekend_types_get() {
  $result = db_select('emmaus_weekend_types', 'w')
    ->fields('w')
    ->execute();
    //Loop through the results
    foreach ($result as $type) {
      $output[$type->type_name] = $type->type_name;
    }
  //return the results in a json object
  return $output;
}


function _emmaus_weekend_weekends( $gender=NULL, $active=NULL, $open=NULL ) {
  //var_dump($position_type);
  $select = db_select('emmaus_weekend_weekends', 'w'  );
  $select->fields('w', array('weekend_number', 'weekend_name', 'start_date') );
  if ( !empty($gender) ) { $select->condition( 'gender', $gender ); }
  if ( !empty($active) ) { $select->condition( 'active', $active ); }
  if ( !empty($open) ) { $select->condition( 'open', $open ); }
  $select->orderBy('w.start_date');
  //var_dump($select);
  $result = $select->execute();
  //Loop through the results nd create an array
  foreach($result as $row) {
    //$weekends[$row->weekend_number] = $row->weekend_name . ' (' . format_date($row->start_date, 'custom', 'M, Y') . ')';
    $weekends[$row->weekend_number] = $row->weekend_name;
  } 
  //Return the result
  return $weekends;
}









/**
* Implementation of hook_views_api().
*/
function emmaus_weekend_views_api() {
  return array(
    'api' => 3.0,
    'path' => drupal_get_path('module', 'emmaus_weekend')
  );
}
 
 
/**
 * Implementation of hook_views_default_views().
 */
function emmaus_weekend_views_default_views() {
 
  //Finds all files that match a given mask in a given directory
  //In our case, looks for any files named *.view in the /views directory
  $files = file_scan_directory(drupal_get_path('module', 'emmaus_weekend'). '/views', '/.view/');
  foreach ($files as $filepath => $file) {
    require $filepath;
    if (isset($view)) {
      $views[$view->name] = $view;
    }
  }
  //Check that there are views in the directory
  //This keeps the site from throwing errors if there are no views to return
  if ($views) {
    return $views;
  }
} 
 


