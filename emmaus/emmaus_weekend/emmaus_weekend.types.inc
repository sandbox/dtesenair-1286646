<?php 
// $Id:  


  
/*************************************************************************************************************/
/*** Table/List builder function(s) to display existing weekend types ****************************************/
/*************************************************************************************************************/
function emmaus_weekend_types_admin() { 
	//Set the tag line
	$output = '<h3><p>Current weekend types defined for the ' . variable_get('emmaus_community_name') . '.</p></h3>';
  $output .= '<p>From this page, you can add, delete, or modify weekend types.</p>';
	//Define the table header
	$header = array(
		array('data' => 'Edit', 'width'=>'40' ),		//edit
    array('data' => 'Type Name', 'width'=>'15%' ),
    array('data' => 'Description' ),
	);

	//Query the database
  $result = db_select('emmaus_weekend_types', 't')
		->fields('t')
		->execute();
	
	//Loop through the result and create an array of rows
	$rows = array();
	foreach ($result as $row) {		
		//create links
		$edit_link = '<a href="/admin/config/emmaus/weekend/types/' . $row->type_id . '/edit"><img border="0" alt="Edit" title="Edit this weekend type" src="/'. drupal_get_path('module','emmaus_community') . '/images/edit.png"></a>';
		//output the table rows
		$rows[] = array($edit_link, $row->type_name, $row->description );
	}
	//output the table
	$tbl_vars = array(  'header'=>$header,	'rows'=>$rows,	'attributes'=>array(),	'caption' => $result->rowCount() . ' row(s) returned.',		'colgroups'=>array(),		'sticky'=>TRUE, 	'empty'=>'No rows returned from query' 	);
	$output .= theme_table($tbl_vars);
	//output a link to add a new assignment
	//$output .= '<a href="/admin/emmaus/weekend/settings/types/add"><img src="/'. drupal_get_path('module','emmaus_community') . '/images/add.png">Add a new weekend type definition</a>';
	//output the pager
	$output .= theme('pager');
	//Return the output
	return $output;
}




/*************************************************************************************************************/
/*** Form builder function(s) to add a new church **********************************************************/
/*************************************************************************************************************/
function emmaus_weekend_types_add() {
	// Define the form elements
  $form['weekend_type_add'] = array( 
    '#type' => 'item', 			
    '#title' => t('Add new weekend type.'),  
    );
  $form['type_name'] = array( 
    '#type' => 'textfield',  
    '#title' => t('Type Name'),   
    '#required' => TRUE,  
    '#size' => 50, 
    '#maxlength' => 255,  
    '#description' => t('The name of the weekend type definition')                  
    );
  $form['description'] = array( 
    '#type' => 'textarea',  
    '#title' => t('Description'),   
    '#rows' => 2, 
    '#description' => t('Enter a descritpion of this weekend type')                  
    );
	//Submit button
  $form['submit'] = array( '#type' => 'submit', '#value' => 'Submit', );
	//Return the form
  return $form;
}
/****************************/
/*** ADD Validate handler ***/
/****************************/
function emmaus_weekend_types_add_validate($form, &$form_state) {
}
/**************************/
/*** ADD Submit handler ***/
/**************************/
function emmaus_weekend_types_add_submit($form, &$form_state) {
  //last minute validation of fields
  if ( $form_state['values']['description'] =='' ) { $form_state['values']['description'] = NULL; }
  if ( strlen($form_state['values']['description'])>512 ) {
    $form_state['values']['description'] = substr($form_state['values']['description'], 0, 512);  
    drupal_set_message( t('Description was too long for database columnn and was truncated.'), 'warning' );
  }
  

	//Generate the insert query
  db_insert('emmaus_weekend_types')
      ->fields(array(
            'type_name'  => $form_state['values']['type_name'],
            'description'  => $form_state['values']['description'],
					)
				)
      ->execute();
	//Set a confirmation message
  drupal_set_message(t('The weekend type <em>@name</em> has been added to the community.', array('@name' => $form_state['values']['type_name'])));
	//Redirect the form
	$form_state['redirect'] = 'admin/config/emmaus/weekend/types';
}




/*************************************************************************************************************/
/*** EDIT: Form builder - function(s) to edit an existing board member entry *********************************/
/*************************************************************************************************************/
function emmaus_weekend_types_edit($form, &$form_state, $params) {
	//Check to see if we should display the confirmation form
	if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Delete') {
		// Define form elements again, prior to final submission
		$form['type_id']   = array( '#type'=>'hidden', '#default_value'=>$form_state['values']['type_id']   );
		$form['type_name'] = array( '#type'=>'hidden', '#default_value'=>$form_state['values']['type_name'] );
		//Display the confirmation form
		return confirm_form($form, t('Are you sure that you want to delete the <em>@name</em> weekend type definition?', array('@name'=>$form_state['values']['type_name'])), 'admin/config/emmaus/weekend/types', 'This action can not be undone.  Please proceed with caution.', 'Remove', 'Don\'t Remove'); //Had better luck leaving off last param 'name'  
	}	//confirmation form
	
	//Query the database for the record to edit
  $type = db_select( 'emmaus_weekend_types', 't' )
  	->fields( 't')
		->condition( 't.type_id' , $params )
		->execute()
    ->fetchAssoc();
	//Make sure only one entry was returned.  If not, display an error
	//TODO:  Make a prettier form when an error is returned.  Perhaps add a link to the list page.
	if ( empty($type) ) {
		drupal_set_message(  t('Unable to locate the weekend type specified in the URL (Param: @param).  Please try your query again.' , array( '@param'=>$params )) ,'error');
		return '';
	}
	// Define all the form elements
  $form['weekend_type_edit'] 	= array( 
    '#type' => 'item', 			
    '#title' => t('Edit existing weekend type definition.'),  
    );
  $form['type_id'] = array( 
    '#type' => 'hidden',
    '#default_value' => $type['type_id'],
    );
  $form['type_name'] = array( 
    '#type' => 'textfield',	
    '#title' => t('Type Name'),		
    '#required' => TRUE, 	
    '#size' => 50, 	
    '#maxlength' => 255, 	
    '#default_value' => $type['type_name'],  	
    '#description' => t('The name of the weekend type definition')                  
    );
  $form['description'] = array( 
    '#type' => 'textarea',  
    '#title' => t('Description'),   
    '#rows' => 2, 
    '#description' => t('Enter a descritpion of this weekend type'),                  
    '#default_value' => $type['description'],   
    );
	//Define the Submit buttons
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit'), );
  $form['delete'] = array( '#type' => 'submit', '#value' => t('Delete'), );
	//Return the form
 	return $form;
}
/***************************/
/* EDIT: Validate handler  */
/***************************/
function emmaus_weekend_types_edit_validate($form, &$form_state) {
}
/************************/
/* EDIT: Submit handler */
/************************/
function emmaus_weekend_types_edit_submit($form, &$form_state) {
	//**Delete button has been clicked initially.  Set a flag and rebuild the form.
	if (!isset($form_state['storage']['confirm']) && $form_state['values']['op']=='Delete' ) {
		$form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form      
		$form_state['rebuild'] = TRUE; // along with this    
	}
	//**Deletion has been confirmed.  Process the queries.
	elseif (isset($form_state['storage']['confirm']) ) {		
		$num_deleted = db_delete('emmaus_weekend_types')
			->condition('type_id', $form_state['values']['type_id'])
			->execute();
		drupal_set_message( t('<em>@name</em> weekend type definition was deleted.  Existing weekends of this type were NOT affected by this change.', array('@name' => $form_state['values']['type_name'])), 'status'  );
	}

	//**Submit button was pressed.  Write the record.
	elseif ($form_state['values']['op']=='Submit') {
    //last minute validation of fields
    if ( $form_state['values']['description'] =='' ) { $form_state['values']['description'] = NULL; }
    if ( strlen($form_state['values']['description'])>512 ) {
      $form_state['values']['description'] = substr($form_state['values']['description'], 0, 512);  
      drupal_set_message( t('Description was too long for database columnn and was truncated.'), 'warning' );
    }
    //write the record to the database
		drupal_write_record('emmaus_weekend_types', $form_state['values'], 'type_id');
		//set a confirmation message
		drupal_set_message(t('@name weekend type definition has been updated', array('@name' => $form_state['values']['type_name'])));
	}
	//Redirect the form for all functions, including the cancel
	$form_state['redirect'] = 'admin/config/emmaus/weekend/types';
}






 
 




