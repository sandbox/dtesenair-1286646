<?php
// $Id:  


/**
 * Form builder function to configure global settings for the prayerclock module.
 */
function emmaus_weekend_settings() {
	
  //define a textarea for default description settings when defining a new weekend
  $form['emmaus_weekend_default_duration'] = array(
    '#type' => 'textfield',
    '#title' => t('Weekend duration'),
    '#size' => '5',
    '#description' => t('Please enter the duration of the weekend activities expressed in hours.  Most Walk to Emmaus weekends are 72 hours while Chrysalis Flights are 50 hours.'),
    '#default_value' => variable_get('emmaus_weekend_default_duration', 72),
  );
  $form['emmaus_weekend_default_description'] = array(
    '#type' => 'textarea',
    '#title' => t('Default "Description"'),
    '#description' => t('You may enter a default description that will populate when defining new weekends in your community.'),
    '#default_value' => variable_get('emmaus_weekend_default_description'),
  );
  $form['emmaus_weekend_default_location'] = array(
    '#type' => 'textfield',
    '#title' => t('Default Location'),
    '#description' => t('You may enter a default location name that will populate when defining new weekends in your community.'),
    '#default_value' => variable_get('emmaus_weekend_default_location'),
  );

  $form['emmaus_weekend_default_address'] = array(
    '#type'=>'textfield',
    '#title'=>'Default Address',
    '#description' => t('You may enter a default address that will populate when defining new weekends in your community.'),
    '#default_value' => variable_get('emmaus_weekend_default_address'),
    );
	// Return the form
	return system_settings_form($form);
}

