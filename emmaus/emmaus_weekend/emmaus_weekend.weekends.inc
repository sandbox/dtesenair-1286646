<?php 
// $Id:  



 
/*************************************************************************************************************/
/*** View builder function(s) to display weekend details *****************************************************/
/*************************************************************************************************************/
function emmaus_weekend_view($weekend) { 
  drupal_set_title($weekend['weekend_name']);


  //drupal_set_message('<pre>' . print_r($weekend, true) . '</pre>');

	//format special fields such as date and boolean fields
	$start_date = format_date($weekend['start_date'], 'custom', 'M d, Y');
  //Convert active integer to image
  $active = '';
  if ( $weekend['active'] == 1 ) {
    $active = '<img alt="Yes" src="/'. drupal_get_path('module','emmaus_community') . '/images/check.png">';
  } else {
    drupal_set_message('This weekend is currently closed!', 'warning');
  }
  //Convert open integer to image
  $open = '';
  if ( $weekend['open'] == 1 ) {
    $open = '<img alt="Yes" src="/'. drupal_get_path('module','emmaus_community') . '/images/check.png">';
  }

  //Insert a google map
  $markup = '<div style="float: right; margin: 10px;">';
  //$markup .= '<iframe width="350" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=4990+Picciola+Road+Fruitland+Park,+FL+34731&amp;aq=&amp;sll=28.035215,-80.676433&amp;sspn=0.015265,0.033023&amp;vpsrc=0&amp;ie=UTF8&amp;hq=&amp;hnear=4990+Picciola+Rd,+Fruitland+Park,+Florida+34731&amp;ll=28.852868,-81.870375&amp;spn=0.022553,0.025749&amp;z=14&amp;iwloc=A&amp;output=embed"></iframe><br /><small><a href="http://maps.google.com/maps?f=q&amp;source=embed&amp;hl=en&amp;geocode=&amp;q=4990+Picciola+Road+Fruitland+Park,+FL+34731&amp;aq=&amp;sll=28.035215,-80.676433&amp;sspn=0.015265,0.033023&amp;vpsrc=0&amp;ie=UTF8&amp;hq=&amp;hnear=4990+Picciola+Rd,+Fruitland+Park,+Florida+34731&amp;ll=28.852868,-81.870375&amp;spn=0.022553,0.025749&amp;z=14&amp;iwloc=A" style="color:#0000FF;text-align:left">View Larger Map</a></small>';
  $markup .= '<iframe width="250" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="http://maps.google.com/maps?q=' . $weekend['location'] . ' ' . $weekend['address'] . '&amp;output=embed"></iframe><br />';
  //$markup .= '<small><a target="_blank" href="http://maps.google.com/maps?q=' . $weekend['location'] . ' ' . $weekend['address'] . '" style="color:#0000FF;text-align:left">View Larger Map</a></small>';
  $markup .= '</div>';
  //Output fields
  $markup  .= '<p><b>Date:</b> ' . $start_date . '</p>';
  $markup  .= '<p><b>Type:</b> ' . $weekend['weekend_type'] . '</p>';
  $markup .= '<p><b>Location:</b> ' . $weekend['location'] . '<br/><b>Address:</b> ' . $weekend['address'] . '</p>';
	$markup .= '<b>Gender: </b>' . $weekend['gender'] . '</p>' ;
	$markup .= '<p><b>Active: </b>' . $active . '<br/><b>Open: </b>' . $open . '<br/></p>';
  $markup .= '<p>' . $weekend['description'] . '</p>';
  
  //If the 'team' module is enabled, display the table here
  if (module_exists('emmaus_team')==TRUE && user_access('view team')==TRUE     ) {
    //module_load_include('inc', 'emmaus_team', 'emmaus_team.selection');
    //Return a 'drupal_render'ed output
    $team_markup['fieldset'] = array( '#type'=>'fieldset', '#title'=>'<b>Team Members for ' . $weekend['weekend_name'] . '</b>',  '#collapsible'=>TRUE);
    //$team_markup['fieldset']['markup_clergy'] = array( '#markup'=>views_embed_view('emmaus_team','clergy_attachment')  );
    //$team_markup['fieldset']['markup_lay'] = array( '#markup'=>views_embed_view('emmaus_team','lay_attachment')  );
    //$team_markup['fieldset']['markup_clergy'] = array( '#markup'=>views_embed_view('emmaus_team','team_export_csv')  );
    $team_markup['fieldset']['markup_team'] = array( '#markup'=>views_embed_view('emmaus_team','team_display_page')  );
    $markup .= '<div style="clear: both;">' . drupal_render($team_markup) . '</div>';
  }
  
  //If the 'prayerclock' module is enabled, embed it's default view
  if (module_exists('emmaus_prayerclock')==TRUE && user_access('view prayerclock')==TRUE     ) {
    $pc_markup['fieldset'] = array( '#type'=>'fieldset', '#title'=>'<b>Prayer Clock for ' .$weekend['weekend_name'] .'</b>' ,  '#collapsible'=>TRUE);
    //$pc_markup['fieldset']['markup'] = array( '#markup'=>views_embed_view('prayerclock_export', 'prayerclock_block', $weekend['weekend_number']));
    $pc_markup['fieldset']['markup'] = array( '#markup'=>views_embed_view('emmaus_prayerclock','prayerclock_page') );
    $markup .= '<div style="clear: both;">' . drupal_render($pc_markup) .'</div>'  ;
  }

	//return the markedup page
	return $markup;
}





/*************************************************************************************************************/
/*** Form builder function(s) to add a new church **********************************************************/
/*************************************************************************************************************/
function emmaus_weekend_add() {
  //Get the list of weekend type before building the form
  $types = _emmaus_weekend_types_get();
  
	// Define the form elements
  $form['weekend_number'] = array( 
    '#type' => 'textfield',	
    '#title' => t('Number'),			
    '#required' => TRUE,  
    '#size' => 10, 	
    '#maxlength' => 10, 		
    '#description' => t('Enter a number for this weekend.</em>')   					
    );
  $form['start_date']	= array( 
    '#type' => 'date',
    '#title' => t('Date'),
    '#required' => TRUE,
    '#description' => t('Enter the date that this weekend starts.') 
    );
  $form['term_start'] = array( 
    '#type' => 'date_popup',
    '#date_format'=>'F d, Y',
    '#title' => t('Term Start'),
    '#required' => TRUE,
    '#default_value' => isset($assignment['term_start']) ? date('Y-m-d', $assignment['term_start']) : NULL ,
    //($var > 2 ? true : false)
    
    '#date_label_position' => 'within',                                                         
    );
	$form['gender'] = array( 
	 '#type' => 'select', 	 	
	 '#title' => t('Gender'),  		
	 '#required' => TRUE,  
	 '#options' => array(	'M'=>t('Male'), 'F'=>t('Female')	)
   );
	$form['weekend_type'] = array( 
	 '#type' => 'select', 	 	
	 '#title' => t('Type'),  			
	 '#required' => TRUE,  
	 '#options' => $types, 
   );
  $form['weekend_name'] = array( 
    '#type' => 'textfield',	
    '#title' => t('Name'),				
    '#required' => TRUE, 
    '#size' => 80, 
    '#maxlength' => 255, 
    '#description' => t('Enter a name for this weekend (e.g. - <em>Men\'s Walk to Emmaus #12</em>)')   			
    );
  $form['description'] = array( 
    '#type' => 'textarea',	  
    '#title' => t('Description'),	
    '#required' => FALSE, 
    '#rows' => 4, 
    '#maxlength' => 2046, 
    '#description' => t('<em>Optional.</em>  Enter a description of the weekend.'), 
    '#default_value'=>variable_get('emmaus_weekend_default_description')   								
    );
  $form['location'] = array( 
    '#type' => 'textfield',	
    '#title' => t('Location'),		
    '#required' => TRUE, 	
    '#maxlength' => 255, 
    '#description' => t('Enter a descriptive location name where the weekend will be held.'), 
    '#autocomplete_path' => 'emmaus/weekend/locations/autocomplete',   			
    '#default_value'=>variable_get('emmaus_weekend_default_location') 
    );
  $form['address'] = array( 
    '#type' => 'textfield',	
    '#title' => t('Address'),			
    '#required' => FALSE, 
    '#maxlength' => 255, 
    '#description' => t('Enter the physical adress where the weekend will be held.'),   							
    '#default_value'=>variable_get('emmaus_weekend_default_address') 
    );
  $form['options_label']	= array( '#type' => 'item', 			'#title' => t('Options'),  										);
  //$form['is_spanish']			= array( '#type' => 'checkbox',		'#title' => t('Spanish Speaking Weekend?'),		);
  $form['active']					= array( '#type' => 'checkbox',		'#title' => t('Active?'),											);
  $form['open']						= array( '#type' => 'checkbox',		'#title' => t('Open for Registration?'),			);
	//Submit button
  $form['submit'] = array( '#type' => 'submit', '#value' => 'Submit', );
	//Return the form
  return $form;
}
/****************************/
/*** ADD Validate handler ***/
/****************************/
function emmaus_weekend_add_validate($form, &$form_state) {
  //add a gender prefix to the weekend_number field if none was provided
  if (is_numeric($form_state['values']['weekend_number'])) {
    $form_state['values']['weekend_number'] == $form_state['values']['gender'] . $form_state['values']['weekend_number']; 
  }
  

}
/**************************/
/*** ADD Submit handler ***/
/**************************/
function emmaus_weekend_add_submit($form, &$form_state) {
	//Convert the date item into a string
	$start_date = $form_state['values']['start_date']['month'] . '/' . $form_state['values']['start_date']['day'] . '/' . $form_state['values']['start_date']['year'];

	//Generate the insert query
  db_insert('emmaus_weekend_weekends')
      ->fields(array(
						'weekend_number'	=> $form_state['values']['weekend_number'],
            'weekend_name'    => $form_state['values']['weekend_name'],
            'weekend_type'    => $form_state['values']['weekend_type'],
						'description' 		=> $form_state['values']['description'],
						'start_date' 			=> strtotime($start_date),
						'gender' 					=> $form_state['values']['gender'],
						'location'		 		=> $form_state['values']['location'],
						'address' 				=> $form_state['values']['address'],
						'active' 					=> $form_state['values']['active'],
						'open' 						=> $form_state['values']['open'],
						//'is_spanish' 			=> $form_state['values']['is_spanish'],
					)
				)
      ->execute();
	//Set a confirmation message
  drupal_set_message(t('The weekend <em>@name</em> has been added to the community.', array('@name' => $form_state['values']['weekend_name'])));
	//Redirect the form
	$form_state['redirect'] = 'emmaus/weekend';
}




/*************************************************************************************************************/
/*** EDIT: Form builder - function(s) to edit an existing board member entry *********************************/
/*************************************************************************************************************/
function emmaus_weekend_edit($form, &$form_state, $params) {
	//Check to see if we should display the confirmation form
	if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Delete') {
		// Define form elements again, prior to final submission
		$form['weekend_number']		= array( '#type' => 'textfield', '#title' => t('Weekend Number'), 		'#default_value' => $form_state['values']['weekend_number']   	);
	  $form['weekend_name']	= array( '#type' => 'textfield', '#title' => t('Weekend Name'), 	'#default_value' => $form_state['values']['weekend_name']   );
		//Display the confirmation form
		return confirm_form($form, t('Are you sure that you want to delete the @name weekend?', array('@name'=>$form_state['values']['weekend_name'])), 'admin/emmaus/weekend', 'This action can not be undone.  Please proceed with caution.', 'Remove', 'Don\'t Remove'); //Had better luck leaving off last param 'name'  
	}	//confirmation form
	
	
	//Get the list of weekend type before building the form
	$types = _emmaus_weekend_types_get();
  
	//Query the database for the record to edit
  $result = db_select( 'emmaus_weekend_weekends', 'w' )
  	->fields( 'w')
		->condition( 'w.weekend_number',$params )
		->execute();
	//Make sure only one entry was returned.  If not, display an error
 	//TODO:  Make a prettier form when an error is returned.  Perhaps add a link to the list page.
	if ( !$result->rowcount() == 1 ) {
		drupal_set_message(  t('Unable to locate the weekend specified in the URL (Param: @param).  Please try your query again. (Rows returned: @number)' , array('@number'=>$result->rowcount(), '@param'=>$params )) ,'error');
		return '';
	}

	//One result confirmed.  Set that row into a variable
	foreach ($result as $row) {	$weekend = $row; }

	//Format the date arrays
	$start_date = explode('/' , format_date( $weekend->start_date , 'custom', 'n/j/Y')  );
	$start_date = array( 'month'=>$start_date[0], 'day'=>$start_date[1], 'year'=>$start_date[2] );

	// Define the form elements
  $form['weekend_add'] 		= array( '#type' => 'item', 			'#title' => t('Edit existing weekend.'),  );
  $form['weekend_id'] 		= array( '#type' => 'hidden',			'#default_value'=> $weekend->weekend_id	);
  $form['weekend_number'] = array( '#type' => 'textfield',	'#title' => t('Number'),			'#required' => TRUE,  '#size' => 10, 	'#maxlength' => 10, 		'#default_value'=> $weekend->weekend_number,				'#description' => t('Enter a number for this weekend.  <em>Must be unique.</em>')   					);
	//TODO:  Make this a date picker
	$form['start_date'] 		= array( '#type' => 'date',				'#title' => t('Date'),				'#required' => TRUE,  																			'#default_value'=>$start_date ,	'#description' => t('Enter the date that this weekend starts.')   														);
	$form['gender'] 				= array( '#type' => 'select', 	 	'#title' => t('Gender'),  		'#required' => TRUE,  '#options' => array(	'M'=>t('Male'), 'F'=>t('Female')	), '#default_value'=> $weekend->gender,		  													);
	//TODO:  Update this with a call to the weekend_types table
	$form['weekend_type'] 	= array( '#type' => 'select', 	 	'#title' => t('Type'),  			'#required' => TRUE,  '#options' => $types,									'#default_value'=> $weekend->weekend_type,																											);
  $form['weekend_name'] 	= array( '#type' => 'textfield',	'#title' => t('Name'),				'#required' => FALSE, '#size' => 80, '#maxlength' => 255, 	'#default_value'=> $weekend->weekend_name,					'#description' => t('<em>Optional.</em>  Enter a name for this weekend.  Defaults to \'Gender + Type + Number\'.  <em>i.e. - Men\'s Walk to Emmaus #12</em>	')   			);
  $form['description']		= array( '#type' => 'textarea',  	'#title' => t('Description'),	'#required' => FALSE, '#rows' => 4,  '#maxlength' => 2046, 	'#default_value'=> $weekend->description,						'#description' => t('<em>Optional.</em>  Enter a description of the weekend.')   								);
  $form['location'] 			= array( '#type' => 'textfield',	'#title' => t('Location'),		'#required' => TRUE, 	'#size' => 80, '#maxlength' => 255, 	'#default_value'=> $weekend->location,							'#description' => t('Enter a descriptive location name where the weekend will be held.'), '#autocomplete_path' => 'emmaus/weekend/locations/autocomplete',   			);
  $form['address'] 				= array( '#type' => 'textfield',	'#title' => t('Address'),			'#required' => FALSE, '#size' => 80, '#maxlength' => 255, 	'#default_value'=> $weekend->address,								'#description' => t('Enter the physical adress where the weekend will be held.')   							);

  $form['options_label']	= array( '#type' => 'item', 			'#title' => t('Options'),  																																		);
  //$form['is_spanish']			= array( '#type' => 'checkbox',		'#title' => t('Spanish Speaking Weekend?'),		'#default_value'=> $weekend->is_spanish,				);
  $form['active']					= array( '#type' => 'checkbox',		'#title' => t('Active?'),											'#default_value'=> $weekend->active,						);
  $form['open']						= array( '#type' => 'checkbox',		'#title' => t('Open for Registration?'),			'#default_value'=> $weekend->open,							);
	//Define the Submit buttons
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit'), );
  $form['delete'] = array( '#type' => 'submit', '#value' => t('Delete'), );
  $form['cancel'] = array( '#type' => 'submit', '#value' => t('Cancel'), );
	//Return the form
 	return $form;
}
/***************************/
/* EDIT: Validate handler  */
/***************************/
function emmaus_weekend_edit_validate($form, &$form_state) {
}
/************************/
/* EDIT: Submit handler */
/************************/
function emmaus_weekend_edit_submit($form, &$form_state) {
	//Set the default form redirect
	$form_state['redirect'] = 'emmaus/weekend/' . $form_state['values']['weekend_number'] . '/view';

	//Delete button has been clicked initially.  Set a flag and rebuild the form.
	if (!isset($form_state['storage']['confirm']) && $form_state['values']['op']=='Delete' ) {
		$form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form      
		$form_state['rebuild'] = TRUE; // along with this    
	}
	//Deletion has been confirmed.  Process the queries.
	elseif (isset($form_state['storage']['confirm']) ) {
		$num_deleted = db_delete('emmaus_weekend_weekends')
			->condition('weekend_number', $form_state['values']['weekend_number'])
			->execute();
		drupal_set_message( t('<em>@name</em> weekend was deleted', array('@name' => $form_state['values']['weekend_name'])), 'status'  );
		$form_state['redirect'] = 'emmaus/weekend';
	}
	//Submit button was pressed.  Write the record.
	elseif ($form_state['values']['op']=='Submit') {
		//convert date field to timestamp before saving record
		$form_state['values']['start_date'] = strtotime( $form_state['values']['start_date']['month'] . '/' . $form_state['values']['start_date']['day'] . '/' . $form_state['values']['start_date']['year']      );
		//Write the record
		drupal_write_record('emmaus_weekend_weekends', $form_state['values'], 'weekend_id');
		drupal_set_message(t('@name weekend has been updated', array('@name' => $form_state['values']['weekend_name'])));
		$form_state['redirect'] = 'emmaus/weekend/' . $form_state['values']['weekend_number'] . '/view';
	}
}


 
 




/*********************************************************************************************/
/** BEGIN CUSTOM HELPER FUNCTIONS ************************************************************/
/*********************************************************************************************/




/********************************************************************************************************/
/*** AUTOCOMPLETE FUNCTIONS *****************************************************************************/
/********************************************************************************************************/
function emmaus_weekend_weekends_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $result = db_select('emmaus_weekend_weekends', 'w')
      ->fields('w', array('weekend_name'))
      ->condition('weekend_name', '%' . db_like($string) . '%', 'LIKE')
      ->range(0, 10)
      ->execute();
    //$result = db_query('SELECT DISTINCT weekend_name FROM {emmaus_weekend_weekends}');
    //Loop through the results
    foreach ($result as $weekend) {
      $matches[$weekend->weekend_name] = check_plain($weekend->weekend_name);
    }
  }
  //return the results in a json object
  drupal_json_output($matches);
}

function emmaus_weekend_locations_autocomplete($string = '') {
  $matches = array();
  if ($string) {
		$result = db_query('SELECT DISTINCT location FROM {emmaus_weekend_weekends}');
		//Loop through the results
    foreach ($result as $location) {
      $matches[$location->location] = check_plain($location->location);
			//$matches[$city->cityName . '|' . cityID] = $city->cityName;
    }
  }
	//return the results in a json object
  drupal_json_output($matches);
}

