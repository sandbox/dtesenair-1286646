<?php
// $Id:  


/**
 * Form builder function to configure global settings for the prayerclock module.
 */
function emmaus_pilgrim_settings() {
  $form['emmaus_pilgrim_apply_tag'] = array(  '#type'=>'textarea',  '#title'=>t('Header text for sponsor/pilgrim application form'),    '#default_value'=>variable_get('emmaus_pilgrim_apply_tag', ''),         );

	// Return the form
	return system_settings_form($form);
}


 
/**
* Function to retrieve and serialize schema this module
* TODO: Remove this function.  It is only for testing purposes
*/
function emmaus_pilgrim_admin_schema() {
	//$output = 'This is test output';
	//$output = drupal_get_schema('emmaus_board_primary',TRUE);
	$this_schema = drupal_get_schema('emmaus_pilgrim_applications',TRUE);
	$output = '<h3>Serialized array for "emmaus_pilgrim_applications" schema</h3><br/><br/>';
	$output .= '<pre>' . print_r( $this_schema, TRUE) . '</pre>'  ;

  $this_schema = drupal_get_schema('emmaus_pilgrim_sponsor_history',TRUE);
  $output .= '<h3>Serialized array for "emmaus_pilgrim_sponsor_history" schema</h3><br/><br/>';
  $output .=  print_r( $this_schema, TRUE)   ;


	return $output;
}
