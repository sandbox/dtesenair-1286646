<?php



/*************************************************************************************************************/
/*** Form builder function(s) to assign clergy to a team *****************************************************/
/*************************************************************************************************************/
function emmaus_team_clergy_form($form, &$form_state, $weekend) {
  //Set the page title 
  drupal_set_title( $weekend['weekend_name'] . ' - Assign Clergy' );
  
  //query the database for weekends
  $weekends = _emmaus_weekend_weekends( );
  //retrieve the list of talks and positions.
  $positions = array_merge( array(NULL=>'- None -') , _emmaus_team_positions('C') );
  $talks = array_merge( array(NULL=>'- None -') , _emmaus_team_talks('C') );

  //Query the database for a list of community profiles that are clergy
  $result = db_select('emmaus_community_profile', 'p'  )
    ->fields('p', array('profile_id') )
    ->condition('is_clergy','1')
    ->execute();
  //Loop through the results and create an $options array to pass to the tableselect form element
  //$options = array();
  //foreach ($clergy_results as $clergy) {
  //  $options[$clergy->profile_id] = array( 'full_name'=>$clergy->full_name, 'gender'=>$clergy->gender,  'church_name'=>$clergy->church_name, 'location'=>$clergy->city, 'num_weekends'=>'3', 'num_talks'=>'2', 'last_weekend'=>'Men\'s Walk to Emmaus #123'    );
  //}
  $clergy_list = array();
  foreach ($result as $row) {
    //load each user object so we have access to their history
    $profile = user_load($row->profile_id);    
    //grab the last weekend  
    $last_weekend = end($profile->emmaus_team_history['teams']);
    //build an array of profiles for the tableselect form element
    $clergy_list[ $profile->uid ] = array( 
      'full_name'=>$profile->emmaus_profile['full_name'], 
      'gender'=>$profile->emmaus_profile['gender'], 
      'church_name'=>$profile->emmaus_profile['church_name'], 
      'location'=> $profile->emmaus_profile['city'] . ', ' . $profile->emmaus_profile['state'], 
      'num_teams' => count($profile->emmaus_team_history['teams']), 
      'num_talks'=> count($profile->emmaus_team_history['talks']['primary']), 
      'talks_given' => implode(', ', array_keys($profile->emmaus_team_history['talks']['primary']) ),
      'positions_held' => implode(', ', array_keys($profile->emmaus_team_history['positions']) ),
      'last_weekend'=> $last_weekend['weekend_number'],
    );
    
    
  }



  //build a header for the table select element.  This will be passed to the form array
  $header = array(
    'full_name'   => t('Name'),
    'gender'      => t('Gender'),
    //'church_name' => t('Home Church'),
    'location'    => t('Location'),
    //'num_talks'   => t('# Talks'),
    'positions_held'=> t('Positions'),
    'talks_given'   => t('Talks Given'),
    'num_teams'   => t('Teams'),
    'last_weekend'=> t('Last Weekend')
    );


  //define the form elements
  $form['to-do'] = array( '#markup'=>'TO DO:  Pull real data from history table.'   );
  $form['profile_id'] = array( 
    '#type' => 'tableselect', 
    '#header' => $header, 
    '#options'=>$clergy_list, 
    '#empty' => t('No users found'), 
    '#required'=>TRUE, 
    '#multiple'=>FALSE  
    );
  $form['open_table'] = array(
    '#markup'=>'<table><tr>'
    );
  $form['weekend_number'] = array(
    '#type'=>'hidden', 
    '#default_value'=>$weekend['weekend_number'] 
    );
  $form['type'] = array(
    '#type'=>'hidden',   
    '#default_value'=>'C',  
    );
  $form['assigned_weekend'] = array(
    '#type'=>'select',   
    '#title'=>'Assign to Weekend',  
    '#default_value'=>$weekend['weekend_number'],  
    '#options'=>$weekends,  
    '#disabled'=>TRUE,
    '#prefix'=>'<td valign="top">',  
    '#suffix'=>'</td>'  
    );
  $form['assigned_position'] = array( 
    '#type'=>'select',   
    '#title'=>'Assign Position',
    '#options'=>$positions, 
    '#prefix'=>'<td valign="top">',  
    '#suffix'=>'</td>'  
    );
  $form['primary_talk'] = array(
    '#type'=>'select',
    '#title'=>'Assign Talk',
    '#options'=>$talks,
    '#prefix'=>'<td valign="top">',
    );
  $form['close_table'] = array( 
    '#markup'=>'</tr></table>'  
    );
 
 
  //define the submit button
  $form['submit'] = array(   '#type' => 'submit',   '#value' => t('Submit'), '#disabled'=>empty($clergy_list)  );

  //drupal_set_message('Form: <pre>' .  print_r($form, TRUE) . '</pre>' );

  //return the form
  return $form;    
}
/****************************/
/*** Validate handler *******/
/****************************/
function emmaus_team_clergy_form_validate($form, &$form_state) {
  if ($form_state['values']['assigned_weekend'] == '' ) {
    $form_state['values']['assigned_weekend'] == NULL;
    $form_state['values']['assigned_position'] == NULL;
    $form_state['values']['primary_talk'] == NULL;
    //$form_state['values']['talk_is_primary'] == NULL;
  }
}
/**************************/
/*** Submit handler *******/
/**************************/
function emmaus_team_clergy_form_submit($form, &$form_state) {
  //drupal_set_message('$form_state[\'values\'] = <pre>' . print_r($form_state['values'], TRUE) . '</pre>'   );

  //check to see if name and email are empty, first.  if so, set them to NULL.
  if ($form_state['values']['assigned_weekend'] == '' )  {
    $form_state['values']['assigned_weekend'] = NULL;
    $form_state['values']['assigned_position'] = NULL;
    $form_state['values']['primary_talk'] = NULL;
    //$form_state['values']['talk_is_primary'] = NULL;
  }
  
  //Loop through all form values and set them to NULL if they are an empty string
  foreach ( $form_state['values'] as &$value ) {
    if (empty($value) ) { $value = NULL; }    
  }
  //Write the record to the database
  drupal_write_record('emmaus_team_leadership', $form_state['values'] );
  //Set a confirmation message
  $profile_id = $form_state['values']['profile_id'];
  drupal_set_message(t('@name has been assigned to @weekend', array( '@name'=>$form['profile_id']['#options'][$profile_id]['full_name'],    '@weekend'=>$form_state['values']['assigned_weekend']  )   )   );
  //Should we redirect the form
  if ( !empty($form_state['values']['weekend_number']) ) {
    $form_state['redirect'] = 'emmaus/weekend/' . $form_state['values']['assigned_weekend'] . '/edit/team' ;    
  }
  
}





/*************************************************************************************************************/
/*** Form builder function(s) to assign leadership to a team *************************************************/
/*************************************************************************************************************/
function emmaus_team_leadership_form($form, &$form_state, $weekend=NULL) {


  //Set up the form based on whether or we are adding or editing 
  switch ($weekend) {
    case NULL:  //Leadership selection report page.  No default weekend specified.
      break;
    default:  //else a default weekend was specified
      //Set the page title 
      drupal_set_title( $weekend['weekend_name'] . ' - Assign Leadership' );
      $form['weekend_number'] = array('#type'=>'hidden', '#default_value'=>$weekend['weekend_number'] );
      break;
  }

  //query the database for weekends
  if (empty($weekend)) {
    $weekends = _emmaus_weekend_weekends( NULL, 1 );
  } else {
    $weekends = _emmaus_weekend_weekends( );    
  }

  //retrieve the list of talks and positions and create other form element options
  $talks = array_merge( array(NULL=>'- None -') , _emmaus_team_talks('L') );
  $search_pos = array_merge( array(NULL=>'- None -') , _emmaus_team_positions('L'), _emmaus_team_positions('I'), _emmaus_team_positions('O') );
  $assign_pos = array_merge( array(NULL=>'- None -') , _emmaus_team_positions('L')  );
  $count_option = array(0,1,2,3,4,5,6,7,8,9,10);
  $gender_option = array(NULL=>'- None -', 'M'=>'Male', 'F'=>'Female');
  
  //Define the search form elements
  $form['search'] = array( '#type'=>'fieldset', '#title'=>'Search Parameters'  );
  $form['search']['open_table']       = array( '#markup'=>'<table><tr>'  );
  $form['search']['search_min_teams'] = array('#type'=>'select',  '#title'=>'Min Teams',     '#options'=>$count_option,  '#default_value'=>1, '#prefix'=>'<td valign="top" width="20%">',  '#suffix'=>'</td>'  );
  $form['search']['search_gender']    = array('#type'=>'select',  '#title'=>'Gender',        '#options'=>$gender_option, '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );
  $form['search']['search_position']  = array( '#type'=>'select', '#title'=>'Position Held', '#options'=>$search_pos,    '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );
  $form['search']['search_talk']      = array( '#type'=>'select', '#title'=>'Talk Given',    '#options'=>$talks,         '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );
  $form['search']['row2'] = array( '#markup'=>'</tr><tr>' );
  $form['search']['search'] = array( '#type'=>'submit',  '#value'=>'Search', '#submit'=>array('emmaus_team_leadership_form_submit'), '#limit_validation_errors'=>array() , '#prefix'=>'<td valign="bottom">',  );
  $form['search']['reset'] = array( '#type'=>'submit',   '#value'=>'Reset',  '#submit'=>array('emmaus_team_leadership_form_submit'), '#limit_validation_errors'=>array() , '#suffix'=>'</td>'  );
  $form['search']['close_table']      = array( '#markup'=>'</tr></table>' );

  //**Query the database for a list of community profiles that are not clergy
  //@TODO: Check out this query because I don;t think it needs the 'COUNT' function   
  $select = db_select('emmaus_community_profile', 'p'  );
    $select->fields('p', array('profile_id'));
    $select->leftJoin('emmaus_team_history', 'h', 'p.profile_id=h.profile_id'  );
    $select->addExpression( 'COUNT(h.history_id)', 'num_teams' );
    $select->groupBy('p.profile_id');
    $select->orderBy('num_teams', 'DESC');
    $select->condition('p.is_clergy','0');;
  $result = $select->execute();


  //Loop through the result and create an array of rows
  $results = array();
  foreach ($result as $row) {
    //load each user object so we have access to their history
    $profile = user_load($row->profile_id);    
    
    //apply search parameters for minimum teams
    if( !empty( $form_state['input']['search_min_teams']) && $profile->emmaus_team['weekends']['count']  < $form_state['input']['search_min_teams'] ) {
      continue;
    }
    //apply search parameters for gender
    if( !empty( $form_state['input']['search_gender']) && $profile->emmaus_profile['gender'] != $form_state['input']['search_gender'] ) {
      continue;
    }
    //apply search parameters for position
    if( !empty( $form_state['input']['search_position']) && !in_array( $form_state['input']['search_position'], array_keys($profile->emmaus_team['positions']) ) ) {
      continue;
    }
    //apply search parameters for talks
    if( !empty( $form_state['input']['search_talk']) && !in_array( $form_state['input']['search_talk'], array_keys($profile->emmaus_team['talks']['primary']) ) ) {
      continue;
    }
   
   
    //set the last_weekend 
    $last_weekend = end($profile->emmaus_team['weekends']['local']); 
    //convert 'special_needs' field to an image
    $special_needs = '';
    if ( !empty($profile->emmaus_profile['special_needs'])) {
      $special_needs = '<img title="' . $profile->emmaus_profile['special_needs'] .  '" alt="Special Needs" src="/'. drupal_get_path('module','emmaus_community') . '/images/special_needs.png">';
    }
    
    
    $results[ $profile->uid ] = array( 
      'full_name'=> '<a href="/user/' .  $profile->uid . '">' .    $profile->emmaus_profile['full_name'] . '</a>', 
      'gender'=>$profile->emmaus_profile['gender'], 
      'church_name'=>$profile->emmaus_profile['church_name'], 
      'location'=> $profile->emmaus_profile['city'] . ', ' . $profile->emmaus_profile['state'], 
      'num_teams' => $profile->emmaus_team['weekends']['count'], 
      'num_talks'=> count($profile->emmaus_team['talks']['primary']), 
      'talks_given' => implode(', ', array_keys($profile->emmaus_team['talks']['primary']) ),
      'positions_held' => implode(', ', array_keys($profile->emmaus_team['positions']) ),
      'last_weekend'=> $last_weekend['weekend_number'],
      'special_needs' => $special_needs,
      //'special_needs' => $profile->emmaus_profile['special_needs'],
      );
  
  } //foreach



  
  //build a header for the table select element.  This will be passed to the form array
  $header = array(
    'full_name'   => t('Name'),
    'gender'      => t('Gender'),
    //'church_name' => t('Home Church'),
    'location'    => t('Location'),
    //'num_talks'   => t('# Talks'),
    'positions_held'=> t('Positions'),
    'talks_given'   => t('Talks Given'),
    'num_teams'   => t('Teams'),
    'last_weekend'=> t('Last Weekend'),
    'special_needs'=> t('Needs'),
    );

  //define the form elements or the assignment form
  $form['profile_id'] = array( 
    '#type' => 'tableselect', 
    '#header' => $header, 
    '#options'=>$results, 
    '#empty' => t('No users found'), 
    //'#required'=>TRUE, 
    '#multiple'=>FALSE  
    );
  $form['open_table'] = array(
    '#markup'=>'<table><tr>'
    );
  $form['type'] = array(
    '#type'=>'hidden',
    '#default_value'=>'L'
  );
  $form['assigned_weekend'] = array(
    '#type'=>'select',  
    '#required'=>TRUE,
    '#title'=>'Assigned Weekend',
    '#options'=>$weekends,
    '#disabled'=> !empty($weekend),   
    '#prefix'=>'<td valign="top">',  
    '#suffix'=>'</td>'  
    );
  if ( !empty($weekend) ) { $form['assigned_weekend']['#default_value'] = $weekend['weekend_number'];}
  
  $form['assigned_position'] = array( 
    '#type'=>'select',   
    '#title'=>'Assign Position',
    '#options'=>$assign_pos, 
    '#prefix'=>'<td valign="top">',  
    '#suffix'=>'</td>'  
    );
  $form['primary_talk'] = array(
    '#type'=>'select',
    '#title'=>'Primary Talk',
    '#options'=>$talks,
    '#prefix'=>'<td valign="top">',  
    '#suffix'=>'</td>'  
    );
  $form['backup_talk'] = array(
    '#type'=>'select',
    '#title'=>'Backup Talk',
    '#options'=>$talks,
    '#prefix'=>'<td valign="top">',  
    '#suffix'=>'</td>'  
    );
  $form['close_table'] = array( 
    '#markup'=>'</tr></table>'  
    );
  //define the submit button
  $form['submit'] = array(   '#type' => 'submit',   '#value' => t('Submit'), '#disabled'=>empty($results)  );
  //return the form
  return $form;    
}
/****************************/
/*** Validate handler *******/
/****************************/
function emmaus_team_leadership_form_validate($form, &$form_state) {
  //make sure a profile was selected
  if ( $form_state['values']['op'] == 'Submit' && empty($form_state['values']['profile_id']) ) {
    form_set_error('profile_id', 'You must select a user profile.');
  } 
  
}
/**************************/
/*** Submit handler *******/
/**************************/
function emmaus_team_leadership_form_submit($form, &$form_state) {
  //drupal_set_message('<pre>' . print_r($form_state['input'], TRUE) . '</pre>');
  
  
  //Clear button has been clicked.
  if ( $form_state['values']['op']=='Reset' ) {
    $form_state['input']['search_min_teams'] = 0;
    $form_state['input']['search_gender'] = NULL;
    $form_state['input']['search_position'] = NULL;
    $form_state['input']['search_talk'] = NULL;
    $form_state['rebuild'] = TRUE;
    return;
  }
  
  //Search button has been clicked initially.  Set a flag and rebuild the form.
  if ( $form_state['values']['op']=='Search' ) {
    $form_state['rebuild'] = TRUE;
    return;
  }
      
  //Before we write to the db, set any form values to literal NULL if they are empty strings.
  foreach ( $form_state['values'] as &$value ) {
    if (empty($value) ) { $value = NULL; }    
  }
  //Write the record to the database
  drupal_write_record('emmaus_team_leadership', $form_state['values'] );
  //Set a confirmation message
  $profile_id = $form_state['values']['profile_id'];
  drupal_set_message(t('@name has been assigned to @weekend', array( '@name'=>$form['profile_id']['#options'][$profile_id]['full_name'],    '@weekend'=>$form_state['values']['assigned_weekend']  )   )   );
  //Should we redirect the form
  if ( !empty($form_state['values']['weekend_number']) ) {
    $form_state['redirect'] = 'emmaus/weekend/' . $form_state['values']['assigned_weekend'] . '/edit/team' ;    
  }

}
