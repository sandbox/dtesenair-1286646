<?php
// $Id:

/*
 * @file
 * Administrative functions for the emmaus_team module
 */


/**
 * Form builder function to configure global settings for the emmaus_team module.
 */
function emmaus_team_admin_settings() {
  //define form elements and varianles for team module settings
  $form['emmaus_team_max_applications'] = array(
    '#type'=>'textfield',
    '#title'=>'Maximum simultaneous applications per user',
    '#description'=>'The maximum number of simultaneous team applications that each user can have active at a time (this includes applications that are currently assigned to a team).  For unlimited, enter 0.',
    '#size'=>5,
    '#default_value' => variable_get('emmaus_team_max_applications', 1),       
  );

  //Notification CC  
  $form['emmaus_team_notify_address'] = array(
    '#type'=>'textfield',
    '#title'=>'Team Coordinator Email',
    '#description'=>'A copy of all team notification emails will be sent to this address.',
    '#size'=>50,
    '#default_value' => variable_get('emmaus_team_notify_address', variable_get('site_mail') ),       
  );
  
  //New application emails template
  $form['title'] = array(
    '#type'=>'item',
    '#title'=>'Email Templates'
  );
  $form['email'] = array(
    '#type'=>'vertical_tabs',
    '#title'=>'Email Templates'
  );
  
  $form['email']['new-application'] = array(
    '#type'=>'fieldset',
    '#title'=>'New Application Entered'
  );
  $form['email']['new-application']['descrioption'] = array(
    '#markup'=>'This email template will be set when a new application is entered into the system.'
  );
  $form['email']['new-application']['subject'] = array(
    '#type'=>'textfield',
    '#title'=>'Subject'
  );
  $form['email']['new-application']['body'] = array(
    '#type'=>'textarea',
    '#title'=>'Body'
  );
  
  $form['email']['mod-application'] = array(
    '#type'=>'fieldset',
    '#title'=>'Existing Application Modified'
  );
  $form['email']['mod-application']['subject'] = array(
    '#type'=>'textfield',
    '#title'=>'Subject'
  );
  $form['email']['mod-application']['body'] = array(
    '#type'=>'textarea',
    '#title'=>'Body'
  );
  
  $form['email']['leadership-assigned'] = array(
    '#type'=>'fieldset',
    '#title'=>'Leadership Assignment Made'
  );
  $form['email']['leadership-assigned']['subject'] = array(
    '#type'=>'textfield',
    '#title'=>'Subject'
  );
  $form['email']['leadership-assigned']['body'] = array(
    '#type'=>'textarea',
    '#title'=>'Body'
  );
  
  
  
  
  // Return the form
  return system_settings_form($form);
}
/***************************/
/* Validate handler  *******/
/***************************/
function emmaus_team_admin_settings_validate($form, &$form_state) {
  //make sure the max applications entry is numeric
  if (is_numeric($form_state['values']['emmaus_team_max_applications'])==FALSE ) {
    form_set_error('emmaus_team_max_applications', 'Please enter a numeric value for maximum applications.');
  }
}
/***************************/
/* Submit handler  *********/
/***************************/
function emmaus_team_admin_settings_submit($form, $form_state) {
  //TODO:  Find out if this function call is PRE or POST form validation.  I think it's POST.
}





/*************************************************************************************************************/
/*** Table/List builder function(s) to display existing positions in admin section ***************************/
/*************************************************************************************************************/
function emmaus_team_positions_list() { 
  //Set the tag line
  $output = '<h3><p>Current positions defined for the ' . variable_get('emmaus_community_name') . '.</p></h3>';
  $output .= '<p>From this page, you can add, delete, or modify a weekend position definition.</p>';
  //Define the table header
  $header = array(
    array('data' => 'Edit', 'width'=>'5%'  ),   //edit
    //array('data' => '', 'width'=>'40'  ),   //contact
    array('data' => 'Weight', 'width'=>'5%'     ),
    array('data' => 'Code',  'width'=>'7%'  ),
    array('data' => 'Type', 'width'=>'7%'     ),
    array('data' => 'Position Name'      ),
    array('data' => 'Description'      ),
//    array('data' => 'Team Selection',  'align'=>'center', 'width'=>'40'     ),
  );
  //Query the database
  $result = db_select('emmaus_team_positions', 't')
    ->fields('t')
    ->extend('TableSort')         //Extend the query to include table sorting
    ->orderBy('weight')
    ->execute();
  //Loop through the result and create an array of rows
  $rows = array();
  foreach ($result as $row) {   
    //create links
    $edit_link = '<a href="/admin/config/emmaus/team/positions/' . $row->position_id . '/edit"><img border="0" alt="Edit" title="Edit this position" src="/'. drupal_get_path('module','emmaus_community') . '/images/edit.png"></a>';
    $delete_link = '<a href="/admin/config/emmaus/team/positions/' . $row->position_id . '/delete"><img border="0" alt="Edit" title="Delete this position" src="/'. drupal_get_path('module','emmaus_community') . '/images/drop.png"></a>';
    //format the leadership column
    $grant_rights = array('data'=>'','align'=>'center');
//    if ( $row->grant_rights == 1 ) {
//      $grant_rights = array( 'data'=>'<img border="0" alt="Clergy" src="/'. drupal_get_path('module','emmaus_community') . '/images/check.png">', 'align'=>'center' );
//    }
    
    //output the table rows
    $rows[] = array($edit_link, $row->weight, $row->position_code, $row->type, $row->position_name, $row->description, $grant_rights );
  }
  //output the table
  $tbl_vars = array(  'header'=>$header,  'rows'=>$rows,  'attributes'=>array(),  'caption' => $result->rowCount() . ' row(s) returned.',   'colgroups'=>array(),   'sticky'=>TRUE,   'empty'=>'No rows returned from query'  );
  $output .= theme_table($tbl_vars);
  //output a link to add a new assignment
  //$output .= '<a href="/admin/emmaus/team/settings/positions/add"><img src="/'. drupal_get_path('module','emmaus_team') . '/images/add.png">Add a new position</a>';
  //Return the output
  return $output;
}


/******************************************************************************************************************/
/*** POSITIONS: Form builder function(s) to manage positions ******************************************************/
/******************************************************************************************************************/
function emmaus_team_positions_form($form, &$form_state, $params=NULL) {
  //Check to see if we should display the confirmation form after the delete button has been pressed
  if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Delete') {
    // Define form elements again, prior to final submission
    $form['position_id']   = array( '#type' => 'hidden', '#default_value' => $form_state['values']['position_id']   );
    $form['position_name'] = array( '#type' => 'hidden', '#default_value' => $form_state['values']['position_name'] );
    //Display the confirmation form
    return confirm_form( 
      $form, 
      t('Are you sure that you want to delete the position <em>@name</em>?', array( '@name'=>$form_state['values']['position_name'] )  ), 
      'admin/config/emmaus/team/positions/' . $form_state['values']['position_id'] . '/edit', 
      'This action can not be undone.  Please proceed with caution.', 
      'Delete', 
      'Don\'t Delete'
    );  
  } // end of delete confirmation form
  
  
  //Set up the form based on whether or we are adding or editing 
  switch ($params) {
    case NULL:  //submitting a new 
      drupal_set_title(t('Add New Team Position'));
      //set default values for the form elements
      $default_values = array ( 'position_id'=>NULL, 'position_name'=>NULL, 'position_code'=>NULL, 'description'=>NULL, 'type'=>NULL, 'grant_rights'=>NULL, 'weight'=>NULL );    
      break;
    case is_numeric($params): //editing an existing application
      drupal_set_title( t('Edit Existing Team Position') );
      //Query the database for the record to edit
      $default_values = db_select( 'emmaus_team_positions', 'p' )
        ->fields('p')
        ->condition( 'p.position_id', $params )
        ->execute()
        ->fetchAssoc();
      //add a delete button to the bottom of the form
      $form['delete'] = array(  '#type'=>'submit', '#value'=>'Delete',  '#weight'=>90  );
      break;
   }  
  
  //Define an array of position types
  $types = array( 'C'=>'Clergy', 'L'=>'Lay Leadership', 'I'=>'Inside Team', 'O'=>'Outside Team'   );
  // Define all the form elements
  $form['position_id'] = array( 
    '#type'=>'hidden',     
    '#default_value' => $default_values['position_id'],         
    );
  $form['position_name'] = array( 
    '#type'=>'textfield',  
    '#title'=>t('Position Name'),         
    '#required' => TRUE,  
    '#size' => 50,  
    '#maxlength' => 50,   
    '#default_value' => $default_values['position_name'],     
    '#description' => t('Enter a name of this position.')       
    );
  $form['position_code'] = array( 
    '#type'=>'textfield',  
    '#title'=>t('Position Code'),         
    '#required' => TRUE,  
    '#size' => 4,  
    '#maxlength' => 5,   
    '#default_value' => $default_values['position_code'],     
    '#description' => t('Enter a short abbreviation or code to represent this position in history reports.')       
    );
  $form['description'] = array( 
    '#type'=>'textarea',   
    '#title'=>t('Description'),           
    '#default_value'=>$default_values['description'], 
    '#description'=>t('Optional.  Describe the primary duties of this position.')          
    );     
  $form['type'] = array( 
    '#type'=>'select',     
    '#title'=>t('Position Type?'),
    '#required'=>TRUE,        
    '#default_value'=>$default_values['type'],   
    '#options'=>$types,        
    '#description'=>t('Select the position type')  
    );    
/*  $form['grant_rights'] = array( 
    '#type'=>'checkbox',   
    '#title'=>t('Grant Team Selection?'), 
    '#default_value'=>$default_values['grant_rights'],                      
    '#description'=>t('Grant additional rights to team selection for an assigned weekend?')  
    );  */    
  $form['weight'] = array( 
    '#type'=>'weight',     
    '#title'=>t('Weight'),                
    '#default_value'=>$default_values['weight'],  
    '#delta'=>20,             
    '#description' => t('Optional. A numeric value to sort the positions.')  
    );  
  //Define the Submit buttons
  $form['submit'] = array( 
    '#type' => 'submit', 
    '#value' => t('Submit'), 
    );
  //$form['cancel'] = array( '#type' => 'submit', '#value' => t('Cancel'), );
  //Return the form
  return $form;
}
/********************************/
/* POSITIONS: Validate handler  */
/********************************/
function emmaus_team_positions_form_validate($form, &$form_state) {
}
/*****************************/
/* POSITIONS: Submit handler */
/*****************************/
function emmaus_team_positions_form_submit($form, &$form_state) {
  //Delete button has been clicked initially.  Set a flag and rebuild the form.
  if (!isset($form_state['storage']['confirm']) && $form_state['values']['op']=='Delete' ) {
    $form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form      
    $form_state['rebuild'] = TRUE; // along with this    
  }
  //Deletion has been confirmed.  Process the queries.
  elseif (isset($form_state['storage']['confirm']) ) {    
    //Delete the history row
    $num_deleted = db_delete('emmaus_team_positions')
      ->condition('position_id', $form_state['values']['position_id'])
      ->execute();
    drupal_set_message( t('<em>@name</em> position was deleted.', array('@name'=>$form_state['values']['position_name'] ) ), 'status'  );
  }
  //Submit button was pressed.  Write the record.
  elseif ($form_state['values']['op']=='Submit') {
  
    if ( $form_state['values']['position_id'] == '' ) {
      drupal_write_record('emmaus_team_positions', $form_state['values'] );
      drupal_set_message(t('Position <em>@name</em> has been added.', array('@name'=>$form_state['values']['position_name'] ) ) );
    } else {
      drupal_write_record('emmaus_team_positions', $form_state['values'], 'position_id');
      drupal_set_message(t('Position <em>@name</em> has been edited.', array('@name'=>$form_state['values']['position_name'] ) ) );
    }
  }
  //Redirect the form for all functions, including the cancel
  $form_state['redirect'] = 'admin/config/emmaus/team/positions';
}






/*************************************************************************************************************/
/*** Table/List builder function(s) to display existing talks in admin section *******************************/
/*************************************************************************************************************/
function emmaus_team_talks_list() { 
  //Set the tag line
  $output = '<h3><p>Current talks defined for the ' . variable_get('emmaus_community_name') . '.</p></h3>';
  $output .= '<p>From this page, you can add, delete, or modify a weekend talk definition.</p>';
  //Define the table header
  $header = array(
    array('data' => 'Edit',      'width'=>'5%'     ),   //edit
    array('data' => 'Order',     'width'=>'5%',    ),
    array('data' => 'Talk Code', 'width'=>'10%'    ),
    array('data' => 'Type',      'width'=>'7%'     ),
    array('data' => 'Talk Name'                    ),
    array('data' => 'Description'                  ),
  );
  //Query the database
  $result = db_select('emmaus_team_talks', 't')
    ->fields('t')
    ->extend('TableSort')         //Extend the query to include table sorting
    ->orderBy('talk_order')
    ->execute();
  
  //Loop through the result and create an array of rows
  $rows = array();
  foreach ($result as $row) {   
    //create links
    $edit_link = '<a href="/admin/config/emmaus/team/talks/' . $row->talk_id . '/edit"><img border="0" alt="Edit" title="Edit this talk" src="/'. drupal_get_path('module','emmaus_community') . '/images/edit.png"></a>';
    //$delete_link = '<a href="/admin/config/emmaus/team/talks/' . $row->talk_id . '/delete"><img border="0" alt="Edit" title="Delete this talk" src="/'. drupal_get_path('module','emmaus_community') . '/images/drop.png"></a>';

    //output the table rows
    $rows[] = array($edit_link, $row->talk_order,  $row->talk_code, $row->type, $row->talk_name, $row->description );
  }
  //output the table
  $tbl_vars = array(  'header'=>$header,  'rows'=>$rows,  'attributes'=>array(),  'caption' => $result->rowCount() . ' row(s) returned.',   'colgroups'=>array(),   'sticky'=>TRUE,   'empty'=>'No rows returned from query'  );
  $output .= theme_table($tbl_vars);
  //output a link to add a new assignment
  //$output .= '<a href="/admin/emmaus/team/settings/talks/add"><img src="/'. drupal_get_path('module','emmaus_team') . '/images/add.png">Add a new talk</a>';
  //Return the output
  return $output;
}



/**********************************************************************************************************/
/*** TALKS: Form builder function(s) to manage talks ******************************************************/
/**********************************************************************************************************/
function emmaus_team_talks_form($form, &$form_state, $params=NULL) {
  //Check to see if we should display the confirmation form after the delete button has been pressed
  if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Delete') {
    // Define form elements again, prior to final submission
    $form['talk_id']   = array( '#type' => 'hidden', '#default_value' => $form_state['values']['talk_id']   );
    $form['talk_name'] = array( '#type' => 'hidden', '#default_value' => $form_state['values']['talk_name'] );
    //Display the confirmation form
    return confirm_form( 
      $form, 
      t('Are you sure that you want to delete the <em>@name</em> talk?', array( '@name'=>$form_state['values']['talk_name'] )  ), 
      'admin/config/emmaus/team/talks/' . $form_state['values']['talk_id'] . '/edit', 
      'This action can not be undone.  Please proceed with caution.', 
      'Delete', 
      'Don\'t Delete'
    );  
  } // end of delete confirmation form
  
  
  //Set up the form based on wether or we are adding or editing 
  switch ($params) {
    case NULL:  //submitting a new 
      drupal_set_title(t('Add New Team Talk'));
      //set default values for the form elements
      $default_values = array ( 'talk_id'=>NULL, 'talk_name'=>NULL, 'talk_code'=>NULL, 'description'=>NULL, 'type'=>NULL, 'talk_order'=>NULL );    
      break;
    case is_numeric($params): //editing an existing application
      drupal_set_title( t('Add Existing Team Talk') );
      //Query the database for the record to edit
      $default_values = db_select( 'emmaus_team_talks', 't' )
        ->fields('t')
        ->condition( 't.talk_id', $params )
        ->execute()
        ->fetchAssoc();
      //add a delete button to the bottom of the form
      $form['delete'] = array(  '#type'=>'submit', '#value'=>'Delete',  '#weight'=>90  );
      break;
   }  
  
  //Define an array of talk types
  $types = array( 'C'=>'Clergy', 'L'=>'Lay'  );
  // Define all the form elements
  $form['talk_id'] = array( 
    '#type'=>'hidden',     
    '#default_value' => $default_values['talk_id'],         
    );
  $form['talk_name'] = array( 
    '#type'=>'textfield',  
    '#title'=>t('Talk Name'),         
    '#required' => TRUE,  
    '#size' => 50,  
    '#maxlength' => 50,   
    '#default_value' => $default_values['talk_name'],     
    '#description' => t('Enter a name of this talk.')       
    );
  $form['talk_code'] = array( 
    '#type'=>'textfield',  
    '#title'=>t('Talk Code'),         
    '#required' => TRUE,  
    '#size' => 4,  
    '#maxlength' => 5,   
    '#default_value' => $default_values['talk_code'],     
    '#description' => t('Enter a short abbreviation or code to represent this talk in history reports.')       
    );
  $form['description'] = array( 
    '#type'=>'textarea',   
    '#title'=>t('Description'),           
    '#default_value'=>$default_values['description'], 
    '#description'=>t('Optional. Describe the content of this talk.')          
    );     
  $form['type'] = array( 
    '#type'=>'select',     
    '#title'=>t('Talk Type'),
    '#required'=>TRUE,        
    '#default_value'=>$default_values['type'],   
    '#options'=>$types,        
    '#description'=>t('Select the talk type')  
    );    
  $form['talk_order'] = array( 
    '#type'=>'weight',     
    '#title'=>t('Talk Order'),                
    '#default_value'=>$default_values['talk_order'],  
    '#delta'=>15,             
    '#description' => t('Optional. A numeric value to sort the talks.')  
    );  
  //Define the Submit buttons
  $form['submit'] = array( 
    '#type' => 'submit', 
    '#value' => t('Submit'), 
    );
  //$form['cancel'] = array( '#type' => 'submit', '#value' => t('Cancel'), );
  //Return the form
  return $form;
}
/****************************/
/* TALKS: Validate handler  */
/****************************/
function emmaus_team_talks_form_validate($form, &$form_state) {
}
/*************************/
/* TALKS: Submit handler */
/*************************/
function emmaus_team_talks_form_submit($form, &$form_state) {
  //Delete button has been clicked initially.  Set a flag and rebuild the form.
  if (!isset($form_state['storage']['confirm']) && $form_state['values']['op']=='Delete' ) {
    $form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form      
    $form_state['rebuild'] = TRUE; // along with this    
  }
  //Deletion has been confirmed.  Process the queries.
  elseif (isset($form_state['storage']['confirm']) ) {    
    //Delete the history row
    $num_deleted = db_delete('emmaus_team_talks')
      ->condition('talk_id', $form_state['values']['talk_id'])
      ->execute();
    drupal_set_message( t('<em>@name</em> talk was deleted.', array('@name'=>$form_state['values']['talk_name'] ) ), 'status'  );
  }
  //Submit button was pressed.  Write the record.
  elseif ($form_state['values']['op']=='Submit') {
  
    if ( $form_state['values']['talk_id'] == '' ) {
      drupal_write_record('emmaus_team_talks', $form_state['values'] );
      drupal_set_message(t('Talk <em>@name</em> has been added.', array('@name'=>$form_state['values']['talk_name'] ) ) );
    } else {
      drupal_write_record('emmaus_team_talks', $form_state['values'], 'talk_id');
      drupal_set_message(t('Talk <em>@name</em> has been modified.', array('@name'=>$form_state['values']['talk_name'] ) ) );
    }
  }
  //Redirect the form for all functions, including the cancel
  $form_state['redirect'] = 'admin/config/emmaus/team/talks';
}






