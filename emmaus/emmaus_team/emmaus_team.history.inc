<?php

/*************************************************************************************************************/
/*** EDIT: Form builder - function(s) to edit an existing board member entry *********************************/
/*************************************************************************************************************/
function emmaus_team_user_history_form($form, &$form_state, $profile, $history=NULL) {
  //Check to see if we should display the confirmation form for the delete operation
  if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Delete') {
    // Define form elements again, prior to final submission
    $form['history_id']   = array( '#type' => 'hidden', '#default_value' => $form_state['values']['history_id']    );
    $form['profile_id']   = array( '#type' => 'hidden', '#default_value' => $form_state['values']['profile_id']    );
    //$form['full_name']    = array( '#type' => 'hidden', '#default_value' => $form_state['values']['full_name']   );
    $date = format_date( $form_state['values']['weekend_date'] , 'custom', 'M, Y'); 
    $form['date']         = array( '#type' => 'item',  '#title' => 'Date: ' . $date   );
    $form['weekend']      = array( '#type' => 'item',  '#title' => 'Weekend: ' . $form_state['values']['weekend_name'],   );
    //Display the confirmation form
    return confirm_form($form, t('Are you sure that you want to delete this history entry for @name?', array('@name'=>$profile->emmaus_profile['full_name'])), 'user/' .$form_state['values']['profile_id'] . '/edit/team-history', 'This action can not be undone.  Please proceed with caution.', 'Remove', 'Don\'t Remove');   
  } //confirmation form  
  
  //Set up the form based on wether or we are adding or editing 
  if (empty($history)) {  //We were passed an empty history so set the form up as an 'add'
    drupal_set_title( t('Add "Out of Community" Team History for @name', array('@name'=>$profile->emmaus['profile']['full_name']) ) );
    //set default values for the form elements
    $history = array ( 'history_id'=>NULL, 'profile_id'=>$profile->uid, 'weekend_date'=>NULL, 'community_name'=>NULL, 'weekend_name'=>NULL, 'position_code'=>NULL, 'talk_code'=>NULL );    
  } else {  //otherwise, we mus thave been passed a valid history so let's set up the form as an 'edit'
    drupal_set_title( t('Edit Out-of-Community Team Experience History for @name', array('@name'=>$profile->emmaus['profile']['full_name']) ) );
    //add a delete button to the bottom of the form
    $form['delete'] = array(  '#type'=>'submit', '#value'=>'Delete',  '#weight'=>90  );
  }  
  
  //retrieve the list of talks and positions to populate the select boxes
  if ( $profile->emmaus['profile']['is_clergy'] == 1 ) {
    $positions = array_merge(  _emmaus_team_positions('C') );  
    $talks = array_merge( array('NONE'=>'- None -') ,  _emmaus_team_talks('C')   )  ;
  } else {
    $positions = array_merge( _emmaus_team_positions('L'), _emmaus_team_positions('I'), _emmaus_team_positions('O')  );
    $talks = array_merge( array('NONE'=>'- None -') ,  _emmaus_team_talks('L')    )  ;
  }

  //convert the date back to an array
  if ( !empty($history['weekend_date']) ) {
    $date = $history['weekend_date'];
    $history['weekend_date'] = array( 'month'=>date('m', $date), 'day'=>date('d', $date), 'year'=>date('Y', $date),    );
    //drupal_set_message('<pre>' . print_r($history['weekend_date'], TRUE) . '</pre>' );  
  }

  // Define all the form elements
  $form['history_id']    = array( '#type'=>'hidden',  '#default_value'=> $history['history_id'], );
  $form['profile_id']    = array( '#type'=>'hidden',  '#default_value'=> $profile->uid, );

  $form['weekend_date']   = array( '#type'=>'date',        '#title'=>'Weekend Date',   '#required'=>TRUE, '#default_value'=>$history['weekend_date'], '#description'=>'Please enter the month and day of the weekend that you worked.'  );
  $form['community_name'] = array( '#type' => 'textfield', '#title'=>'Community Name', '#default_value'=>$history['community_name'], '#description'=>'Enter the name of the community in which you worked (i.e. - South Bend Emmaus)'     );
  $form['weekend_name']   = array( '#type' => 'textfield', '#title'=>'Weekend Name/Number', '#default_value'=>$history['weekend_name'],  '#description'=>'Enter the name and number of the weekend.  (i.e. - Men\'s Walk to Emmaus #12)'     );
  //$form['weekend_number']= array( '#type' => 'select',     '#title'=>'Select Weekend',       '#required'=>TRUE,    '#options'=>$weekends,  '#default_value'=>$history['weekend_number']  );
  //position
  $form['position_code'] = array( '#type' => 'select',     '#title' => t('Position Held'),   '#required' => TRUE,  '#options'=>$positions, '#default_value'=>$history['position_code'],   '#description' => t('The name of the talk that your delivered.')       ); 
  $form['position_name'] = array ('#type' => 'hidden');
  //talk
  $form['talk_code']   = array( '#type' => 'select',     '#title' => t('Primary Talk'),    '#required' => FALSE, '#options'=>$talks,     '#default_value'=>$history['talk_code'],     '#description' => t('The name of the talk that your delivered.')       ); 
  $form['talk_name']   = array ('#type' => 'hidden');
  //Define the Submit buttons
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit'), );
  //Return the form
  return $form;
}

/***************************/
/* EDIT: Validate handler  */
/***************************/
function emmaus_team_user_history_form_validate($form, &$form_state) {
  //Convert the date
  if ( !empty($form_state['values']['weekend_date'] ) ) {
    //drupal_set_message('<pre>' . print_r($form_state['values']['weekend_date'], TRUE) . '</pre>' );  
    $form_state['values']['weekend_date'] = mktime(0, 0, 0, $form_state['values']['weekend_date']['month'], $form_state['values']['weekend_date']['day'], $form_state['values']['weekend_date']['year']);  
  }
  
  //retrieve the name of the position by referring the values in the select box form elements
  if ( !empty($form_state['values']['position_code']) ) {
    $code = $form_state['values']['position_code'];
    $form_state['values']['position_name'] = $form['position_code']['#options'][$code];
  }
  //retrieve the name of the talk by referring the values in the select box form elements  
  if ( !empty($form_state['values']['talk_code']) ) {
    $code = $form_state['values']['talk_code'];
    $form_state['values']['talk_name'] = $form['talk_code']['#options'][$code];
  }
}

/************************/
/* EDIT: Submit handler */
/************************/
function emmaus_team_user_history_form_submit($form, &$form_state) {
  //loop through all form elemts and set their values to the literal NULL if they are empty strings
  foreach ($form_state['values'] as &$value) {  //don't forget to put the ampersand in front of the loop variable to specify a reference to the original
    if (empty($value) ) {
      $value = NULL;
    }    
  }
  //drupal_set_message('$form_state[\'values\'] => <pre>' . print_r($form_state['values'], TRUE) . '</pre>', 'warning'   );

  //Delete button has been clicked initially.  Set a flag and rebuild the form.
  if (!isset($form_state['storage']['confirm']) && $form_state['values']['op']=='Delete' ) {
    $form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form      
    $form_state['rebuild'] = TRUE; // along with this    
  }
  //Deletion has been confirmed.  Process the queries.
  elseif (isset($form_state['storage']['confirm']) ) {    
    //Delete the history row
    $num_deleted = db_delete('emmaus_team_history')
      ->condition('history_id', $form_state['values']['history_id'])
      ->execute();
    drupal_set_message( t('History entry was deleted.') );
  }
  //Submit button was pressed.  Write the record.
  elseif ($form_state['values']['op']=='Submit') {
    //convert date field to timestamp before saving record
    //$form_state['values']['date'] = strtotime( $form_state['values']['date']['month'] . '/' . $form_state['values']['date']['day'] . '/' . $form_state['values']['date']['year']      );
    //drupal_set_message('$form_state[\'values\'] => <pre>' . print_r($form_state['values'], TRUE) . '</pre>'   );
    //Write the record
    if ( empty($form_state['values']['history_id']) )  { //we're inserting a new record
      drupal_write_record('emmaus_team_history', $form_state['values'] );
      drupal_set_message(t('History entry was successfully added'));
    } else {    
      drupal_write_record('emmaus_team_history', $form_state['values'], 'history_id');
      drupal_set_message(t('History entry was successfully updated'));
    }
  }
  //Redirect the form for all functions, including the cancel
  $form_state['redirect'] = 'user/' .$form_state['values']['profile_id'] . '/edit/team'  ;
}

