<?php


/*************************************************************************************************************/
/*** Table/List builder function(s) to display team assignment for a given weekend ***************************/
/*************************************************************************************************************/
function emmaus_team_weekend_view( $weekend_number ) { 
  //Initialize the output variable
  $output = ''; 
  //Get the weekend name 
  $weekend_name = db_select('emmaus_weekend_weekends','w')
    ->condition('weekend_number',$weekend_number)
    ->fields('w', array('weekend_name'))
    ->execute()
    ->fetchField();
  //Set the page title
  //drupal_set_title( $weekend_name . ' Team' );

  //Define a header for all tables
  $header = array(
    array( 'data' => 'Name',      'width'=>'30%',   ),
    array( 'data' => 'Position',  'width'=>'30%',   ),
    array( 'data' => 'Talk'   ),
  );    

  //** Clergy table
  //Query the database
  $select = db_select('emmaus_team_roster', 'r');
  $select ->leftJoin('emmaus_community_profile', 'p', 'r.profile_id = p.profile_id');
  $select ->leftJoin('emmaus_team_positions', 'pos', 'r.position_code = pos.position_code');
  $select ->leftJoin('emmaus_team_talks', 'tlk', 'r.p_talk_code = tlk.talk_code');
  $select ->condition('r.weekend_number' , $weekend_number);    //define a conditional query
  $select ->condition('r.type' , 'C');    //define a conditional query
  $select ->fields( 'p' , array('full_name') );
  $select ->fields( 'pos' , array('position_name') );
  $select ->fields( 'tlk' , array('talk_name') );
  $select ->orderBy('pos.weight');
  $clergy = $select->execute()->fetchAllAssoc('full_name', PDO::FETCH_ASSOC);
  //create the empty table string @author DA104940
  $empty = 'No clergy has been assigned to this weekend.</a>'; 
  //output the table
  $tbl_vars = array(  'header'=>$header,  'rows'=>$clergy,  'attributes'=>array(),  'caption' => '<h4>Clergy</h4>',   'colgroups'=>array(),   'sticky'=>TRUE,   'empty'=>$empty   );
  $output .= theme_table($tbl_vars);

  //** Lay Leadership table
  //Query the database
  $select = db_select('emmaus_team_roster', 'r');
  $select ->leftJoin('emmaus_community_profile', 'p', 'r.profile_id = p.profile_id');
  $select ->leftJoin('emmaus_team_positions', 'pos', 'r.position_code = pos.position_code');
  $select ->leftJoin('emmaus_team_talks', 'tlk', 'r.p_talk_code = tlk.talk_code');
  $select ->condition('r.weekend_number' , $weekend_number);    //define a conditional query
  $select ->condition('r.type' , 'L');    //define a conditional query
  $select ->fields( 'p' , array('full_name') );
  $select ->fields( 'pos' , array('position_name') );
  $select ->fields( 'tlk' , array('talk_name') );
  $select ->orderBy('pos.weight');
  $leadership = $select->execute()->fetchAllAssoc('full_name', PDO::FETCH_ASSOC);
  //create the empty table string @author DA104940
  $empty = 'No lay leadership has been assigned to this weekend.'; 
  //output the table
  $tbl_vars = array(  'header'=>$header,  'rows'=>$leadership,  'attributes'=>array(),  'caption' => '<h4>Lay Leadership</h4>',   'colgroups'=>array(),   'sticky'=>TRUE,   'empty'=>$empty   );
  $output .= theme_table($tbl_vars);

  /*
  //** Team table
  //Query the database
  $select = db_select('emmaus_team_applications', 'a');
  $select ->leftJoin('emmaus_community_profile', 'p', 'a.profile_id = p.profile_id');
  $select ->leftJoin('emmaus_team_positions', 'pos', 'a.assigned_position = pos.position_code');
  $select ->leftJoin('emmaus_team_talks', 'tlk', 'a.primary_talk = tlk.talk_code');
  $select ->condition('a.assigned_weekend' , $weekend_number);    //define a conditional query
  //$select ->condition('p.is_clergy' , 0);   //define a conditional query
  $select ->fields( 'p' , array('first_name', 'last_name') );
  $select ->fields( 'a' , array('application_id', 'assigned_position', 'primary_talk', 'backup_talk') );
  $select ->fields( 'pos', array('position_name') );
  $select ->fields( 'tlk', array('talk_name') );
  $result = $select->execute();
  //Loop through the result and create an array of rows
  $rows = array();
  foreach ($result as $row) {   
    //output the table rows
    $rows[] = array( $row->first_name.' '.$row->last_name, $row->position_name, $row->talk_name  );
  }
  //output the table
  $tbl_vars = array(  'header'=>$header,  'rows'=>$rows,  'attributes'=>array(),  'caption' => '<h4>Team Members</h4>',   'colgroups'=>array(),   'sticky'=>TRUE,   'empty'=>'No team members have been assigned to this weekend'   );
  $output .= theme_table($tbl_vars);
  */
 
  //Return the output
  return $output;
}



/*************************************************************************************************************/
/*** Table/List builder function(s) to display team assignment for a given weekend ***************************/
/*************************************************************************************************************/
function emmaus_team_weekend_edit_form( $form, $form_state, $params ) { 
  //Get the weekend name 
  $weekend_name = db_select('emmaus_weekend_weekends','w')
      ->condition('weekend_number',$params)
      ->fields('w', array('weekend_name'))
      ->execute()
      ->fetchField();
  //Set the page title
  drupal_set_title( $weekend_name . ' - Edit Team' );

  //Build some initial, hidden form fields for use by the submit function
  $form['weekend_number'] = array( '#type'=>'hidden', '#default_value'=>$params   ); 
  $form['weekend_name'] = array( '#type'=>'hidden', '#default_value'=>$weekend_name   ); 


  /*** Clergy Members ***********************/
  //query the database for assigned clergy members
  $select = db_select('emmaus_team_roster', 'r');
  $select ->leftJoin('emmaus_community_profile', 'p', 'r.profile_id = p.profile_id');
  $select ->condition('r.assigned_weekend' , $params);    //define a conditional query
  $select ->condition('r.type' , 'C');    //define a conditional query
  $select ->fields( 'p' , array('full_name') );
  $select ->fields( 'r' , array('assignment_id', 'assigned_position', 'primary_talk') );
  $clergy_list = $select->execute();

  //retrieve the list of talks and positions for the associated form elements
  $clergy_positions = array_merge( array(NULL=>'- None -') , _emmaus_team_positions('C') );
  $clergy_talks = array_merge( array(NULL=>'- None -') , _emmaus_team_talks('C') );
  
  //build the form for clergy.
  $form['clergy']             = array( '#type'=>'fieldset', '#tree'=>TRUE,   '#title'=>'Clergy for ' . $weekend_name  );
  $form['clergy']['open_table'] = array(   '#markup' => '<table><thead><tr><th width="5%">Assigned</th><th width="20%">Name</th><th width="25%">Position</th><th>Talk</th></tr></thead>',   '#weight' => 0,   );
  //Loop through the assigned clergy and build a table row with select fields for each one
  foreach ($clergy_list as $clergy) {
    //$this_id = $clergy->assignment_id;  
    $form['clergy'][$clergy->assignment_id]['assignment_id'] = array('#type'=>'hidden', '#default_value'=>$clergy->assignment_id );
    $form['clergy'][$clergy->assignment_id]['assigned']  = array( '#type'=>'checkbox', '#default_value'=>1,     '#prefix'=>'<tr><td align="center">',  '#suffix'=>'</td>');
    $form['clergy'][$clergy->assignment_id]['name']      = array( '#markup'=>$clergy->full_name,     '#prefix'=>'<td>',  '#suffix'=>'</td>'  );
    $form['clergy'][$clergy->assignment_id]['position']  = array( '#type'=>'select',  '#default_value'=>$clergy->assigned_position, '#options'=>$clergy_positions, '#prefix'=>'<td>',   '#suffix'=>'</td>'        );
    $form['clergy'][$clergy->assignment_id]['talk']      = array( '#type'=>'select',  '#default_value'=>$clergy->primary_talk,     '#options'=>$clergy_talks,     '#prefix'=>'<td>',   '#suffix'=>'</td></tr>'   );
  }
  //close the table
  $form['clergy']['close_table']        = array( '#markup'=>'</tr></table>'  );
  //$form['clergy']['add_link'] = array( '#markup'=>'<a href="/emmaus/weekend/' . $params . '/edit/team/add-clergy?destination=emmaus/weekend/'.$params.'/edit/team"><img src="/'. drupal_get_path('module','emmaus_community') . '/images/add.png">Add a clergy member to this weekend</a>');
  
  
  /*** Lay Leadership ***********************/
  //query the database for assigned lay leadership
  $select = db_select('emmaus_team_roster', 'r');
  $select ->leftJoin('emmaus_community_profile', 'p', 'r.profile_id = p.profile_id');
  $select ->condition('r.assigned_weekend' , $params);    //define a conditional query
  $select ->condition('r.type' , 'L');    //define a conditional query
  $select ->fields( 'p' , array('full_name') );
  $select ->fields( 'r' , array('assignment_id', 'assigned_position', 'primary_talk', 'backup_talk'  ) );
  $layleaders = $select->execute();

  //retrieve the list of talks and positions for the associated form elements
  $lay_leader_positions = array_merge( array(NULL=>'- None -') , _emmaus_team_positions('L') );
  $lay_talks = array_merge( array(NULL=>'- None -') , _emmaus_team_talks('L') );

  //build the form for lay leadership.
  $form['lay']             = array( '#type'=>'fieldset', '#tree'=>TRUE,   '#title'=>'Lay Leadership for ' . $weekend_name  );
  $form['lay']['open_table'] = array(   '#markup' => '<table><thead><tr><th width="5%">Assigned</th><th width="20%">Name</th><th width="25%">Position</th><th>Primary Talk</th><th>Backup Talk</th></tr></thead>',   '#weight' => 0,   );
  //Loop through the assigned clergy and build a table row with select fields for each one
  foreach ($layleaders as $row) {
    //$this_id = $clergy->assignment_id;  
    $form['lay'][$row->assignment_id]['assignment_id'] = array('#type'=>'hidden', '#default_value'=>$row->assignment_id );
    $form['lay'][$row->assignment_id]['assigned']  = array( '#type'=>'checkbox', '#default_value'=>1,     '#prefix'=>'<tr><td align="center">',  '#suffix'=>'</td>');
    $form['lay'][$row->assignment_id]['name']      = array( '#markup'=>$row->full_name,     '#prefix'=>'<td>',  '#suffix'=>'</td>'  );
    $form['lay'][$row->assignment_id]['position']  = array( '#type'=>'select',  '#default_value'=>$row->assigned_position, '#options'=>$lay_leader_positions, '#prefix'=>'<td>',   '#suffix'=>'</td>'        );
    $form['lay'][$row->assignment_id]['primary_talk'] = array( '#type'=>'select',  '#default_value'=>$row->primary_talk,     '#options'=>$lay_talks,     '#prefix'=>'<td>',   '#suffix'=>'</td>'   );
    $form['lay'][$row->assignment_id]['backup_talk']  = array( '#type'=>'select',  '#default_value'=>$row->backup_talk,     '#options'=>$lay_talks,     '#prefix'=>'<td>',   '#suffix'=>'</td></tr>'   );
  }
  //close the table
  $form['lay']['close_table']        = array( '#markup'=>'</tr></table>'  );
  //$form['layleader']['add_link'] = array( '#markup'=>'<a href="/emmaus/weekend/' . $params . '/edit/team/add-clergy?destination=emmaus/weekend/'.$params.'/edit/team"><img src="/'. drupal_get_path('module','emmaus_community') . '/images/add.png">Add a clergy member to this weekend</a>');
  
  
  
  
  
  
  /*
  // ** Team Members ***********************
  //query the database for assigned team applications
  $select = db_select('emmaus_team_applications', 'a');
  $select ->leftJoin('emmaus_community_profile', 'p', 'a.profile_id = p.profile_id');
  $select ->condition('a.assigned_weekend' , $params);    //define a conditional query
  $select ->fields( 'p' , array('full_name') );
  $select ->fields( 'a' , array('application_id', 'assigned_position', 'primary_talk', 'backup_talk') );
  $team_list = $select->execute();
  
  //retrieve the list of talks and positions for the associated form elements
  $team_positions = array_merge( array(NULL=>'- None -') , _emmaus_team_positions('I'), _emmaus_team_positions('O') );
  $team_talks = array_merge( array(NULL=>'- None -') , _emmaus_team_talks('L') );
  
  //build the form for team
  $form['team']             = array( '#type'=>'fieldset',  '#tree'=>TRUE,  '#title'=>'Team Members for ' . $weekend_name  );
  $form['team']['open_table'] = array(   '#markup' => '<table><thead><tr><th width="5%">Assigned</th><th width="20%">Name</th><th width="25%">Position</th><th width="25%">Primary Talk</th><th>Backup Talk</th></tr></thead>',   '#weight' => 0,   );
  //Loop through the assigned clergy and build a table row with select fields for each one
  foreach ($team_list as $team) {
    $form['team'][$team->application_id]['application_id'] = array('#type'=>'hidden', '#default_value'=>$team->application_id );
    $form['team'][$team->application_id]['assigned']  = array( '#type'=>'checkbox', '#default_value'=>1,     '#prefix'=>'<tr><td align="center">',  '#suffix'=>'</td>');
    $form['team'][$team->application_id]['name']      = array( '#markup'=>$team->full_name,     '#prefix'=>'<td>',  '#suffix'=>'</td>'  );
    $form['team'][$team->application_id]['assigned_position']  = array( '#type'=>'select',   '#default_value'=>$team->assigned_position, '#options'=>$team_positions, '#prefix'=>'<td>',   '#suffix'=>'</td>'           );
    $form['team'][$team->application_id]['primary_talk']      = array( '#type'=>'select',   '#default_value'=>$team->primary_talk,     '#options'=>$team_talks,     '#prefix'=>'<td>',   '#suffix'=>'</td>'           );
    $form['team'][$team->application_id]['backup_talk']      = array( '#type'=>'select',   '#default_value'=>$team->backup_talk,     '#options'=>$team_talks,     '#prefix'=>'<td>',   '#suffix'=>'</td></tr>'           );
    //$form['team'][$team->application_id]['talk_is_primary']    = array( '#type'=>'checkbox', '#default_value'=>$team->talk_is_primary,                                '#prefix'=>'<td>',   '#suffix'=>'</td></tr>'      );
  }
  //close the table
  $form['team']['close_table']        = array( '#markup'=>'</tr></table>'  );
  //$form['team']['add_link'] = array( '#markup'=>'<a href="/admin/emmaus/team/applications"><img src="/'. drupal_get_path('module','emmaus_community') . '/images/add.png">View avaiable team applications</a>');
  */
 
  //define the submit button
  $form['submit'] = array(   '#type' => 'submit',   '#value' => t('Submit'),  );
  //return the form
  return $form;      
  }
/****************************/
/*** Validate handler *******/
/****************************/
function emmaus_team_weekend_edit_form_validate( $form, $form_state ) { 
}
/****************************/
/*** Submit handler *******/
/****************************/
function emmaus_team_weekend_edit_form_submit( $form, $form_state ) {
  
  //Loop through the returned clergy assignments and update the matching rows in the database using the form values
  if (isset($form_state['values']['clergy'])==TRUE ) {
    foreach ( $form_state['values']['clergy'] as $clergy_assignment) {
      //validate field values
      if ( $clergy_assignment['position'] == '' ) { $clergy_assignment['position'] = NULL; }
      if ( $clergy_assignment['talk'] == '' ) { $clergy_assignment['talk'] = NULL; }
      //update the database
      if ( $clergy_assignment['assigned'] == 0  ) {
        db_delete('emmaus_team_leadership')
          ->condition('assignment_id', $clergy_assignment['assignment_id'] )
          ->execute();
      } else {
        db_update('emmaus_team_leadership')
          ->condition('assignment_id', $clergy_assignment['assignment_id'] )
          ->fields( array(
            'assigned_position'  => $clergy_assignment['position'],
            'primary_talk'  => $clergy_assignment['talk'],
            ))
          ->execute();
      } // if / else
    } //foreach
  } //if
    
  //Loop through the returned clergy assignments and update the matching rows in the database using the form values
  if (isset($form_state['values']['lay'])==TRUE ) {
    foreach ( $form_state['values']['lay'] as $lay_assignment) {
      //validate field values
      if ( $lay_assignment['position'] == '' ) { $lay_assignment['position'] = NULL; }
      if ( $lay_assignment['primary_talk'] == '' ) { $lay_assignment['primary_talk'] = NULL; }
      if ( $lay_assignment['backup_talk'] == '' ) { $lay_assignment['backup_talk'] = NULL; }
      //update the database
      if ( $lay_assignment['assigned'] == 0  ) {
        db_delete('emmaus_team_leadership')
          ->condition('assignment_id', $lay_assignment['assignment_id'] )
          ->execute();
      } else {
        db_update('emmaus_team_leadership')
          ->condition('assignment_id', $lay_assignment['assignment_id'] )
          ->fields( array(
            'assigned_position'  => $lay_assignment['position'],
            'primary_talk'  => $lay_assignment['primary_talk'],
            'backup_talk'  => $lay_assignment['backup_talk'],
            ))
          ->execute();
      } // if / else
    } //foreach
  } //if
    
    
    
    
      //Loop through the returned team assignments and update the matching rows in the database using the form values
  if (isset($form_state['values']['team'])==TRUE ) {
    foreach ($form_state['values']['team'] as $team_assignment) {
      //validate field values
      if ( $team_assignment['assigned_position'] == '' ) { $team_assignment['assigned_position'] = NULL; }
      if ( $team_assignment['primary_talk'] == '' )  {  $team_assignment['primary_talk'] = NULL; }
      if ( $team_assignment['backup_talk'] == '' )  {  $team_assignment['backup_talk'] = NULL; }
      //update the database
      if ( $team_assignment['assigned'] == 0 )  {
        db_update('emmaus_team_applications')
          ->condition('application_id', $team_assignment['application_id'])
          ->fields( array(
            'assigned_weekend'  => NULL,
            'assigned_position' => NULL,
            'primary_talk'      => NULL,
            'backup_talk'       => NULL,
            ))
          ->execute();
      } else {
        db_update('emmaus_team_applications')
          ->condition('application_id', $team_assignment['application_id'])
          ->fields( array(
            'assigned_position' => $team_assignment['assigned_position'],
            'primary_talk'      => $team_assignment['primary_talk'],
            'backup_talk'       => $team_assignment['backup_talk'],
            ))
          ->execute();
      } //if
    } //foreach
    } //if
  //Set a confirmation message
  drupal_set_message(t('Team assignments for @weekend have been updated.', array('@weekend'=>$form_state['values']['weekend_name']  )   )   );
   
}
