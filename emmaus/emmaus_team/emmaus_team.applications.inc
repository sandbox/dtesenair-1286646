<?php
//$Id$ 




/*************************************************************************************************************/
/*** Table/List builder function(s) to display weekend summary report ****************************************/
/*************************************************************************************************************/
function emmaus_team_applications_summary() {
	$output = ''; //'<p>This is where a table will show all of the available and assigned applications.</p>';
	//$output .= '<p>TODO:  Change these tag lines to pull from a variable.  Maybe?</p>';
	
	//Define the table header
	$header = array( 'Number', 'Name', 'Date', '1st Choice', '2nd Choice', '3rd Choice', 'Assigned');

	//Get the weekends
	$weekends = _emmaus_weekend_weekends(NULL, 1);

	$rows = array();
	foreach ( $weekends as $key=>$value) {		
		//load the weekend into an array
		$weekend = _emmaus_weekend_load($key);
		//Get the number of applications for each choice and assignment
		$first_choice 	= db_query('SELECT COUNT(*) FROM {emmaus_team_applications} a WHERE a.assigned_weekend IS NULL AND a.weekend_pref1 = :weekend_num', array(':weekend_num' => $weekend['weekend_number']))->fetchField();
		$second_choice 	= db_query('SELECT COUNT(*) FROM {emmaus_team_applications} a WHERE a.assigned_weekend IS NULL AND a.weekend_pref2 = :weekend_num', array(':weekend_num' => $weekend['weekend_number'])) ->fetchField();
		$third_choice 	= db_query('SELECT COUNT(*) FROM {emmaus_team_applications} a WHERE a.assigned_weekend IS NULL AND a.weekend_pref3 = :weekend_num', array(':weekend_num' => $weekend['weekend_number'])) ->fetchField();
		$assigned 			= db_query('SELECT COUNT(*) FROM {emmaus_team_applications} a WHERE a.assigned_weekend = :weekend_num', array(':weekend_num' => $weekend['weekend_number'])) ->fetchField();
		//output the table rows
		$rows[] = array( $weekend['weekend_number'], $weekend['weekend_name'], format_date( $weekend['start_date'], 'custom', 'N j, Y' ), $first_choice[0], $second_choice[0], $third_choice[0], $assigned[0] );
	}
	//output the table
	$tbl_vars = array(  'header'=>$header,	'rows'=>$rows,	'attributes'=>array(),	'caption' => '<h3>Application Summary Report</h3>',		'colgroups'=>array(),		'sticky'=>TRUE, 	'empty'=>'No weekends scheduled' 	);
	$output .= theme_table($tbl_vars);

	return $output;
}


/*************************************************************************************************************/
/*** Table/List builder function(s) to display unassigned applications ***************************************/
/*************************************************************************************************************/
function emmaus_team_applications_available() {
	//$output = '';
	$output = '<p>The following are applications that have been submitted by a community member but have yet to be assigned to a team:</p>';
	$gender_arr = array('M','F');
	foreach ($gender_arr as $gender) {
		//Define the table header
		$header = array(
			array('data' => '', 'width'=>'30'	 ),		//edit
			array('data' => '', 'width'=>'30'	 ),		//delete
			array('data' => 'Name',					'field'=>'full_name', 				'sort'=>'asc'		),
			//array('data' => 'Date',					'field'=>'application_date'										),
			array('data' => '1st Choice',		'field'=>'weekend_pref1'											),
			array('data' => '2nd Choice',		'field'=>'weekend_pref2'											),
			array('data' => '3rd Choice',		'field'=>'weekend_pref3'											),
			array('data' => 'Teams'																											),
			array('data' => 'Talks'																											),
			array('data' => 'Last Walk'																										),		
		);	
		//Set the table caption
		if ($gender == 'M') { $caption = 'Applications for Men\'s Weekends'; }
		if ($gender == 'F') { $caption = 'Applications for Women\'s Weekends'; }
	
		//Query the database for applications
		$select = db_select('emmaus_team_applications', 'a'  );
		$select ->leftJoin( 'emmaus_community_profile', 'p', 'a.profile_id = p.profile_id');
		$select ->condition('a.assigned_weekend' , NULL )		//define a conditional query
			->condition('p.gender' , $gender )		//define a conditional query
			->fields('a')
			->fields('p', array('full_name'));
		$result = $select->execute();	
	
		//Loop through the result and create an array of rows
		$rows = array();
		foreach ($result as $row) {		
      //for each application, query the database for the person's history
      $history_result = db_select('emmaus_team_history', 'h'  )
        ->condition('h.profile_id' , $row->profile_id )   //define a conditional query
        ->fields('h')
        ->orderBy('date')
        ->execute(); 
      $num_teams = 0;
      $num_talks = 0;
      $last_weekend = '';
      //loop through each history entry and build the team & talk counts and determine the last weekend worked
      foreach($history_result as $history ) {
        $num_teams++;
        if ( !$history->p_talk_code==NULL ) {$num_talks++; } 
        if ( !empty($history->b_talk_code) ) {$num_talks++; } 
        $last_weekend = $history->weekend_number;  //since the query sorts by date, the last one will be set to this variable and then output in the row below
      }

			//create links
			$assign_link = '<a href="/admin/emmaus/team/applications/' . $row->application_id . '/assign"><img border="0" alt="Edit" title="Edit/Assign this application" src="/'. drupal_get_path('module','emmaus_community') . '/images/assign.gif"></a>';
			$delete_link = '<a href="/emmaus/team/applications/' . $row->application_id . '/delete"><img border="0" alt="Del" title="Delete this application" src="/'. drupal_get_path('module','emmaus_community') . '/images/drop.png"></a>';
 	    $profile_link = '<a href="/user/' . $row->profile_id . '">' . $row->full_name . '</a>';
 	
			//build the view link
			if ( empty($row->weekend_pref1)==FALSE  ) {  $weekend_pref1 = '<a href="/emmaus/weekend/' . $row->weekend_pref1 . '/view">' . $row->weekend_pref1 . '</a>';  } else { $weekend_pref1=''; }
			if ( empty($row->weekend_pref2)==FALSE  ) {  $weekend_pref2 = '<a href="/emmaus/weekend/' . $row->weekend_pref2 . '/view">' . $row->weekend_pref2 . '</a>';  } else { $weekend_pref2=''; }
			if ( empty($row->weekend_pref3)==FALSE  ) {  $weekend_pref3 = '<a href="/emmaus/weekend/' . $row->weekend_pref3 . '/view">' . $row->weekend_pref3 . '</a>';  } else { $weekend_pref3=''; }
		
			//output the table rows
			$rows[] = array( $assign_link, $delete_link, $profile_link, /*format_date($row->application_date, 'emmaus'),*/ $weekend_pref1, $weekend_pref2, $weekend_pref3, $num_teams, $num_talks, $last_weekend );
		}
		//output the table
		$tbl_vars = array(  'header'=>$header,	'rows'=>$rows,	'attributes'=>array(),	'caption' => '<h3>'.$caption.'</h3>',		'colgroups'=>array(),		'sticky'=>TRUE, 	'empty'=>'No available applications' 	);
		$output .= theme_table($tbl_vars);
	}
	//return the themed table
	return $output;
}



/*************************************************************************************************************/
/*** Form builder function(s) to assign an application to a team *********************************************/
/*************************************************************************************************************/
function emmaus_team_application_assign_form($form, &$form_state, $application) {
  //drupal_set_message('<pre>' . print_r($application, TRUE) . '</pre>'   );
    
  //Now that we have the profile_id from the application, load the whole user profile
  $profile = user_load($application['profile_id']);
  
	//set the page title
	drupal_set_title( t('Assign application for @name', array('@name'=>$profile->emmaus_profile['full_name'] ) ) );

  module_load_include('inc', 'emmaus_team', 'emmaus_team.history');
  //Return a 'drupal_render'ed output
  $team_markup['fieldset'] = array( '#type'=>'fieldset', '#title'=>'Team History',  '#collapsible'=>TRUE);
  $team_markup['fieldset']['markup'] = array( '#markup'=>emmaus_team_user_history_view($profile->uid) );
  $history_markup1 = '<div style="clear: both;">' . drupal_render($team_markup) . '</div>';

  //create arrays for weekends, talks & positions for the form elements
  $weekends = _emmaus_weekend_weekends( $profile->emmaus_profile['gender'], 1 );
  $talks = array_merge( array(NULL=>'- None -') , _emmaus_team_talks('L') );
  $positions = array_merge( array(NULL=>'- None -') , _emmaus_team_positions('I'), _emmaus_team_positions('O') );
  
	//build the form
	$form['app']						= array( '#type'=>'fieldset',	'#title'=>'Application Details'	);
  $form['app']['picture'] = array(
    '#markup' => theme('user_picture', array('account'=>$profile)),
    '#weight' => -10,
  );
  $form['app']['application_date']  = array( '#type'=>'markup', '#markup'=>'<p><b>Application Date:</b> ' . format_date($application['application_date'],'custom', 'M j,Y') . '</p>'  );
  /*
  $form['app']['special_needs']  = array( 
    '#markup'=>'<p><b>Dietary Needs:</b> ' . $application['dietary_needs'] . '<br/>' .  
               '<b>Physical Needs:</b> ' . $application['physical_needs'] . '<br/>' .  
               '<b>Medical Needs:</b> ' . $application['medication_needs'] . '</p>'  
    );
  */
  $form['app']['special_needs']  = array( 
    '#markup'=>'<p><b>Special Needs:</b> ' . $profile->emmaus_profile['special_needs'] . '</p>'  
    );
  $form['app']['reason']  = array( 
    '#markup'=>'<p><b>Reason:</b> ' . $application['reason'] . '</p>'  
    );
  $form['app']['notes']  = array( 
    '#markup'=>'<p><b>Notes:</b> ' . $application['notes'] . '</p>'  
    );

	$form['app']['weekend_pref1']	= array( '#type'=>'markup',	'#markup'=>'<b>Weekend Preference #1:</b> <a href="/emmaus/weekend/' . $application['weekend_pref1'] . '/team">'. $application['weekend_pref1'] . '</a><br/>'	);
	$form['app']['weekend_pref2']	= array( '#type'=>'markup',	'#markup'=>'<b>Weekend Preference #2:</b> <a href="/emmaus/weekend/' . $application['weekend_pref2'] . '/team">'. $application['weekend_pref2'] . '</a><br/>'	);
	$form['app']['weekend_pref3']	= array( '#type'=>'markup',	'#markup'=>'<b>Weekend Preference #3:</b> <a href="/emmaus/weekend/' . $application['weekend_pref3'] . '/team">'. $application['weekend_pref3'] . '</a><br/>'	);
	$form['app']['position_pref']	= array( '#type'=>'markup',	'#markup'=>'<p><b>Position Preference:</b> '. $application['position_pref'] . '</p>'	);

	$form['team_history'] = array( '#type'=>'markup',	'#markup'=>$history_markup1	);

  $form['assign']             = array( '#type'=>'fieldset', '#title'=>'Weekend Assignment'  );
	$form['assign']['open_table']					= array( '#markup'=>'<table><tr>'  );
  $form['assign']['application_id']     = array( '#type'=>'hidden',   '#title'=>'Applicaiton ID',     '#default_value'=>$application['application_id'],                            '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );
  $form['assign']['assigned_weekend']   = array( '#type'=>'select',   '#title'=>'Assign to Weekend',  '#default_value'=>$application['assigned_weekend'],  '#options'=>$weekends,  '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );
  $form['assign']['assigned_position']	= array( '#type'=>'select',	  '#title'=>'Assign Position',		'#default_value'=>$application['assigned_position'], '#options'=>$positions, '#prefix'=>'<td valign="top">',	'#suffix'=>'</td>'	);
  $form['assign']['primary_talk']       = array( '#type'=>'select',   '#title'=>'Primary Talk',       '#default_value'=>$application['primary_talk'],      '#options'=>$talks,     '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );
  $form['assign']['backup_talk']        = array( '#type'=>'select',   '#title'=>'Backup Talk',        '#default_value'=>$application['backup_talk'],       '#options'=>$talks,     '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );
	$form['assign']['close_table']				= array( '#markup'=>'</tr></table>'  );
	//Submit button
  $form['assign']['submit'] = array( '#type' => 'submit', '#value' => 'Submit', );
	//Return the form
  return $form;

}
/****************************/
/*** Validate handler *******/
/****************************/
function emmaus_team_application_assign_form_validate($form, &$form_state) {
  if ($form_state['values']['assigned_weekend'] == '' ) {
    $form_state['values']['assigned_weekend'] == NULL;
    $form_state['values']['assigned_position'] == NULL;
    $form_state['values']['assigned_talk'] == NULL;
    $form_state['values']['talk_is_primary'] == NULL;
  }
}
/**************************/
/*** Submit handler *******/
/**************************/
function emmaus_team_application_assign_form_submit($form, &$form_state) {
  //check to see if assigned_weekend is NULL.  if so, set other assignment fields to NULL, as well.
  if ($form_state['values']['assigned_weekend'] == '' )  {
    $form_state['values']['assigned_weekend'] = NULL;
    $form_state['values']['assigned_position'] = NULL;
    $form_state['values']['assigned_talk'] = NULL;
    $form_state['values']['talk_is_primary'] = 0;
  }
  if ($form_state['values']['assigned_position'] == '' )  { $form_state['values']['assigned_position'] = NULL; }
  if ($form_state['values']['assigned_talk'] == '' )  { $form_state['values']['assigned_talk'] = NULL; }
  if ($form_state['values']['assigned_position'] == '' )  { $form_state['values']['assigned_position'] = NULL; }
  
  //Write the record
  drupal_write_record('emmaus_team_applications', $form_state['values'], 'application_id');
  
  //Set a confirmation message
  drupal_set_message(t('Application has been assigned to @weekend', array('@weekend'=>$form_state['values']['assigned_weekend']  )   )   );
  //Redirect the form
  //$form_state['redirect'] = 'emmaus/team/applications/available'  ;
}
























/*************************************************************************************************************/
/*** Form builder function(s) to apply for a team ************************************************************/
/*************************************************************************************************************/
function emmaus_team_application_form($form, &$form_state, $application=NULL) {
  //Attempt to get current user id and retrieve their gender
  global $user;

  //Check to see if we should display the confirmation form after the delete button has been pressed
  if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Delete') {
    //Load the profile of the user
    $account = user_load( $form_state['values']['profile_id'] );    
    // Define form elements again, prior to final submission
    $form['application_id'] = array( '#type' => 'hidden', '#default_value' => $form_state['values']['application_id']  );
    $form['profile_id']     = array( '#type' => 'hidden', '#default_value' => $form_state['values']['profile_id']      );
    //Display the confirmation form
    return confirm_form( 
      $form, 
      t('Are you sure that you want to delete application @number for @name?', array( '@name'=>$account->emmaus_profile['full_name'], '@number'=>$form_state['values']['application_id'] )  ), 
      'user/' .$form_state['values']['profile_id'] . '/edit/team', 
      'This action can not be undone.  Please proceed with caution.', 
      'Remove', 
      'Don\'t Remove'
    );  
  } // end of delete confirmation form


  //Set up the form based on wether or we are submitting a new application (self or admin) or editing an existing one
  switch ($application) {
    case NULL:  //submitting a new application for self
      drupal_set_title(t('Apply to Work an Emmaus Weekend'));
      //make sure the user has a valid profile.  If so, set the gender variable
      $profile = user_load($user->uid);
      if ( empty($profile->emmaus_profile) ) {
        drupal_set_message( t('Your community profile could not be located.  Therefore, you can not apply to work a team at this time.  If you feel that you have received this message in error, please contact the site administrator for further assistance.'), 'warning');
        return;
      } 
      //add a form element with the action
      $form['action'] = array( '#type'=>'hidden', '#default_value'=>'new');
      //Query the database to make sure we aren't exceeding the max number of applications
      $existing_apps = db_select( 'emmaus_team_applications', 'a' )
        ->fields('a')
        ->condition( 'a.profile_id', $profile->emmaus_profile['profile_id'] )
        ->execute()
        ->rowCount();
      if ( variable_get('emmaus_team_max_applications')!=0 && $existing_apps >= variable_get('emmaus_team_max_applications')  ) {
        drupal_set_message( t('You have submitted the maimum number of team application permitted by your community.  Therefore, you can not apply to work a team at this time.  If you feel that you have received this message in error, please contact the site administrator for further assistance.'), 'warning');
        return '';         
      }      
      //set default values for the form elements
      $default_values = array ( 'application_id'=>'', 'application_date'=>time(), 'profile_id'=>'', 'full_name'=>'', 'position_pref'=>'', 'dietary_needs'=>'', 'physical_needs'=>'', 'medication_needs'=>'', 'reason'=>'', 'notes'=>'', 'weekend_pref1'=>'',  'weekend_pref2'=>'', 'weekend_pref3'=>'' );    
      $default_values['profile_id'] = $profile->emmaus_profile['profile_id'];
      break;
    case 'admin':   //submitting a new application for others
      //drupal_set_title(t(''));
      $form['caption'] = array( '#markup'=>'<h3>Submit an application on behalf of a community member.</h3>');
      $form['action'] = array( '#type'=>'hidden', '#default_value'=>'admin');
      //drupal_set_message('The $param variable is ' . $params , 'error');
      $default_values = array ( 'application_id'=>'', 'application_date'=>time(), 'profile_id'=>'', 'full_name'=>'', 'position_pref'=>'', 'dietary_needs'=>'', 'physical_needs'=>'', 'medication_needs'=>'', 'reason'=>'', 'notes'=>'', 'weekend_pref1'=>'',  'weekend_pref2'=>'', 'weekend_pref3'=>'' );    
      break;
    case is_array($application): //editing an existing application
      drupal_set_title( t('Edit Team Application') );
      //Load the user's profile
      $profile = user_load($application['profile_id']);
      //Query the database for the application to edit
      $default_values =$application;
      //add a form element with the action
      $form['action'] = array( '#type'=>'hidden', '#default_value'=>'edit');
      //add a delete button to the bottom of the form
      $form['delete'] = array(  
        '#type'=>'submit', 
        '#value'=>'Delete', 
        '#weight'=>90,
        //disable the button if the application has already been assigned
        '#disabled' => !empty($default_values['assigned_weekend'])  
        );
      break;
    default:
      drupal_set_message('Could not determine the appropriate action to perform.  Exiting team application.', 'error');
      return;
      break;
  }
  
  //Build an array of profiles to select from
  //TODO: Change this to use a helper function in the community module
  $result = db_select( 'emmaus_community_profile', 'p' )
    ->fields('p', array('profile_id', 'first_name', 'last_name', 'city' ) )
    ->orderBy('last_name', 'ASC')
    ->orderBy('first_name', 'ASC')
    ->execute();
  $profiles = array(NULL=>'- Select -');
  $num_profiles = $result->rowCount();
  //Make sure a profile was returned from the query
  if ( $result->rowCount() < 1) {
    drupal_set_message( t('There are no available community user profiles to apply for.  If you feel that you have received this message in error, please contact the site administrator for further assistance.'), 'warning');
    return $form;
  }
  foreach ($result as $row) {
    $profiles[$row->profile_id] =  $row->first_name .' '. $row->last_name . ' (' . $row->city . ')'  ;
  }

  //Build an array of available weekends to select from
  //TODO: Change this to use a helper function in the weekend module
  $result = db_select( 'emmaus_weekend_weekends', 'w' )
    ->fields('w', array('weekend_number', 'weekend_name'))
    ->condition( 'w.start_date', time(), '>' );
    //add an additional condition if not in 'admin' mode
    if ( $application != 'admin') {$result->condition( 'w.gender', $profile->emmaus_profile['gender'] );  }
    $result = $result->execute();
  //Make sure a weekend was returned
  $num_weekends = $result->rowCount();
  if ( $num_weekends < 1) {
    drupal_set_message( t('We apologize but there are no open/active weekends available to apply for.  If you feel that you have received this message in error, please contact the site administrator for further assistance.'), 'warning');
    //return;
  }
  //Loop through the returned weekends and create an options array
  $weekends = array(NULL=>'- Select -');
  foreach ($result as $row) {
    $weekends[$row->weekend_number] = t($row->weekend_name);
  }

  //Build an array of position preferences to pass as the #options value  
  $positions = array(
    'Any'=>'Any', 
    'Inside'=>'Inside (Conference Room, Music, etc.)', 
    'Outside'=>'Outside (Kitchen, Agape, Chapel, etc.)'
    );
    
  // Define the form elements
  $form['application_id'] = array( 
    '#type'=>'hidden',     
    '#default_value'=>$default_values['application_id']       
    );
  $form['application_date'] = array( 
    '#type'=>'hidden',     
    '#default_value'=>$default_values['application_date']       
    );
  $form['submitted_by'] = array( 
    '#type'=>'hidden', 
    '#default_value'=>$user->name,
    );
  $form['profile_id'] = array( 
    '#type'=>'select',  
    '#title'=>t('Applicant Name'), 
    '#options'=>$profiles, 
    '#default_value'=>$default_values['profile_id'],
    '#disabled' => !($application == 'admin' ),      //enable the select box if this is an 'admin' submission
    );
  $form['position_pref'] = array( 
    '#type'=>'radios',     
    '#title'=>t('Position Preference'),   
    '#options'=>$positions,  
    '#required'=>TRUE,  
    '#default_value'=>$default_values['position_pref']   
    );
  $form['note']  = array( 
    '#type'=>'fieldset',  
    '#title'=>t('Notes'),   
    );
  $form['note']['reason'] = array( 
    '#type'=>'textarea',   
    '#title'=>t('Briefly state why you are applying to work a team'),
    '#default_value'=>$default_values['reason'], 
    '#rows'=>2, 
    '#resizable'=>FALSE,    
    );
  $form['note']['notes'] = array( 
    '#type'=>'textarea',   
    '#title'=>t('Please provide any additional information pertinent to this application'), 
    '#default_value'=>$default_values['notes'], 
    '#rows'=>2, 
    '#resizable'=>FALSE,    
    );
  //Weekend selection
  $form['weekends']  = array( 
    '#type'=>'fieldset',  
    '#title'=>t('Weekend Selection'),   
    );
  if ( $num_weekends >= 1 ) {
    $form['weekends']['weekend_pref1']  = array( 
      '#type'=>'select',     
      '#title'=>t('Weekend First Choice'),  
      '#default_value'=>$default_values['weekend_pref1'], 
      '#options'=>$weekends,
      //disable the weekend selection box if the application has already been assigned
      //'#disabled' => !empty($default_values['assigned_weekend'])  
      );
  }
  if ( $num_weekends >= 2 ) {
    $form['weekends']['weekend_pref2'] = array( 
      '#type'=>'select',     
      '#title'=>t('Weekend Second Choice'), 
      '#default_value'=>$default_values['weekend_pref2'], 
      '#options'=>$weekends,  
      //disable the weekend selection box if the application has already been assigned
      //'#disabled' => !empty($default_values['assigned_weekend'])  
      );
  }
  if ( $num_weekends >= 3 ) {
    $form['weekends']['weekend_pref3'] = array( 
      '#type'=>'select',     
      '#title'=>t('Weekend Third Choice'),  
      '#default_value'=>$default_values['weekend_pref3'], 
      '#options'=>$weekends,  
      //disable the weekend selection box if the application has already been assigned
      //'#disabled' => !empty($default_values['assigned_weekend'])  
      );
  }
  //Submit button
  $form['submit'] = array( 
    '#type' => 'submit', 
    '#value' => 'Submit', 
    );
  //Return the form
  return $form;
}
/****************************/
/*** ADD Validate handler ***/
/****************************/
function emmaus_team_application_form_validate($form, &$form_state) {
  //Skip validation if we are confirming deletion
  if (isset($form_state['storage']['confirm']) ) {    return;    }
  
  //Otherwise, check weekend_pref1 and set error if not selected
  if ($form_state['values']['weekend_pref1'] == NULL) {
    form_set_error('weekend_pref1', 'You must select at least one weekend to apply to.');
  }
  //check the weekend prefernces and make sure values and set to null if empty
  if ( empty($form_state['values']['weekend_pref2'])  ) {
    $form_state['values']['weekend_pref2'] = NULL;
  }
  if ( empty($form_state['values']['weekend_pref3'])  ) {
    $form_state['values']['weekend_pref3'] = NULL;
  }
  //set the special needs fields to NULL if the special_check checkbox was 0
  /*
  if ( $form_state['values']['special_check']==0 ) {
    $form_state['values']['dietary_needs'] = NULL;
    $form_state['values']['physical_needs'] = NULL;
    $form_state['values']['medication_needs'] = NULL;
  } 
  */
}
/**************************/
/*** Submit handler *******/
/**************************/
function emmaus_team_application_form_submit($form, &$form_state) {
  global $user;
  $account = user_load($user->uid);

  //Delete button has been clicked initially.  Set a flag and rebuild the form.
  if (!isset($form_state['storage']['confirm']) && $form_state['values']['op']=='Delete' ) {
    $form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form      
    $form_state['rebuild'] = TRUE; // along with this    
  }
  //Deletion has been confirmed.  Process the queries.
  elseif (isset($form_state['storage']['confirm']) ) {    
    //Delete the history row
    $num_deleted = db_delete('emmaus_team_applications')
      ->condition('application_id', $form_state['values']['application_id'])
      ->execute();
    drupal_set_message( t('Application was deleted.'), 'status'  );
  }

  //Submit button was pressed.  Write the record.
  elseif ($form_state['values']['op']=='Submit') {
    //do some last minute validation of fields that are foreign keys
    if ( empty( $form_state['values']['weekend_pref2'] ) )  { $form_state['values']['weekend_pref2'] = NULL; }
    if ( empty( $form_state['values']['weekend_pref3'] ) )  { $form_state['values']['weekend_pref3'] = NULL; }
  
    if ( $form_state['values']['application_id'] == '' ) {
      //update any remaining fields
      drupal_write_record('emmaus_team_applications', $form_state['values'] );
      drupal_set_message(t('Thank you for applying to work a team on an upcoming weekend in the @community_name. An email address will be sent as confirmation of this choice.', array('@community_name'=>variable_get('emmaus_community_name') ) ) );
    } else {
      drupal_write_record('emmaus_team_applications', $form_state['values'], 'application_id');
      drupal_set_message( t('Application was updated.' ) );    
    }
  
    //drupal_set_message( '$form_state[\'values\']<pre>' . print_r($form_state['values'], TRUE) . '</pre>'   );   
  
    //Send an email
    module_load_include('inc', 'emmaus_team', 'emmaus_team.notify');
    $message = _emmaus_team_email_new_application($form_state['values']);    
    //drupal_mail($module, $key, $to, $language, $params = array(), $from = NULL, $send = TRUE)
    $mail_result = drupal_mail('emmaus_team', 'new_app', $message['recipients'] , language_default(), $message  );
    //drupal_set_message('Mail Result: ' . print_r($mail_result, TRUE) );
  }
  //Redirect the form 
  $form_state['redirect'] = 'user/' .$form_state['values']['profile_id'] .'/edit/team' ;
}





