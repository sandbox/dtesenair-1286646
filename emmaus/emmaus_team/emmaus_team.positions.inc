<?php 
// $Id:   
   
  

/*************************************************************************************************************/
/*** Table/List builder function(s) to display existing positions in admin section ***************************/
/*************************************************************************************************************/
function emmaus_team_positions_admin() { 
	//Set the tag line
	$output = '<h3><p>Current positions defined for the ' . variable_get('emmaus_community_name') . '.</p></h3>';
  $output .= '<p>From this page, you can add, delete, or modify a weekend position definition.</p>';
	//Define the table header
	$header = array(
		array('data' => '', 'width'=>'40'	 ),		//edit
		array('data' => '', 'width'=>'40'	 ),		//contact
    array('data' => 'Order', 'width'=>'10'     ),
    array('data' => 'Position Name'    ),
    array('data' => 'Description'      ),
    array('data' => 'Type',            'align'=>'center', 'width'=>'40'     ),
    array('data' => 'Team Selection',  'align'=>'center', 'width'=>'40'     ),
	);
	//Query the database
  $result = db_select('emmaus_team_positions', 't')
		->fields('t')
  	->extend('TableSort')					//Extend the query to include table sorting
		->orderBy('weight')
		->execute();
	//Loop through the result and create an array of rows
	$rows = array();
	foreach ($result as $row) {		
		//create links
		$edit_link = '<a href="/admin/config/emmaus/team/positions/' . $row->position_id . '/edit"><img border="0" alt="Edit" title="Edit this position" src="/'. drupal_get_path('module','emmaus_community') . '/images/edit.png"></a>';
		$delete_link = '<a href="/admin/config/emmaus/team/positions/' . $row->position_id . '/delete"><img border="0" alt="Edit" title="Delete this position" src="/'. drupal_get_path('module','emmaus_community') . '/images/drop.png"></a>';
    //format the leadership column
    $grant_rights = array('data'=>'','align'=>'center');
    if ( $row->grant_rights == 1 ) {
      $grant_rights = array( 'data'=>'<img border="0" alt="Clergy" src="/'. drupal_get_path('module','emmaus_community') . '/images/check.png">', 'align'=>'center' );
    }
    
		//output the table rows
		$rows[] = array($edit_link, $delete_link, $row->weight, $row->position_name, $row->description, $row->type, $grant_rights );
	}
	//output the table
	$tbl_vars = array(  'header'=>$header,	'rows'=>$rows,	'attributes'=>array(),	'caption' => $result->rowCount() . ' row(s) returned.',		'colgroups'=>array(),		'sticky'=>TRUE, 	'empty'=>'No rows returned from query' 	);
	$output .= theme_table($tbl_vars);
	//output a link to add a new assignment
	//$output .= '<a href="/admin/emmaus/team/settings/positions/add"><img src="/'. drupal_get_path('module','emmaus_team') . '/images/add.png">Add a new position</a>';
	//Return the output
	return $output;
}





/*************************************************************************************************************/
/*** Form builder function(s) to add a newpositions **********************************************************/
/*************************************************************************************************************/
function emmaus_team_positions_add() {
	// Define the form elements
  $form['position_name'] 	= array( '#type'=>'textfield',	'#title'=>t('Position Name'),		    '#required'=>TRUE,  '#size' => 50,  '#maxlength' => 50, 	'#description' => t('Enter a name of this position.')   		                           );
  $form['description']    = array( '#type'=>'textarea',   '#title'=>t('Description'),         '#required'=>FALSE,                                       '#description'=>t('Optional.  Describe the primary duties of this position.')          );     
  $form['is_leadership']  = array( '#type'=>'checkbox',   '#title'=>t('Leadership position?'), '#description'=>t('NOTE: Leadership positions grant additional rights to team selection for an assigned weekend.')  );    
  $form['weight']         = array( '#type' => 'weight',     '#title' => t('Weight'),       '#default_value'=>0,  '#delta'=>10,    '#description' => t('Optional. A numeric value to sort the positions.')  );  

	//Submit button
  $form['submit'] = array( '#type' => 'submit', '#value' => 'Submit', );
	//Return the form
  return $form;
}
/****************************/
/*** ADD Validate handler ***/
/****************************/
function emmaus_team_positions_add_validate($form, &$form_state) {
}
/**************************/
/*** ADD Submit handler ***/
/**************************/
function emmaus_team_positions_add_submit($form, &$form_state) {
	//Generate the insert query
  db_insert('emmaus_team_positions')
      ->fields(array(
						'position_name'		=> $form_state['values']['position_name'],
            'description'     =>  $form_state['values']['description'],
            'is_leadership'   =>  $form_state['values']['is_leadership'],
            'weight'          =>  $form_state['values']['weight']
					)
				)
      ->execute();
	//Set a confirmation message
  drupal_set_message(t('Position <em>@name</em> has been added.', array('@name' => $form_state['values']['position_name'])));
	//Redirect the form
	$form_state['redirect'] = 'admin/config/emmaus/team/positions';
}




/*************************************************************************************************************/
/*** EDIT: Form builder - function(s) to edit an existing board member entry *********************************/
/*************************************************************************************************************/
function emmaus_team_positions_edit($form, &$form_state, $params) {
	//var_dump($params);	
	//Query the database for the record to edit
  $result = db_select( 'emmaus_team_positions', 'p' )
  	->fields('p')
		->condition( 'p.position_id', $params )
		->execute()
		->fetchAssoc();
		var_dump($result);
	//Set a prettier page title		
	drupal_set_title( t('Edit @name Position', array( '@name'=>$result['position_name']) ) );
  //Define an array of position types
  $types = array( 'C'=>'Clergy', 'I'=>'Inside Team', 'O'=>'Outside Team'   );
	// Define all the form elements
  $form['position_id']		=	array( '#type'=>'hidden', 	 	'#title'=>t('ID'), 			              '#default_value' => $result['position_id'], 				);
  $form['position_name']	= array( '#type'=>'textfield',	'#title'=>t('Position Name'),	        '#required' => TRUE,  '#size' => 50,  '#maxlength' => 50, 	'#default_value' => $result['position_name'],			'#description' => t('Enter a name of this position.')   		);
  $form['description']    = array( '#type'=>'textarea',   '#title'=>t('Description'),           '#required'=>FALSE,   '#default_value'=>$result['description'], '#description'=>t('Optional.  Describe the primary duties of this position.')          );     
  $form['type']           = array( '#type'=>'select',     '#title'=>t('Position Type?'),        '#default_value'=>$result['type'],   '#options'=>$types,        '#description'=>t('Select the position type')  );    
  $form['grant_rights']   = array( '#type'=>'checkbox',   '#title'=>t('Grant Team Selection?'), '#default_value'=>$result['grant_rights'],                      '#description'=>t('Grant additional rights to team selection for an assigned weekend?')  );    
  $form['weight']         = array( '#type'=>'weight',     '#title'=>t('Weight'),                '#default_value'=>$result['weight'],  '#delta'=>20,             '#description' => t('Optional. A numeric value to sort the positions.')  );  
	//Define the Submit buttons
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit'), );
  $form['cancel'] = array( '#type' => 'submit', '#value' => t('Cancel'), );
	//Return the form
 	return $form;
}
/***************************/
/* EDIT: Validate handler  */
/***************************/
function emmaus_team_positions_edit_validate($form, &$form_state) {
}
/************************/
/* EDIT: Submit handler */
/************************/
function emmaus_team_positions_edit_submit($form, &$form_state) {
	//Submit button was pressed.  Write the record.
	if ($form_state['values']['op']=='Submit') {
		drupal_write_record('emmaus_team_positions', $form_state['values'], 'position_id');
		drupal_set_message(t('@name position has been updated', array('@name' => $form_state['values']['position_name'])));
	}
	//Redirect the form for all functions, including the cancel
	$form_state['redirect'] = 'admin/config/emmaus/team/positions';
}








/*********************************/
/*** DELETE: Confirmation Form ***/
/*********************************/
function emmaus_team_positions_delete($form, &$form_state, $params) {
	var_dump($params);
  // Always provide entity id in the same form key as in the entity edit form.
  $form['position_id'] 		= array( '#type' => 'hidden',			'#default_value'=> $params	);
	//Return the confirmation form
  return confirm_form($form,
    t('Are you sure you want to delete the %name position?', array('%name' => emmaus_team_positions_name($params) ) ),
    'admin/emmaus/teams/positions',
    t('This action cannot be undone.  Any team history that has held this position will be nullified.' ),
    t('Delete'),
    t('Cancel')
  );
}
/*********************************/
/*** DELETE: Submit Handler ******/
/*********************************/
function emmaus_team_positions_delete_submit($form, &$form_state) {
		var_dump($form_state);
  if ($form_state['values']['confirm']) {
		//get name before deleting
		$position_name = emmaus_team_positions_name($form_state['values']['position_id']);
		$num_deleted = db_delete('emmaus_team_positions')  
				->condition('position_id', $form_state['values']['position_id'])  
				->execute();
		
    drupal_set_message(t('"@name" position has been deleted.', array('@name' => $position_name) ) );
  }

  $form_state['redirect'] = 'admin/emmaus/team/settings/positions';
}








 
 
 
 




/*********************************************************************************************/
/** BEGIN CUSTOM FUNCTIONS *******************************************************************/
/*********************************************************************************************/
/**
 * Get church name for title callback
 */
function emmaus_team_positions_name($position_id) {
	//Query the database
  $result = db_select('emmaus_team_positions','t')
    ->fields('t', array('position_name')) 
		->condition('position_id' , $position_id)			//define a conditional query
		->range(0,1)	 												//determine the range of records... range(offset, number of records)
    ->execute()					//execute the query
		->fetchField();				//retrieve the first column
	//Return the output
	var_dump($result);
	return $result;
}

