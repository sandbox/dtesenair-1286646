<?php


/*************************************************************************************************************/
/*** Table/List builder function(s) to display team assignment for a given weekend ***************************/
/*************************************************************************************************************/
function emmaus_team_view( $weekend_number ) { 
  //Initialize the output variable
  $output = ''; 
  //Get the weekend name 
  $weekend_name = db_select('emmaus_weekend_weekends','w')
    ->condition('weekend_number',$weekend_number)
    ->fields('w', array('weekend_name'))
    ->execute()
    ->fetchField();
  //Set the page title
  //drupal_set_title( $weekend_name . ' Team' );

  //Define a header for all tables
  $header = array(
    array( 'data' => 'Name',      'width'=>'30%',   ),
    array( 'data' => 'Position',  'width'=>'30%',   ),
    array( 'data' => 'Talk'   ),
  );    

  //** Clergy table
  //Query the database
  $select = db_select('emmaus_team_roster', 'r');
  $select ->leftJoin('emmaus_community_profile', 'p', 'r.profile_id = p.profile_id');
  $select ->leftJoin('emmaus_team_positions', 'pos', 'r.position_code = pos.position_code');
  $select ->leftJoin('emmaus_team_talks', 'tlk', 'r.p_talk_code = tlk.talk_code');
  $select ->condition('r.weekend_number' , $weekend_number);    //define a conditional query
  $select ->condition('r.type' , 'C');    //define a conditional query
  $select ->fields( 'p' , array('full_name') );
  $select ->fields( 'pos' , array('position_name') );
  $select ->fields( 'tlk' , array('talk_name') );
  $select ->orderBy('pos.weight');
  $clergy = $select->execute()->fetchAllAssoc('full_name', PDO::FETCH_ASSOC);
  //create the empty table string @author DA104940
  $empty = 'No clergy has been assigned to this weekend.</a>'; 
  //output the table
  $tbl_vars = array(  'header'=>$header,  'rows'=>$clergy,  'attributes'=>array(),  'caption' => '<h4>Clergy</h4>',   'colgroups'=>array(),   'sticky'=>TRUE,   'empty'=>$empty   );
  $output .= theme_table($tbl_vars);

  //** Lay Leadership table
  //Query the database
  $select = db_select('emmaus_team_roster', 'r');
  $select ->leftJoin('emmaus_community_profile', 'p', 'r.profile_id = p.profile_id');
  $select ->leftJoin('emmaus_team_positions', 'pos', 'r.position_code = pos.position_code');
  $select ->leftJoin('emmaus_team_talks', 'tlk', 'r.p_talk_code = tlk.talk_code');
  $select ->condition('r.weekend_number' , $weekend_number);    //define a conditional query
  $select ->condition('r.type' , 'L');    //define a conditional query
  $select ->fields( 'p' , array('full_name') );
  $select ->fields( 'pos' , array('position_name') );
  $select ->fields( 'tlk' , array('talk_name') );
  $select ->orderBy('pos.weight');
  $leadership = $select->execute()->fetchAllAssoc('full_name', PDO::FETCH_ASSOC);
  //create the empty table string @author DA104940
  $empty = 'No lay leadership has been assigned to this weekend.'; 
  //output the table
  $tbl_vars = array(  'header'=>$header,  'rows'=>$leadership,  'attributes'=>array(),  'caption' => '<h4>Lay Leadership</h4>',   'colgroups'=>array(),   'sticky'=>TRUE,   'empty'=>$empty   );
  $output .= theme_table($tbl_vars);

  /*
  //** Team table
  //Query the database
  $select = db_select('emmaus_team_applications', 'a');
  $select ->leftJoin('emmaus_community_profile', 'p', 'a.profile_id = p.profile_id');
  $select ->leftJoin('emmaus_team_positions', 'pos', 'a.assigned_position = pos.position_code');
  $select ->leftJoin('emmaus_team_talks', 'tlk', 'a.primary_talk = tlk.talk_code');
  $select ->condition('a.assigned_weekend' , $weekend_number);    //define a conditional query
  //$select ->condition('p.is_clergy' , 0);   //define a conditional query
  $select ->fields( 'p' , array('first_name', 'last_name') );
  $select ->fields( 'a' , array('application_id', 'assigned_position', 'primary_talk', 'backup_talk') );
  $select ->fields( 'pos', array('position_name') );
  $select ->fields( 'tlk', array('talk_name') );
  $result = $select->execute();
  //Loop through the result and create an array of rows
  $rows = array();
  foreach ($result as $row) {   
    //output the table rows
    $rows[] = array( $row->first_name.' '.$row->last_name, $row->position_name, $row->talk_name  );
  }
  //output the table
  $tbl_vars = array(  'header'=>$header,  'rows'=>$rows,  'attributes'=>array(),  'caption' => '<h4>Team Members</h4>',   'colgroups'=>array(),   'sticky'=>TRUE,   'empty'=>'No team members have been assigned to this weekend'   );
  $output .= theme_table($tbl_vars);
  */
 
  //Return the output
  return $output;
}



/*************************************************************************************************************/
/*** Table/List builder function(s) to display team assignment for a given weekend ***************************/
/*************************************************************************************************************/
function emmaus_team_edit_form( $form, $form_state, $params ) { 
  //drupal_set_message('emmaus_team_edit_form - no val');

  //Get the weekend name 
  $weekend_name = db_select('emmaus_weekend_weekends','w')
      ->condition('weekend_number',$params)
      ->fields('w', array('weekend_name'))
      ->execute()
      ->fetchField();
  //Set the page title
  drupal_set_title( $weekend_name . ' - Team Selection' );

  //Build some initial, hidden form fields for use by the submit function
  $form['weekend_number'] = array( '#type'=>'hidden', '#default_value'=>$params   ); 
  $form['weekend_name'] = array( '#type'=>'hidden', '#default_value'=>$weekend_name   ); 


  /*** Clergy Members ***********************/
  //query the database for assigned clergy members
  $select = db_select('emmaus_team_roster', 'r');
  $select ->leftJoin('emmaus_community_profile', 'p', 'r.profile_id = p.profile_id');
  $select ->leftJoin('emmaus_team_positions', 'pos', 'r.position_code = pos.position_code');
  $select ->condition('r.weekend_number' , $params);    //define a conditional query
  $select ->condition('r.type' , 'C');    //define a conditional query
  $select ->fields( 'p' , array('full_name') );
  $select ->fields( 'r' );
  $select ->orderBy('pos.weight');
  $clergy_list = $select->execute();

  //retrieve the list of talks and positions for the associated form elements
  $clergy_positions = array_merge( array(NULL=>' ') , _emmaus_team_positions('C') );
  $clergy_talks = array_merge( array(NULL=>' ') , _emmaus_team_talks('C') );
  
  //build the form for clergy.
  $form['clergy']             = array( '#type'=>'fieldset', '#tree'=>TRUE,   '#title'=>'Clergy'  );
  $form['clergy']['open_table'] = array(   '#markup' => '<table><thead><tr><th width="5%">Assigned</th><th width="20%">Name</th><th width="25%">Position</th><th>Talk</th></tr></thead>',   '#weight' => 0,   );
  //Loop through the assigned clergy and build a table row with select fields for each one
  foreach ($clergy_list as $clergy) {
    //$this_id = $clergy->assignment_id;  
    $form['clergy'][$clergy->roster_id]['roster_id'] = array('#type'=>'hidden', '#default_value'=>$clergy->roster_id );
    $form['clergy'][$clergy->roster_id]['assigned']  = array( '#type'=>'checkbox', '#default_value'=>1,     '#prefix'=>'<tr><td align="center">',  '#suffix'=>'</td>');
    $form['clergy'][$clergy->roster_id]['name']      = array( '#markup'=>$clergy->full_name,     '#prefix'=>'<td>',  '#suffix'=>'</td>'  );
    //$form['clergy'][$clergy->roster_id]['position_code'] = array( '#type'=>'select',  '#default_value'=>$clergy->position_code, '#options'=>$clergy_positions, '#prefix'=>'<td>',   '#suffix'=>'</td>', '#required'=>TRUE        );
    $form['clergy'][$clergy->roster_id]['position_code'] = array( '#type'=>'select',  '#default_value'=>$clergy->position_code, '#options'=>$clergy_positions, '#prefix'=>'<td>',   '#suffix'=>'</td>' );
    $form['clergy'][$clergy->roster_id]['p_talk_code']   = array( '#type'=>'select',  '#default_value'=>$clergy->p_talk_code,     '#options'=>$clergy_talks,     '#prefix'=>'<td>',   '#suffix'=>'</td></tr>'   );
  }
  //close the table
  $form['clergy']['close_table']        = array( '#markup'=>'</tr></table>'  );
  //$form['clergy']['add_link'] = array( '#markup'=>'<a href="/emmaus/weekend/' . $params . '/edit/team/add-clergy?destination=emmaus/weekend/'.$params.'/edit/team"><img src="/'. drupal_get_path('module','emmaus_community') . '/images/add.png">Add a clergy member to this weekend</a>');
  
  
  /*** Lay Team ***********************/
  //query the database for assigned lay team members
  $select = db_select('emmaus_team_roster', 'r');
  $select ->leftJoin('emmaus_community_profile', 'p', 'r.profile_id = p.profile_id');
  $select ->leftJoin('emmaus_team_positions', 'pos', 'r.position_code = pos.position_code');
  $select ->condition('r.weekend_number' , $params);    //define a conditional query
  $select ->condition('r.type' , 'L');    //define a conditional query
  $select ->fields( 'p' , array('full_name') );
  $select ->fields( 'r' );
  $select ->orderBy('pos.weight');
  $team = $select->execute();
  //$team_arr = $team->fetchAllAssoc('roster_id', PDO::FETCH_ASSOC);
  //drupal_set_message('<pre>' . print_r($team_arr, TRUE) . '</pre>');  



  //retrieve the list of talks and positions for the associated form elements
  $lay_positions = array_merge(   array(NULL=>' '), _emmaus_team_positions('L')  );
  $lay_talks = array_merge(  array(NULL=>' '), _emmaus_team_talks('L') );

  //build the form for lay team members.
  $form['lay']             = array( '#type'=>'fieldset', '#tree'=>TRUE,   '#title'=>'Team Members'  );
  $form['lay']['open_table'] = array(   '#markup' => '<table><thead><tr><th width="5%">Assigned</th><th width="20%">Name</th><th width="25%">Position</th><th>Primary Talk</th><th>Backup Talk</th></tr></thead>',   '#weight' => 0,   );
  //Loop through the assigned clergy and build a table row with select fields for each one
  foreach ($team as $row) {
    //$this_id = $clergy->assignment_id;  
    $form['lay'][$row->roster_id]['roster_id'] = array('#type'=>'hidden', '#default_value'=>$row->roster_id );
    $form['lay'][$row->roster_id]['assigned']  = array( '#type'=>'checkbox', '#default_value'=>1,     '#prefix'=>'<tr><td align="center">',  '#suffix'=>'</td>');
    $form['lay'][$row->roster_id]['full_name'] = array( '#markup'=>$row->full_name,     '#prefix'=>'<td>',  '#suffix'=>'</td>'  );
//    $form['lay'][$row->roster_id]['position_code']  = array( '#type'=>'select',  '#default_value'=>$row->position_code, '#options'=>$lay_positions, '#prefix'=>'<td>',   '#suffix'=>'</td>', '#required'=>TRUE        );
    $form['lay'][$row->roster_id]['position_code']  = array( '#type'=>'select',  '#default_value'=>$row->position_code, '#options'=>$lay_positions, '#prefix'=>'<td>',   '#suffix'=>'</td>'       );
    $form['lay'][$row->roster_id]['p_talk_code'] = array( '#type'=>'select',  '#default_value'=>$row->p_talk_code,     '#options'=>$lay_talks,     '#prefix'=>'<td>',   '#suffix'=>'</td>'   );
    $form['lay'][$row->roster_id]['p_talk_name'] = array( '#type'=>'hidden',  '#default_value'=>$row->p_talk_name );
    $form['lay'][$row->roster_id]['b_talk_code'] = array( '#type'=>'select',  '#default_value'=>$row->b_talk_code,     '#options'=>$lay_talks,     '#prefix'=>'<td>',   '#suffix'=>'</td></tr>'   );
    $form['lay'][$row->roster_id]['b_talk_name'] = array( '#type'=>'hidden',  '#default_value'=>$row->b_talk_name );
  }
  //close the table
  $form['lay']['close_table']        = array( '#markup'=>'</tr></table>'  );
  //$form['layleader']['add_link'] = array( '#markup'=>'<a href="/emmaus/weekend/' . $params . '/edit/team/add-clergy?destination=emmaus/weekend/'.$params.'/edit/team"><img src="/'. drupal_get_path('module','emmaus_community') . '/images/add.png">Add a clergy member to this weekend</a>');
  
  
  //define the submit button
  $form['submit'] = array(   
    '#type' => 'submit',   
    '#value' => t('Submit'),
  );
  $form['export'] = array(   '#type' => 'submit',   '#value' => t('Export to CSV'),  );
  

  //return the form
  return $form;      
  }

/****************************/
/*** Validate handler *******/
/****************************/
function emmaus_team_edit_form_validate( $form, $form_state ) {

  //drupal_set_message('emmaus_team_edit_form_validate');
  //drupal_set_message('<pre>' . print_r($form, TRUE) . '</pre>' );
  //return;
  
  //retrieve the name of the position by referring the values in the select box form elements
  if ( !empty($form_state['values']['lay']['position_code']) ) {
    $code = $form_state['values']['lay']['position_code'];
    $form_state['values']['lay']['position_name'] = $$form['position_code']['#options'][$code];
  }
  //retrieve the name of the talk by referring the values in the select box form elements  
  if ( !empty($form_state['values']['lay']['p_talk_code']) ) {
    $code = $form_state['values']['p_talk_code'];
    $form_state['values']['p_talk_name'] = $form['lay'][$row->roster_id]['p_talk_code']['#options'][$code];
  }
  if ( !empty($form_state['values']['lay']['b_talk_code']) ) {
    $code = $form_state['values']['b_talk_code'];
    $form_state['values']['b_talk_name'] = $form['b_talk_code']['#options'][$code];
  }    
   
}

/****************************/
/*** Submit handler *******/
/****************************/
function emmaus_team_edit_form_submit( $form, $form_state ) {
  
  //drupal_set_message('emmaus_team_edit_form_submit');
  //drupal_set_message('<pre>' . print_r($form_state['values'], TRUE) . '</pre>' );
  //return;
  
  //retrieve the list of talks and positions so we can easily extract the names based on the form selections
  //@TODO: Replace these with some routine to extract the values from the $form elements similar to 'emmaus_team_user_history_form_validate'.  This will reduce the number of datbase quesies in this section 
  $_positions = array_merge(_emmaus_team_positions('C'), _emmaus_team_positions('L') );
  $_talks = array_merge( _emmaus_team_talks('C'),  _emmaus_team_talks('L')) ;
  
  //Loop through the returned clergy assignments and update the matching rows in the database using the form values
  if (isset($form_state['values']['clergy'])==TRUE ) {
    foreach ( $form_state['values']['clergy'] as $clergy) {
      //validate field values
      if ( $clergy['position_code'] == '' ) {
        $clergy['position_code'] = NULL; 
        $clergy['position_name'] = NULL; 
      } else {
        $clergy['position_name'] = $_positions[$clergy['position_code']];
      }
      
      if ( $clergy['p_talk_code'] == '' ) {
        $clergy['p_talk_code'] = NULL; 
        $clergy['p_talk_name'] = NULL; 
      } else {
        $clergy['p_talk_name'] = $_talks[$clergy['p_talk_code']];
      }
      //update the database
      if ( $clergy['assigned'] == 0  ) {
        db_delete('emmaus_team_roster')
          ->condition('roster_id', $clergy['roster_id'] )
          ->execute();
      } else {
        db_update('emmaus_team_roster')
          ->condition('roster_id', $clergy['roster_id'] )
          ->fields( array(
            'position_code'  => $clergy['position_code'],
            'position_name'  => $clergy['position_code'],
            'p_talk_code'  => $clergy['p_talk_code'],
            'p_talk_name'  => $clergy['p_talk_name'],
            ))
          ->execute();
      } // if / else
    } //foreach
  } //if
    
  //Loop through the returned lay assignments and update the matching rows in the database using the form values
  if (isset($form_state['values']['lay'])==TRUE ) {
    foreach ( $form_state['values']['lay'] as $lay) {
      //validate field values
      if ( $lay['position_code'] == '' ) {
         $lay['position_code'] = NULL; 
         $lay['position_name'] = NULL; 
      } else {
         $lay['position_name'] = $_positions[$lay['position_code']]; 
      }
      if ( $lay['p_talk_code'] == '' ) {
        $lay['p_talk_code'] = NULL; 
        $lay['p_talk_name'] = NULL; 
      } else {
         $lay['p_talk_name'] = $_talks[$lay['p_talk_code']]; 
      }
      if ( $lay['b_talk_code'] == '' ) {
        $lay['b_talk_code'] = NULL; 
        $lay['b_talk_name'] = NULL; 
      } else {
         $lay['b_talk_name'] = $_talks[$lay['b_talk_code']]; 
      }
      //update the database
      if ( $lay['assigned'] == 0  ) {
        db_delete('emmaus_team_roster')
          ->condition('roster_id', $lay['roster_id'] )
          ->execute();
      } else {
        db_update('emmaus_team_roster')
          ->condition('roster_id', $lay['roster_id'] )
          ->fields( array(
            'position_code' => $lay['position_code'],
            'position_name' => $lay['position_name'],
            'p_talk_code'  => $lay['p_talk_code'],
            'p_talk_name'  => $lay['p_talk_name'],
            'b_talk_code'  => $lay['b_talk_code'],
            'b_talk_name'  => $lay['b_talk_name'],
            ))
          ->execute();
      } // if / else
    } //foreach
  } //if
  
  //Set a confirmation message
  drupal_set_message(t('Team assignments for @weekend have been updated.', array('@weekend'=>$form_state['values']['weekend_name']  )   )   );
   
}









//**********************************************************************************************************//
//*** Form builder function(s) to assign members to a team *************************************************//
//**********************************************************************************************************//
function emmaus_team_selection_form($form, &$form_state, $weekend=NULL, $type='L') {
  //var_dump($type);

  //Set up the form based on whether or we are adding or editing 
  switch ($weekend) {
    case NULL:  //Leadership selection report page.  No default weekend specified.
      break;
    default:  //else a default weekend was specified
      //Set the page title 
      if ($type=='C') { 
        drupal_set_title( $weekend['weekend_name'] . ' - Assign Clergy' );                
      } else {
        drupal_set_title( $weekend['weekend_name'] . ' - Assign Lay Team Members' );        
      }
      //$form['weekend_number'] = array('#type'=>'hidden', '#default_value'=>$weekend['weekend_number'] );
      break;
  }

  //query the database for weekends
  if (empty($weekend)) {  //only retrieve open weekends
    $weekends = _emmaus_weekend_weekends( NULL, 1 );
  } else {
    $weekends = _emmaus_weekend_weekends( );    
  }

  //retrieve the list of talks and positions and create other form element options
  $talks = array_merge( array(NULL=>'- None -') , _emmaus_team_talks($type) );
  $search_pos = array_merge( array(NULL=>'- None -') , _emmaus_team_positions($type)  );
  $assign_pos = array_merge( array(NULL=>'- None -') , _emmaus_team_positions($type)  );
  $count_option = array(0,1,2,3,4,5,6,7,8,9,10);
  $gender_option = array(NULL=>'- None -', 'M'=>'Male', 'F'=>'Female');
  
  //Define the search form elements
  $form['search'] = array( '#type'=>'fieldset', '#title'=>'Search Parameters'  );
  $form['search']['open_table']       = array( '#markup'=>'<table><tr>'  );
  $form['search']['search_min_teams'] = array('#type'=>'select',  '#title'=>'Min Teams',     '#options'=>$count_option,  '#default_value'=>0, '#prefix'=>'<td valign="top" width="20%">',  '#suffix'=>'</td>'  );
  $form['search']['search_gender']    = array('#type'=>'select',  '#title'=>'Gender',        '#options'=>$gender_option, '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );
  $form['search']['search_position']  = array( '#type'=>'select', '#title'=>'Position Held', '#options'=>$search_pos,    '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );
  $form['search']['search_talk']      = array( '#type'=>'select', '#title'=>'Talk Given',    '#options'=>$talks,         '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );
  $form['search']['row2'] = array( '#markup'=>'</tr><tr>' );
  $form['search']['search'] = array( '#type'=>'submit',  '#value'=>'Search', '#submit'=>array('emmaus_team_selection_form_submit'), '#limit_validation_errors'=>array() , '#prefix'=>'<td valign="bottom">',  );
  $form['search']['reset'] = array( '#type'=>'submit',   '#value'=>'Reset',  '#submit'=>array('emmaus_team_selection_form_submit'), '#limit_validation_errors'=>array() , '#suffix'=>'</td>'  );
  $form['search']['close_table']      = array( '#markup'=>'</tr></table>' );

  //**Query the database for a list of community profiles that are not clergy
  //@TODO: Check out this query because I don't think it needs the 'COUNT' function   
  $select = db_select('emmaus_community_profile', 'p'  );
    $select->fields('p', array('profile_id'));
    $select->leftJoin('emmaus_team_history', 'h', 'p.profile_id=h.profile_id'  );
    $select->addExpression( 'COUNT(h.history_id)', 'num_teams' );
    $select->groupBy('p.profile_id');
    if ($type=='L') { 
      //$select->condition('p.is_clergy','0');    
    } else {
      $select->condition('p.is_clergy','1');;
    }
    $select->orderBy('p.gender', 'ASC');
    $select->orderBy('p.full_name', 'ASC');
  $result = $select->execute();


  //Loop through the result and create an array of rows
  $results = array();
  foreach ($result as $row) {
    //load each user object so we have access to their history
    $profile = user_load($row->profile_id);    
    
    //apply search parameters for minimum teams
    if( !empty( $form_state['input']['search_min_teams']) && $profile->emmaus['team']['history']['weekends']['count']  < $form_state['input']['search_min_teams'] ) {
      continue;
    }
    //apply search parameters for gender
    if( !empty( $form_state['input']['search_gender']) && $profile->emmaus['profile']['gender'] != $form_state['input']['search_gender'] ) {
      continue;
    }
    //apply search parameters for position
    if( !empty( $form_state['input']['search_position']) && !in_array( $form_state['input']['search_position'], array_keys($profile->emmaus['team']['history']['positions']) ) ) {
      continue;
    }
    //apply search parameters for talks
    if( !empty( $form_state['input']['search_talk']) && !in_array( $form_state['input']['search_talk'], array_keys($profile->emmaus['team']['talks']['history']['primary']) ) ) {
      continue;
    }
   
    //set the last_weekend 
    //dpm($profile);
    $last_weekend = end($profile->emmaus['team']['history']['weekends']['local']); 
    //convert 'special_needs' field to an image
    $special_needs = '';
    if ( !empty($profile->emmaus['profile']['special_needs'])) {
      $special_needs = '<img title="' . $profile->emmaus['profile']['special_needs'] .  '" alt="Special Needs" src="/'. drupal_get_path('module','emmaus_community') . '/images/special_needs.png">';
    }
    
    if (!empty($profile->emmaus['profile']['is_clergy'])) {
      $full_name = '<a href="/user/' .  $profile->uid . '">' .  $profile->emmaus['profile']['full_name'] . ' (&dagger;)</a>';
    } else {
      $full_name = '<a href="/user/' .  $profile->uid . '">' .    $profile->emmaus['profile']['full_name'] . '</a>';
    }
    
  
    $results[ $profile->uid ] = array( 
      //'full_name'=> '<a href="/user/' .  $profile->uid . '">' .    $profile->emmaus['profile']['full_name'] . '</a>', 
      'full_name'=> $full_name, 
      'gender'=>$profile->emmaus['profile']['gender'], 
      'church_name'=>$profile->emmaus['profile']['church_name'], 
      //'location'=> $profile->emmaus['profile']['city'] . ', ' . $profile->emmaus['profile']['state'], 
      'num_teams' => $profile->emmaus['team']['history']['weekends']['count'], 
      'num_talks'=> count($profile->emmaus['team']['history']['talks']['primary']), 
      'talks_given' => implode(', ', array_keys($profile->emmaus['team']['history']['talks']['primary']) ),
      'positions_held' => implode(', ', array_keys($profile->emmaus['team']['history']['positions']) ),
      'last_weekend'=> $last_weekend['weekend_number'],
      'special_needs' => $special_needs,
      //'special_needs' => $profile->emmaus['profile']['special_needs'],
      );
  
  } //foreach



  
  //build a header for the table select element.  This will be passed to the form array
  $header = array(
    'full_name'   => t('Name'),
    'gender'      => t('Gender'),
    //'church_name' => t('Home Church'),
    //'location'    => t('Location'),
    //'num_talks'   => t('# Talks'),
    'positions_held'=> t('Positions'),
    'talks_given'   => t('Talks Given'),
    'num_teams'   => t('Teams'),
    'last_weekend'=> t('Last Weekend'),
    'special_needs'=> t('Needs'),
    );

  //define the form elements for the assignment form
  $form['profile_id'] = array( 
    '#type' => 'tableselect', 
    '#header' => $header, 
    '#options'=>$results, 
    '#empty' => t('No users found'), 
    //'#required'=>TRUE, 
    '#multiple'=>FALSE  
    );
  $form['open_table'] = array(
    '#markup'=>'<table><tr>'
    );
  $form['type'] = array(
    '#type'=>'hidden',
    '#default_value'=>$type,
  );
  $form['weekend_number'] = array(
    '#type'=>'select',  
    '#required'=>TRUE,
    '#title'=>'Assigned Weekend',
    '#options'=>$weekends,
    '#disabled'=> !empty($weekend),   
    '#prefix'=>'<td valign="top">',  
    '#suffix'=>'</td>'  
    );
  if ( !empty($weekend) ) { $form['weekend_number']['#default_value'] = $weekend['weekend_number'];}
  
  $form['position_code'] = array( 
    '#type'=>'select',   
    '#title'=>'Position',
    '#options'=>$assign_pos, 
    '#required'=>FALSE,
    '#prefix'=>'<td valign="top">',  
    '#suffix'=>'</td>'  
    );
  $form['position_name'] = array(
    '#type'=>'hidden',
    );
  $form['p_talk_code'] = array(
    '#type'=>'select',
    '#title'=>'Primary Talk',
    '#options'=>$talks,
    '#prefix'=>'<td valign="top">',  
    '#suffix'=>'</td>'  
    );
  $form['p_talk_name'] = array(
    '#type'=>'hidden',
    );
  $form['b_talk_code'] = array(
    '#type'=>'select',
    '#title'=>'Backup Talk',
    '#options'=>$talks,
    '#prefix'=>'<td valign="top">',  
    '#suffix'=>'</td>'  
    );
  $form['b_talk_name'] = array(
    '#type'=>'hidden',
    );
  $form['close_table'] = array( 
    '#markup'=>'</tr></table>'  
    );
  //define the submit button
  $form['submit'] = array(   
    '#type' => 'submit',   
    '#value' => t('Submit'), 
    '#disabled'=>empty($results)  
    );
  //return the form
  return $form;    
}
/****************************/
/*** Validate handler *******/
/****************************/
function emmaus_team_selection_form_validate($form, &$form_state) {
  //make sure a profile was selected
  if ( $form_state['values']['op'] == 'Submit' && empty($form_state['values']['profile_id']) ) {
    form_set_error('profile_id', 'You must select a user profile.');
  } 
  //make sure the profile isn't already assigned
  $result = db_select('emmaus_team_roster', 'r')
    ->fields('r')
    ->condition('r.profile_id' , $form_state['values']['profile_id'])
    ->condition('r.weekend_number' , $form_state['values']['weekend_number'])
    ->execute()
    ->fetchAllAssoc('roster_id', PDO::FETCH_ASSOC );
  //drupal_set_message('<pre>Count: ' . print_r( count($result), TRUE) . '</pre>');
  //drupal_set_message('<pre>' . print_r($result, TRUE) . '</pre>');
  if ( count($result) > 0 ) {
    form_set_error('profile_id', 'This user has already been assigned the weekend.  Please select another person.');
  }
  
}
/**************************/
/*** Submit handler *******/
/**************************/
function emmaus_team_selection_form_submit($form, &$form_state) {
  //drupal_set_message('<pre>' . print_r($form['position_code']['#options'], TRUE) . '</pre>');

  //Clear button has been clicked.  Reset the search form and reload
  if ( $form_state['values']['op']=='Reset' ) {
    $form_state['input']['search_min_teams'] = 0;
    $form_state['input']['search_gender'] = NULL;
    $form_state['input']['search_position'] = NULL;
    $form_state['input']['search_talk'] = NULL;
    $form_state['rebuild'] = TRUE;
    return;
  }
  //Search button has been clicked initially.  Set a flag and rebuild the form.
  if ( $form_state['values']['op']=='Search' ) {
    $form_state['rebuild'] = TRUE;
    return;
  }

        
  
    //update talk and position field values
  if ( $form_state['values']['position_code'] == '' ) {
     $form_state['values']['position_code'] = NULL; 
     $form_state['values']['position_name'] = NULL; 
  } else {
    $code = $form_state['values']['position_code'];
    $form_state['values']['position_name'] = $form['position_code']['#options'][$code]; 
  }
  if ( $form_state['values']['p_talk_code'] == '' ) {
    $form_state['values']['p_talk_code'] = NULL; 
    $form_state['values']['p_talk_name'] = NULL; 
  } else {
    $p_talk_code = $form_state['values']['p_talk_code'];
    $form_state['values']['p_talk_name'] = $form['p_talk_code']['#options'][$p_talk_code]; 
  }
  if ( $form_state['values']['b_talk_code'] == '' ) {
    $form_state['values']['b_talk_code'] = NULL; 
    $form_state['values']['b_talk_name'] = NULL; 
  } else {
    $b_talk_code = $form_state['values']['b_talk_code'];
    $form_state['values']['b_talk_name'] = $form['b_talk_code']['#options'][$b_talk_code]; 
  }
    
  //Before we write to the db, set any form values to literal NULL if they are empty strings.
  foreach ( $form_state['values'] as &$value ) {
    if (empty($value) ) { $value = NULL; }    
  }
  
  //Write the record to the database
  drupal_write_record('emmaus_team_roster', $form_state['values'] );
  //Set a confirmation message
  $profile_id = $form_state['values']['profile_id'];
  $user = user_load($form_state['values']['profile_id']);
  drupal_set_message(t('@name has been assigned to @weekend', array( '@name'=>$user->emmaus['profile']['full_name'],    '@weekend'=>$form_state['values']['weekend_number']  )   )   );
  //Should we redirect the form
  if ( !empty($form_state['values']['weekend_number']) ) {
    $form_state['redirect'] = 'emmaus/weekend/' . $form_state['values']['weekend_number'] . '/edit/team' ;    
  }

}

