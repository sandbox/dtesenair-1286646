<?php 
// $Id:   
   
  

/*************************************************************************************************************/
/*** Table/List builder function(s) to display existing talks in admin section *******************************/
/*************************************************************************************************************/
function emmaus_team_talks_admin() { 
	//Set the tag line
	$output = '<h3><p>Current talks defined for the ' . variable_get('emmaus_community_name') . '.</p></h3>';
  $output .= '<p>From this page, you can add, delete, or modify a weekend talk definition.</p>';
	//Define the table header
	$header = array(
		array('data' => '',         'width'=>'5%'	                         ),		//edit
		array('data' => '',         'width'=>'5%'	                         ),		//contact
    array('data' => 'Order',    'width'=>'10%', 'field'=>'talk_order'  ),
    array('data' => 'Talk Name',  'width'=>'20%'                       ),
    array('data' => 'Description'      ),
	);
	//Query the database
  $result = db_select('emmaus_team_talks', 't')
		->fields('t')
  	->extend('TableSort')					//Extend the query to include table sorting
		->orderBy('talk_order')
		->execute();
	
	//Loop through the result and create an array of rows
	$rows = array();
	foreach ($result as $row) {		
		//create links
		$edit_link = '<a href="/admin/config/emmaus/team/talks/' . $row->talk_id . '/edit"><img border="0" alt="Edit" title="Edit this talk" src="/'. drupal_get_path('module','emmaus_team') . '/images/edit.png"></a>';
		$delete_link = '<a href="/admin/config/emmaus/team/talks/' . $row->talk_id . '/delete"><img border="0" alt="Edit" title="Delete this talk" src="/'. drupal_get_path('module','emmaus_team') . '/images/drop.png"></a>';

		//output the table rows
		$rows[] = array($edit_link, $delete_link, $row->talk_order, $row->talk_name, $row->description );
	}
	//output the table
	$tbl_vars = array(  'header'=>$header,	'rows'=>$rows,	'attributes'=>array(),	'caption' => $result->rowCount() . ' row(s) returned.',		'colgroups'=>array(),		'sticky'=>TRUE, 	'empty'=>'No rows returned from query' 	);
	$output .= theme_table($tbl_vars);
	//output a link to add a new assignment
	//$output .= '<a href="/admin/emmaus/team/settings/talks/add"><img src="/'. drupal_get_path('module','emmaus_team') . '/images/add.png">Add a new talk</a>';
	//Return the output
	return $output;
}





/*************************************************************************************************************/
/*** Form builder function(s) to add a newtalks **********************************************************/
/*************************************************************************************************************/
function emmaus_team_talks_add_form() {
	// Define the form elements
  $form['talk_name']    = array( '#type' => 'textfield',  '#title' => t('Talk Name'),    '#required' => TRUE,  '#size' => 50,  '#maxlength' => 50,   '#description' => t('Enter a name of this talk.')       );
  $form['description']  = array( '#type' => 'textarea',   '#title' => t('Description'),  '#required' => FALSE, '#description' => t('Enter a name of this talk.')       );
  $form['weight']       = array( '#type' => 'weight',     '#title' => t('Weight'),       '#default_value'=>0,  '#delta'=>10,    '#description' => t('Optional. In the menu, the heavier items will sink and the lighter items will be positioned nearer the top.')  );	
  //Submit button
  $form['submit'] = array( '#type' => 'submit', '#value' => 'Submit', );
	//Return the form
  return $form;
}
/****************************/
/*** ADD Validate handler ***/
/****************************/
function emmaus_team_talks_add_form_validate($form, &$form_state) {
}
/**************************/
/*** ADD Submit handler ***/
/**************************/
function emmaus_team_talks_add_form_submit($form, &$form_state) {
	//Generate the insert query
  db_insert('emmaus_team_talks')
      ->fields(array(
            'talk_name'   => $form_state['values']['talk_name'],
            'description' => $form_state['values']['description'],
            'weight'      => $form_state['values']['weight'],
					)
				)
      ->execute();
	//Set a confirmation message
  drupal_set_message(t('Talk <em>@name</em> has been added.', array('@name' => $form_state['values']['talk_name'])));
	//Redirect the form
	$form_state['redirect'] = 'admin/config/emmaus/team/talks';
}




/*************************************************************************************************************/
/*** EDIT: Form builder - function(s) to edit an existing board member entry *********************************/
/*************************************************************************************************************/
function emmaus_team_talks_edit($form, &$form_state, $params) {
	var_dump($params);	
	//Query the database for the record to edit
  $result = db_select( 'emmaus_team_talks', 't' )
  	->fields('t')
		->condition( 't.talk_id', $params )
		->execute()
		->fetchAssoc();
	//Set a prettier page title		
	drupal_set_title( t('Edit @name Talk', array( '@name'=>$result['talk_name']) ) );
	// Define all the form elements
  $form['talk_id']			=	array( '#type' => 'hidden', 	 	'#title' => t('ID'), 			'#default_value' => $result['talk_id'], 				);
  $form['talk_name']	  = array( '#type' => 'textfield',	'#title' => t('Talk Name'),		'#required' => TRUE,  '#size' => 50,  '#maxlength' => 50, 	'#default_value' => $result['talk_name'],			'#description' => t('Enter a name of this talk.')   		);
  $form['description']  = array( '#type' => 'textarea',   '#title' => t('Description'), '#required' => FALSE, '#description' => t('Enter a name of this talk.'),  '#default_value'=>$result['description'],                                );
  $form['weight']       = array( '#type' => 'weight',     '#title' => t('Weight'),      '#delta'=>10,         '#description' => t('Optional. A numeric value to determien the order of the talk.'),  '#default_value'=>$result['weight'],  );  
	//Define the Submit buttons
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit'), );
  $form['cancel'] = array( '#type' => 'submit', '#value' => t('Cancel'), );
	//Return the form
 	return $form;
}
/***************************/
/* EDIT: Validate handler  */
/***************************/
function emmaus_team_talks_edit_validate($form, &$form_state) {
}
/************************/
/* EDIT: Submit handler */
/************************/
function emmaus_team_talks_edit_submit($form, &$form_state) {
	//Submit button was pressed.  Write the record.
	if ($form_state['values']['op']=='Submit') {
		drupal_write_record('emmaus_team_talks', $form_state['values'], 'talk_id');
		drupal_set_message(t('@name talk has been updated', array('@name' => $form_state['values']['talk_name'])));
	}
	//Redirect the form for all functions, including the cancel
	$form_state['redirect'] = 'admin/config/emmaus/team/talks';
}








/*********************************/
/*** DELETE: Confirmation Form ***/
/*********************************/
function emmaus_team_talks_delete($form, &$form_state, $params) {
	var_dump($params);
  // Always provide entity id in the same form key as in the entity edit form.
  $form['talk_id'] 		= array( '#type' => 'hidden',			'#default_value'=> $params	);
	//Return the confirmation form
  return confirm_form($form,
    t('Are you sure you want to delete the %name talk?', array('%name' => emmaus_team_talks_name($params) ) ),
    'admin/emmaus/teams/settings/talks',
    t('This action cannot be undone.  Any team history that has given this talk will be nullified.' ),
    t('Delete'),
    t('Cancel')
  );
}
/*********************************/
/*** DELETE: Submit Handler ******/
/*********************************/
function emmaus_team_talks_delete_submit($form, &$form_state) {
		var_dump($form_state);
  if ($form_state['values']['confirm']) {
		//get name before deleting
		$talk_name = emmaus_team_talks_name($form_state['values']['talk_id']);
		$num_deleted = db_delete('emmaus_team_talks')  
				->condition('talk_id', $form_state['values']['talk_id'])  
				->execute();
		
    drupal_set_message(t('"@name" talk has been deleted.', array('@name' => $talk_name) ) );
  }

  $form_state['redirect'] = 'admin/emmaus/team/settings/talks';
}




/*********************************************************************************************/
/** BEGIN CUSTOM FUNCTIONS *******************************************************************/
/*********************************************************************************************/
/**
 * Get church name for title callback
 */
function emmaus_team_talks_name($talk_id) {
	//Query the database
  $result = db_select('emmaus_team_talks','t')
    ->fields('t', array('talk_name')) 
		->condition('talk_id' , $talk_id)			//define a conditional query
		->range(0,1)	 												//determine the range of records... range(offset, number of records)
    ->execute()					//execute the query
		->fetchField();				//retrieve the first column
	//Return the output
	var_dump($result);
	return $result;
}

