<?php
// $Id: 
 


/*************************************************************************************************************/
/*** Table/List builder function(s) to display existing weekends for prayer clock signup *********************/
/*************************************************************************************************************/
function emmaus_prayerclock_signup() { 
  //Set the tag line
  $output = '<h3><p>Current weekends scheduled for the ' . variable_get('emmaus_community_name') . '.</p></h3>';
  //Set the tag line
  $output .= '<p>' . variable_get('emmaus_prayerclock_view_tag') . '</p>';


  //Define the table header
  $header = array(
    array( 'data'=>'Date',  'field'=>'start_date',  'sort'=>'asc',  'width'=>'20%'   ),
    array( 'data'=>'Name',  'field'=>'weekend_name',                'width'=>'35%'   ),
    array( 'data'=>'Slots Filled',                                                    ),
  );  

  //Query the database
  $select = db_select('emmaus_weekend_weekends', 'w');
  $select = $select->fields('w');
  $select = $select->condition('w.active', 1);
  $select = $select->extend('PagerDefault');        //extend the query to include a pager
  $select = $select->limit(20);                     //Set the number of items per page
  $select = $select->extend('TableSort');           //Extend the query to include table sorting
  $select = $select->orderByHeader($header);        //define sort column
  $weekends = $select->execute();

  //Loop through the result and create an array of rows
  $rows = array();
  foreach ($weekends as $weekend) {   
    //format the date
    $start = new DateTime("@$weekend->start_date");    //create a DateTime object directly from the timestamp integer stored in the DB
    $end = new DateTime("@$weekend->start_date");    //create a DateTime object directly from the timestamp integer stored in the DB
    date_modify($end, '+3 days');
    $weekend_date_rage = date_format($start, 'M d') . '-' . date_format($end, 'd, Y') ;    
    //$weekend_date_rage = date_format($start, 'M d, Y') ;    
    //format the main link
    $view_link = '<a href="/emmaus/weekend/' . $weekend->weekend_number . '/prayerclock/signup">' . $weekend->weekend_name . '</a>';

    //loop through the prayerclock to see if one is enabled
    $enabled = db_select('emmaus_prayerclock_clock', 'c')
      ->fields('c', array('enabled')) 
      ->condition('weekend_number' , $weekend->weekend_number)
      ->execute()
      ->fetchField();
    if ($enabled == 1 ) {
      $filled_slots = db_select('emmaus_prayerclock_slots', 's')
        ->fields('s') 
        ->condition('weekend_number' , $weekend->weekend_number)
        ->condition('name', NULL, 'IS NOT NULL' )
        ->execute();
      $total_slots = db_select('emmaus_prayerclock_slots', 's')
        ->fields('s') 
        ->condition('weekend_number' , $weekend->weekend_number)
        ->execute();
      $filled = $filled_slots->rowCount() . '/' . $total_slots->rowCount();
              //var_dump($total_slots);
      //echo '<br/><br/>' ;      
    } else {
      $filled = 'N/A';
    }

    //output the table rows
    $rows[] = array( $weekend_date_rage, $view_link, $filled           );
  }
  //output the table
  $table_caption = 'Click the name of a weekend to sign up for that weekend\'s prayer clock.';
  $tbl_vars = array(  'header'=>$header,  'rows'=>$rows,  'attributes'=>array(),  'caption'=>$table_caption,   'colgroups'=>array(),   'sticky'=>TRUE,   'empty'=>'There are no entries for this prayer clock.'  );
  $output .= theme_table($tbl_vars);
  //output the pager
  $output .= theme('pager');
  //Return the output
  return $output;
}













/***************************************************************************************************************/
/*** SIGNUP: Form builder - function(s) to edit the settings for a prayer clock ********************************/
/***************************************************************************************************************/
function emmaus_prayerclock_signup_form( $form, &$form_state, $params ) {
	//Get the weekend name
  $weekend = db_select('emmaus_weekend_weekends', 'w')
    ->fields('w') 
		->condition('weekend_number' , $params)			//define a conditional query
    ->execute()
		->fetchAssoc();
  //make sure the weekend is open and active
  if ($weekend['active']==0) { 
    drupal_set_message( t('The prayer vigil for this weekend is currently closed and connot be modified.'), 'warning');
    drupal_goto('emmaus/weekend/' . $params );
    return;
  }
	//Set the page title
	drupal_set_title( $weekend['weekend_name'] . ' - Prayer Clock Signup' );

  //Make sure that the prayer clock is enabled for this weekend before generating the table.
  $enabled = db_select('emmaus_prayerclock_clock', 'c')
    ->fields('c', array('enabled')) 
    ->condition('weekend_number' , $params)     //define a conditional query
    ->execute()
    ->fetchField();
  if ($enabled==0) { 
    drupal_set_message( t('The prayer clock for this weekend has not been enabled.'), 'warning');
    drupal_goto('emmaus/weekend/' . $params );
    return;
  }

	//attempt to get current user id and retrieve their name and email
	global $user;
	$account = user_load($user->uid);
  
  $default_name = '';
  $default_location = '';
  $default_email = '';

  //drupal_set_message( $account );
   
  if ( !empty($account->emmaus['profile']['full_name']) ) {
    $default_name = $account->emmaus['profile']['full_name'] ;
  } 
  if ( empty($account->emmaus['profile']['city'])==FALSE && empty($account->emmaus['profile']['state'])==FALSE  ) {
    $default_location = $account->emmaus['profile']['city'] . ', ' . $account->emmaus['profile']['state'] ;
  } 
	if ( empty($account->mail)==FALSE ) {
		$default_email = $account->mail;
	}

	//Query the database for available prayerclock slots
  $slots = db_select('emmaus_prayerclock_slots','pc')
    ->fields('pc')
		->condition('weekend_number' , $params)	
		->condition('name' , NULL)			       
		->orderBy('slot_datetime')
    ->execute();
	//Convert returned slots into an array for use in the form
	$open_slots = array();
  //This method returns an array of checkboxes
	foreach ($slots as $slot) {
		if ($slot->name == NULL ) {
			$open_slots[$slot->slot_datetime] = format_date( $slot->slot_datetime, 'custom', 'D, M j, Y @ g:i a');
		} else {
			$open_slots[$slot->slot_datetime] = format_date( $slot->slot_datetime, 'custom', 'D, M j, Y @ g:i a') . ' - ' . $slot->name ;		
		}
	}

	// Define the form elements
	$form['markup'] = array(	'#markup' => '<p>' .variable_get('emmaus_prayerclock_signup_tag') . '</p>',		'#weight' => -10000,		);

	$form['weekend_number']= array(	'#type' => 'hidden',	'#value'=>$params			);
  $form['name']     = array(  '#type' => 'textfield', '#title'=>'Name',           '#required'=>TRUE,    '#default_value'=>$default_name,      '#disabled'=>!empty($default_name) );
  $form['location'] = array(  '#type' => 'textfield', '#title'=>'Location',       '#required'=>TRUE,    '#default_value'=>$default_location,  '#disabled'=>!empty($default_location) );
	$form['email'] 	  = array(	'#type' => 'textfield', '#title'=>'Email Address',	'#required'=>FALSE,		'#default_value'=>$default_email,			'#disabled'=>!empty($default_email) );
	//Output a form element for each timeslot
  $form['open_slots']   = array(  '#type' => 'checkboxes', '#required'=>TRUE, '#title' => 'Choose one or more open slots', '#options'=>$open_slots, '#description'=>'Click the checkbox next to the date/time in which you choose'    );

	//This method returns an individual checkbox for each slot
  /*
  foreach ($slots as $slot) {
		if ($slot->name == NULL ) {
			$form['open_slots'][$slot->slot_datetime] = array( '#type'=>'checkbox', '#title'=> format_date( $slot->slot_datetime, 'custom', 'D, M j, Y @ g:i a'), );
		} else {
			$form[$slot->slot_datetime] = array( '#type'=>'checkbox', '#title'=> format_date( $slot->slot_datetime, 'custom', 'D, M j, Y @ g:i a') . ' - ' .$slot->name,  '#default_value'=>1, '#disabled'=>TRUE, '#prefix'=>'<div style="color: #cccccc; margin-top: 0; margin-bottom: 0;">', '#suffix'=>'</div>');
		}
	}
  */
	//Define the Submit buttons
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit'), );
  $form['cancel'] = array( '#type' => 'submit', '#value' => t('Cancel'), );
  //Return the form array
	return $form;
}
/*****************************/
/* SIGNUP: Validate handler  */
/*****************************/
function emmaus_prayerclock_signup_form_validate( $form, &$form_state ) {
}
/***************************/
/* SIGNUP: Submit handler  */
/***************************/
function emmaus_prayerclock_signup_form_submit( $form, &$form_state ) {
 
	foreach ($form_state['values']['open_slots'] as $key=>$timestamp ) {
		//update the database	
		db_update('emmaus_prayerclock_slots')
			->condition('weekend_number', $form_state['values']['weekend_number'])
			->condition('slot_datetime', $timestamp )
			->fields( array(
        'name'     => $form_state['values']['name'],
        'location' => $form_state['values']['location'],
				'email'    => $form_state['values']['email'],
				))
			->execute();
	}
 	//set a confirmation message
  drupal_set_message('Successfully signed up for the prayer clock. Thank you for your participation!');
  //drupal_set_message('TODO:  Write a cron routine that sends email to registered prayer clock email address <em>X</em> amount of time prior to their slot', 'warning');
	//redirect the form to the view page
	$form_state['redirect'] = 'emmaus/weekend/' . $form_state['values']['weekend_number'];

} //end function








/*************************************************************************************************************/
/*** EDIT: Form builder - function(s) to edit the settings for a prayer clock ********************************/
/*************************************************************************************************************/
function emmaus_prayerclock_edit_form( $form, &$form_state, $params ) {
  
  //Check to see if we should display the confirmation form
  if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Save Settings') {
    return emmaus_prayerclock_edit_form_confirm($form_state);
  } //confirmation form


  //Get the weekend name
  $weekend_name = db_select('emmaus_weekend_weekends', 'w')
    ->fields('w', array('weekend_name')) 
    ->condition('weekend_number' , $params)     //define a conditional query
    ->execute()
    ->fetchField();

  //Make sure a valid weekend has been specified in the params.  If so, set the page title.
  if ( empty($weekend_name) ) {
    drupal_set_message( t('The specified weekend could not be located!'), 'error');
    return $form;
  } 
  else { drupal_set_title( $weekend_name . ' - Edit Prayer Clock Settings' ); }
 

  //Define the option arrays for the different form elements & default values 
  //TODO: Change these values to use druapl variables ???
  $start_times = array( 
    '6'=>'6:00 AM', 
    '7'=>'7:00 AM', 
    '8'=>'8:00 AM', 
    '9'=>'9:00 AM', 
    '10'=>'10:00 AM', 
    '11'=>'11:00 AM', 
    '12'=>'12:00 PM', 
    '13'=>'1:00 PM', 
    '14'=>'2:00 PM', 
    '15'=>'3:00 PM',
    '16'=>'4:00 PM',
    '17'=>'5:00 PM',
    '18'=>'6:00 PM',
    '19'=>'7:00 PM',
    '20'=>'8:00 PM',
    '21'=>'9:00 PM',  
    );
  $intervals   = array( '10'=>'10 minutes', '15'=>'15 minutes', '30'=>'30 minutes', '60'=>'60 minutes');
  $durations   = array( '12'=>'12 Hours', '24'=>'24 Hours', '36'=>'36 Hours', '48'=>'48 Hours',  '72'=>'72 Hours');
  //$enabled = 0;
  //$start_time = 1900;
  //$slot_interval = 30;
  //$duration = 72;

  //Query the database for current prayerclock settings
  $settings = db_select('emmaus_prayerclock_clock','c')  //specify table name and alias
    ->fields('c')                               //retrieve all fields
    ->condition('weekend_number' , $params)     //define a conditional query
    ->execute()                                 //execute the query
    ->fetchAssoc();                             //retrieve the results into an array
  //If nothing was returned, set the values to the default system variables
  if (empty($settings)) {
    $settings['enabled'] = 0;
    $settings['start_time'] = variable_get('emmaus_prayerclock_default_start',19);
    $settings['slot_interval'] = variable_get('emmaus_prayerclock_default_interval',30) ;
    $settings['duration'] = variable_get('emmaus_prayerclock_default_duration',72); 
  }
  
  //dpm($settings, '$settings');
  
  //Query the database for prayerclock slots
  $slots = db_select('emmaus_prayerclock_slots','pc')
    ->fields('pc') 
    ->condition('weekend_number' , $params)     //define a conditional query
    ->orderBy('slot_datetime')
    ->execute();
    
  //Define the hidden form field elements
  $form_state['storage']['weekend_number'] = $params;
  $form_state['storage']['weekend_name']   = $weekend_name;
  $form_state['storage']['original_settings'] = $settings; 
  //$form['storage']['original_settings'] = $settings;
  
  
  //Define the form elements for the settings
  $form['settings'] = array( 
    '#type'=>'fieldset', 
    '#title'=>'Settings (click to expand/collapse)', 
    '#collapsible'=>TRUE, 
    '#tree'=>TRUE,
    '#collapsed'=> $form_state['storage']['original_settings']['enabled'] 
    );
  $form['settings']['markup']     = array(  '#markup' => '<div class="messages warning">Warning: Modifying these settings after a prayer clock has any filled slots WILL result in a loss of data.  Please proceed with caution.</div>',   '#weight' => -100,    );
  $form['settings']['enabled']    = array(  '#type' => 'checkbox',  '#default_value'=>$settings['enabled'],       '#title' => t('Enable a prayer clock for this weekend?'), '#weight' => -99,     );
  $form['settings']['start_time'] = array(  '#type' => 'select',    '#default_value'=>$settings['start_time'],    '#title' => t('Start Time'),      '#options' => $start_times,     
      '#states' => array( 'enabled' => array( ':input[name="settings[enabled]"]' => array('checked' => TRUE), ),  ),
  );
  $form['settings']['slot_interval']  = array(  '#type' => 'select',    '#default_value'=>$settings['slot_interval'], '#title' => t('Slot Interval'),   '#options' => $intervals,   '#description' => 'How long each prayer clock slot lasts.',
      '#states' => array( 'enabled' => array( ':input[name="settings[enabled]"]' => array('checked' => TRUE), ),  ),
  );
  $form['settings']['duration']       = array(  '#type' => 'select',    '#default_value'=>$settings['duration'],      '#title' => t('Duration'),        '#options' => $durations,   '#description' => 'How long should this prayer clock last (in hours)?', 
      '#states' => array( 'enabled' => array( ':input[name="settings[enabled]"]' => array('checked' => TRUE), ),  ),
  );
  $form['settings']['save'] = array( 
    '#type' => 'submit', 
    '#value' => t('Save Settings'),
    '#submit' => array('emmaus_prayerclock_edit_form_submit'),
    '#element_validate'=>array('emmaus_prayerclock_edit_form_validate',)
  );  
  $form['settings']['cancel'] = array( 
    '#type' => 'submit', 
    '#value' => t('Cancel'),
    '#submit' => array('emmaus_prayerclock_edit_form_submit'),
    '#element_validate'=>array('emmaus_prayerclock_edit_form_validate',)
  );  
  
  

  
  // Define the form elements for editing existing slots
  $form['slots'] = array( 
    '#type'=>'fieldset', 
    '#tree'=>TRUE,  
    '#title'=>'Edit Prayer Clock Slots (click to expand/collapse)', 
    '#collapsible'=>TRUE, 
    '#collapsed'=>FALSE,
    '#states' => array( 
      'visible' => array( 
        ':input[name="settings[enabled]"]' => array('checked' => TRUE), 
        ),  
      ),
  );


  //Check to see if time slots were returned.  If so, output a form element for each timeslot
  if ($slots->rowCount() == 0 ) {
    $form['slots']['empty'] = array( '#markup'=>'<p>No slots were returned.  Enable the prayerclock for this weekend, save the form, and try again.</p>'  );
    $form['slots']['num_slots'] = array( '#type'=>'hidden', '#default_value'=>$slots->rowCount()  );
  } else {
    $form['slots']['markup'] = array( '#markup' => '<p>Use this form to perform mass updates to this prayer clock.  When you are finished, click the Update Time Slots button.</p>'   );
    $form['slots']['num_slots'] = array( '#type'=>'hidden', '#default_value'=>$slots->rowCount()  );

    //Define the Submit buttons
    $form['slots']['submit_top'] = array( '#type' => 'submit', '#value' => t('Update Time Slots'));
    $form['slots']['export_top'] = array( 
      '#type' => 'submit', 
      '#value' => t('Export to Excel'),
      //'#disabled'=>TRUE,
      );
    
    //Create a table time slots    
    $form['slots']['open_table'] = array(   '#markup' => '<table><thead><tr><th>Day</th><th>Time</th><th>Name</th><th>Location</th><th>Email Address</th></tr></thead>'   );
    foreach ($slots as $slot) {   
      $form['slots'][$slot->slot_datetime]['slot_datetime'] = array( '#type'=>'hidden',   '#default_value' => $slot->slot_datetime  );
      $form['slots'][$slot->slot_datetime]['day']       = array( '#type'=>'item',     '#title' => format_date( $slot->slot_datetime, 'custom', 'l')  ,      '#prefix' =>'<tr><td>',   '#suffix' =>'</td>',      );
      $form['slots'][$slot->slot_datetime]['time']      = array( '#type'=>'item',     '#title' => format_date( $slot->slot_datetime, 'custom', 'g:i A')  ,  '#prefix' =>'<td>',   '#suffix' =>'</td>',      );
      $form['slots'][$slot->slot_datetime]['name']      = array( '#type'=>'textfield', '#size'=>20,  '#default_value' => $slot->name,     '#prefix' =>'<td>', '#suffix' =>'</td>',         );
      $form['slots'][$slot->slot_datetime]['location']  = array( '#type'=>'textfield', '#size'=>20,  '#default_value' => $slot->location, '#prefix' =>'<td>', '#suffix' =>'</td>',         );
      $form['slots'][$slot->slot_datetime]['email']     = array( '#type'=>'textfield',  '#size'=>20,  '#default_value' => $slot->email,    '#prefix' =>'<td>',  '#suffix' =>'</td></tr>',   );
    } 
    $form['slots']['close_table'] = array( '#markup' => '</table>'   );

    //Define the Submit buttons on the bottom
    $form['slots']['submit_bot'] = array( '#type' => 'submit', '#value' => t('Update Time Slots'));
    $form['slots']['export_bot'] = array( 
      '#type' => 'submit', 
      '#value' => t('Export to Excel'),
      //'#disabled'=>TRUE,
      );


  }
  
  //dpm($form_state, '$form_state')
  //dpm($form, '$form');
  
  //Return the form array
  return $form;
}


/***************************/
/* EDIT: Validate handler  */
/***************************/
function emmaus_prayerclock_edit_form_validate( $form, &$form_state ) {
  //drupal_set_message("emmaus_prayerclock_edit_form_validate() function called");
}



function emmaus_prayerclock_edit_form_confirm(&$form_state) {
  //echo("emmaus_prayerclock_edit_form_confirm() function called");
  //drupal_set_message("emmaus_prayerclock_edit_form_confirm() function called");
  //return;
   
  //dpm($form_state, '$form_state (confirm)');
 
  // Define a couple of form elements again, prior to final submission, so we can pass the correct values

  $form['settings'] = array('#type'=>'fieldset', '#default_value'=>NULL, '#tree'=>TRUE, '#access'=>FALSE );
  $form['settings']['enabled']     = array( '#type'=>'hidden',  '#default_value' => $form_state['values']['settings']['enabled']    );
  $form['settings']['start_time']     = array( '#type'=>'hidden',  '#default_value' => $form_state['values']['settings']['start_time']    );
  $form['settings']['duration']       = array( '#type'=>'hidden',  '#default_value' => $form_state['values']['settings']['duration']    );
  $form['settings']['slot_interval']  = array( '#type'=>'hidden',  '#default_value' => $form_state['values']['settings']['slot_interval']    );
    
    //set a flag to tell the submit function to regenerate the entire clock
    $form_state['storage']['regenerate'] = TRUE;  

    $msg = "Changing these settings will require that the entire prayer clock be rebuilt.  All exising slots will be removed.  This can not be undone.";

    //Display the confirmation form    
    return confirm_form(
      $form, 
      t('Are you sure that you want to modify the prayer clock settings for <em>@name</em>?', array('@name'=>$form_state['storage']['weekend_name'])), 
      'emmaus/weekend/' . $form_state['storage']['weekend_number'] . '/edit/prayerclock'  , 
      t($msg), 
      'Proceed', 'Cancel');
  
}


/*************************/
/* EDIT: Submit handler  */
/*************************/
function emmaus_prayerclock_edit_form_submit( $form, &$form_state ) {
  //drupal_set_message("emmaus_prayerclock_edit_form_submit() function called");
  //dpm($form_state, '$form_state (submit)');
              
  //CANCEL button clicked.  Redirect to view page.
  if ($form_state['values']['op']=='Cancel') {
    $form_state['redirect'] = 'emmaus/weekend/' . $form_state['storage']['weekend_number'];
    return;
  }
  
  //EXPORT button clicked.
  if ($form_state['values']['op']=='Export to Excel') {
    $form_state['redirect'] = 'emmaus/weekend/' . $form_state['storage']['weekend_number'] . '/prayerclock/export-xls' ;
    //drupal_set_message("EXPORT button clicked");
    //_customer_export($form_state['storage']['weekend_number']);
    return;
  }
  
  
  //SAVE SETTINGS button has been clicked initially, but not confirmed.  Set a flag and rebuild the form.
  if ($form_state['values']['op']=='Save Settings' && !isset($form_state['storage']['confirm']) ) 
  {
      if ($form_state['values']['settings']['start_time'] != $form_state['storage']['original_settings']['start_time'] ||
          $form_state['values']['settings']['duration'] != $form_state['storage']['original_settings']['duration'] ||
          $form_state['values']['settings']['slot_interval'] != $form_state['storage']['original_settings']['slot_interval'] ) 
          {
            $form_state['storage']['confirm'] = TRUE; //set a flag in the $form_state variable stating that we need to display a confirmation form      
            $form_state['rebuild'] = TRUE; // casue the form to tbe rebuilt
            return;    
          }
  }


  //SAVE SETTINGS has been clicked but does not require confirmation OR confirmation has been made.
  if ($form_state['values']['op']=='Save Settings' ||
     ($form_state['values']['op']=='Proceed' && $form_state['storage']['confirm'] == TRUE ) ) {
    
    //Update the clock database with the new settings
    db_merge('emmaus_prayerclock_clock')
      ->key( array('weekend_number' => $form_state['storage']['weekend_number'] ) )
      ->fields(array(
          'enabled' => $form_state['values']['settings']['enabled'],
          'start_time' => $form_state['values']['settings']['start_time'],
          'slot_interval' => $form_state['values']['settings']['slot_interval'],
          'duration' => $form_state['values']['settings']['duration'],
        )
      )
      ->execute();
       
      
      //determine if there are any rows in the clock table for this walk.  If not, perform a rebuild.
      $slotcount = db_select('emmaus_prayerclock_slots','s')  //specify table name and alias
        ->fields('s')                               //retrieve all fields
        ->condition('weekend_number' , $form_state['storage']['weekend_number'])     //define a conditional query
        ->execute()
        ->rowCount();
      if ($slotcount == 0 && $form_state['values']['settings']['enabled'] == TRUE )
      {
        $form_state['storage']['regenerate'] = TRUE;
        //drupal_set_message(t('@count rows exist for @name', array('@count'=>$slotcount, '@name' => $form_state['storage']['weekend_number'])));
      }
       
      
      drupal_set_message(t('Prayer Clock settings for weekend @name have been updated', array('@name' => $form_state['storage']['weekend_number'])));
    
    
    if (isset($form_state['storage']['regenerate']) == TRUE) {
      _emmaus_prayerclock_regenerate($form_state['storage']['weekend_number']);
    }

  }


  //UPDATE TIME SLOTS button was pressed.  
  elseif ($form_state['values']['op']=='Update Time Slots') {
    //drupal_set_message('UPDATE TIME SLOTS button clicked');
    
  
    //Loop through the returned slots and update the matching rows based on the form values
    foreach ($form_state['values']['slots'] as $cur_slot) {
      if (isset($cur_slot['name'])) {
        //check to see if name and email are empty, first.  if so, set them to NULL.
        //if ($cur_slot['name'] == '') { $cur_slot['name'] = NULL; }
        //if ($cur_slot['location'] == '' ) { $cur_slot['location'] = NULL; }
        //if ($cur_slot['email'] == '' ) { $cur_slot['email'] = NULL; }
        //update the database 
        db_update('emmaus_prayerclock_slots')
          ->condition('weekend_number', $form_state['storage']['weekend_number'])
          ->condition('slot_datetime', $cur_slot['slot_datetime'] )
          ->fields( array(
             'name' => ( $cur_slot['name'] == '' ? NULL : $cur_slot['name'] ),
             'location' => ( $cur_slot['location'] == '' ? NULL : $cur_slot['location'] ),
             'email' => ( $cur_slot['email'] == '' ? NULL : $cur_slot['email'] ),
            ))
          ->execute();
      } //if
    } //foreach
    //set a confirmation message
    drupal_set_message( t('Prayer clock time slot entries were successfully updated'));
  }   


} //end function



function _emmaus_prayerclock_regenerate($weekend_number) {

  //Query the database for current prayerclock settings
  $clock = db_select('emmaus_prayerclock_clock','c')  //specify table name and alias
    ->fields('c')                               //retrieve all fields
    ->condition('weekend_number' , $weekend_number)     //define a conditional query
    ->execute()                                 //execute the query
    ->fetchAssoc();     



    //Delete all of the existing slots for this walk from the table so we can recalculate the number of slots needed    
    $num_deleted = db_delete('emmaus_prayerclock_slots')  
      ->condition('weekend_number', $weekend_number )  
      ->execute();

   //Generate a corrected timestamp for the clock start based on the weekend start date and clock start time
   $weekend_start = db_select('emmaus_weekend_weekends' ,'w')
      ->fields('w', array('start_date'))
      ->condition('w.weekend_number', $weekend_number )
      ->execute()
      ->fetchField();

    //calculate the starting and ending date/time stamps
    $clock_start = mktime ( $clock['start_time'] , 0, 0, date('n', $weekend_start), date('j', $weekend_start), date('Y', $weekend_start));
    $clock_end = mktime ( $clock['start_time'] + $clock['duration'] , 0, 0, date('n', $clock_start), date('j', $clock_start), date('Y', $clock_start));
    //Insert the appropriate number of rows into the database
      $slot = $clock_start;
      $count = 0;
      //Insert rows   
      while ( $slot < $clock_end ):
        echo $slot . ' <-> ' . $clock_end;
        //insert the slot
        db_insert('emmaus_prayerclock_slots')
          ->fields( array (
              'weekend_number'=>$weekend_number,
              'slot_datetime'=> $slot,
              'name' => NULL,
              'location' => NULL,
              'email' => NULL,
            ))
          ->execute();
        //increment the slot
        $slot = mktime (  date('H',$slot) , date('i',$slot) + $clock['slot_interval'] , 0, date('n', $slot), date('j', $slot), date('Y', $slot) );
        //increment the counter
        $count ++;
      endwhile;
 
    //DEBUG messages
    //dpm($clock);       
    //drupal_set_message('$weekend_start='. $weekend_start);     
    //drupal_set_message(t('@num_deleted slots were removed from the Prayer Clock for weekend @weekend_number.', array( '@num_deleted'=>$num_deleted, '@weekend_number' => $weekend_number ) ) );
    //drupal_set_message('$clock_start: ' . $clock_start . ' (' . format_date($clock_start, 'medium') . ')'  );
    //drupal_set_message('$clock_end: ' . $clock_end . ' ('  . format_date($clock_end, 'medium') . ')'  );
 
  
}














