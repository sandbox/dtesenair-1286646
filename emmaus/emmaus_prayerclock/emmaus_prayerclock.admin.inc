<?php
// $Id: 

  
/**
 * Form builder function to configure global settings for the prayerclock module.
 */
function emmaus_prayerclock_admin_form() {
	//Define arrays for use as options in form elements
	$start_times = array( 
   6=>'6:00 AM',
   7=>'7:00 AM',
   8=>'8:00 AM',
   9=>'9:00 AM',
   10=>'10:00 AM',
   11=>'11:00 AM',
   12=>'12:00 PM',
	 13=>'1:00 PM',
	 14=>'2:00 PM',
	 15=>'3:00 PM',
	 16=>'4:00 PM',
	 17=>'5:00 PM',
	 18=>'6:00 PM',
	 19=>'7:00 PM',
	 20=>'8:00 PM',
	 21=>'9:00 PM',
   );
	$intervals 	 = array( 10=>'10 minutes', 15=>'15 minutes', 30=>'30 minutes', 60=>'60 minutes');
	$durations 	 = array( 12=>'12 Hours', 24=>'24 Hours', 36=>'36 Hours', 48=>'48 Hours',  72=>'72 Hours');
	
	//Define the form elements
	$form['emmaus_prayerclock_default_start'] 		= array(	'#type'=>'select',		'#title'=>t('Default Start Time'),					'#options'=>$start_times,	'#default_value'=>variable_get('emmaus_prayerclock_default_start', 19),		'#description'=>t('Choose the default start time for new prayer clocks.'),					);
	$form['emmaus_prayerclock_default_duration'] 	= array(	'#type'=>'select',		'#title'=>t('Default Clock Duration'),			'#options'=>$durations,		'#default_value'=>variable_get('emmaus_prayerclock_default_duration', 72),	'#description'=>t('Choose the default duration of new prayer clocks.'),							);
	$form['emmaus_prayerclock_default_interval'] 	= array(	'#type'=>'select',		'#title'=>t('Default Time Interval'),				'#options'=>$intervals,		'#default_value'=>variable_get('emmaus_prayerclock_default_interval', 15),	'#description'=>t('Choose the default time interval for new paryer clocks.'),				);
	$form['emmaus_prayerclock_view_tag']				 	= array(	'#type'=>'textarea',	'#title'=>t('Tag Line for "View" Page'),															'#default_value'=>variable_get('emmaus_prayerclock_view_tag', ''),																																																);
	$form['emmaus_prayerclock_signup_tag'] 				= array(	'#type'=>'textarea',	'#title'=>t('Tag Line for "Sign Up" Page'),														'#default_value'=>variable_get('emmaus_prayerclock_signup_tag', '')																																																);

	//Set the function that will be called on submit
	$form['#submit'][] = 'prayerclock_admin_settings_submit';
	// Return the form
	return system_settings_form($form);
}


/**
* Process annotation settings submission.
*/
function prayerclock_admin_settings_submit($form, $form_state) {
	//TODO:  Find out if this function call is PRE or POST form validation.  I think it's POST.	
}

