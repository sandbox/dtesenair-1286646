<?php
// $Id: 

/*
 * @file
 * Administrative functions for the community module
 */


/**
 * Form builder function to configure global settings for the community module
 */
function emmaus_community_settings() {
	// Build the form
  $form['emmaus_community_name'] = array( 
    '#type' => 'textfield',  
    '#title' => t('Emmaus Community Name'),     
    '#default_value' => variable_get('emmaus_community_name', ''),      
    '#description' => t('Please enter the name of your Emmaus or Chrisyalis Community.  e.g. - "Aldersgate Emmaus Community"'),
    );
  $form['emmaus_community_short_name'] = array( 
    '#type' => 'textfield',  
    '#title' => t('Emmaus Community Short Name'),     
    '#default_value' => variable_get('emmaus_community_short_name', ''),      
    '#description' => t('Please enter a SHORT name of your Emmaus or Chrysalis Community.  e.g. - "Aldersgate"'),
    );
  $form['emmaus_community_override_usernames'] = array( 
    '#type' => 'checkbox',  
    '#title' => t('Override usernames throughout site'),     
    '#default_value' => variable_get('emmaus_community_override_usernames', ''),      
    '#description' => t('Check this box to display usernames on site using "Full Name" value from profile.'),
    );
  $form['emmaus_community_auto_add_church'] = array( 
    '#type' => 'checkbox',  
    '#title' => t('Auto-add churches during profile creation?'),     
    '#default_value' => variable_get('emmaus_community_auto_add_church', ''),      
    '#description' => t('Automatically add churches to the database if no match is found during user profile creation?'),
    );
  $form['emmaus_community_auto_add_group'] = array( 
    '#type' => 'checkbox',  
    '#title' => t('Auto-add reunion groups during profile creation?'),     
    '#default_value' => variable_get('emmaus_community_auto_add_group', ''),      
    '#description' => t('Automatically add reunion groups to the database if no match is found during user profile creation?'),
    );
	
	//Set the function that will be called on submit
	//$form['#submit'][] = 'emmaus_admin_settings_submit';
	
	// Return the form
	return system_settings_form($form);
}
