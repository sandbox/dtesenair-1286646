<?php 
// $Id:   
/*
 * @file
 * Include file for Emmaus Profile - Group
 */
  
   
  
/*************************************************************************************************************/
/*** Table/List builder function(s) to display existing groups *********************************************/
/*************************************************************************************************************/
function emmaus_community_group_list() { 
	//Set the tag line
	$output = '<h3><p>Current reunion groups defined for the ' . variable_get('emmaus_community_name') . '.</p></h3>';
  $output .= '<p>Click on a group to see additional details.</p>';
	//Define the table header
	$header = array(
		array('data' => '', 'width'=>'40' ),		//contact
		array('data' => 'Group Name', 'field'=>'group_name', 'sort'=>'asc' 		  ),
		array('data' => 'City',       'field'=>'city'  	                        ),
		array('data' => 'Contact Name'                                          ),
		array('data' => 'Notes'	),
		array('data' => 'Accepting', 'field'=>'is_accepting'  ),
	);
	//Query the database
  $result = db_select('emmaus_community_group', 'g')
		->fields('g')
  	->extend('PagerDefault')				//extend the query to include a pager
  	->limit(20)										//Set the number of items per page
  	->extend('TableSort')					//Extend the query to include table sorting
  	->orderByHeader($header)			//define sort column
		->execute();
	
	//Loop through the result and create an array of rows
	$rows = array();
	foreach ($result as $row) {		
		//view links
		$view_link = '<a href="/emmaus/community/group/' . $row->group_id . '">' . $row->group_name. '</a>';
		//create links
		$email_link='';
		if ( strlen($row->contact_email) > 0 ) {
			$email_link = '<a href="mailto:' . $row->contact_email . '"><img border="0" alt="Contact" title="Contact this reunion group" src="/'. drupal_get_path('module','emmaus_community') . '/images/envelope_12.jpg"></a>';
		}
		//Convert is_accepting integer to image
		$accepting = array('data'=>'-','align'=>'center');
		if ( $row->is_accepting == 1 ) {
			$accepting = array('data'=>'<img alt="Yes" src="/'. drupal_get_path('module','emmaus_community') . '/images/check.png">', 'align'=>'center');
		}
		//Convert notes text field to an image
		$notes = '';
		if ( strlen($row->notes) > 0 ) {
			$notes = array('data'=>'<img title="' . $row->notes .  '" alt="Notes" src="/'. drupal_get_path('module','emmaus_community') . '/images/document.gif">', 'align'=>'center');
		}

		//output the table rows
		$rows[] = array($email_link, $view_link, $row->city, $row->contact_name, $notes, $accepting );
	}
	//output the table
	$tbl_vars = array(  'header'=>$header,	'rows'=>$rows,	'attributes'=>array(),	'caption' => $result->rowCount() . ' row(s) returned.',		'colgroups'=>array(),		'sticky'=>TRUE, 	'empty'=>'No rows returned from query' 	);
	$output .= theme_table($tbl_vars);

	//output the pager
	$output .= theme('pager');
	//Return the output
	return $output;
}



/*************************************************************************************************************/
/*** View builder function(s) to display existing church *****************************************************/
/*************************************************************************************************************/
function emmaus_community_group_view($group_id) { 
	//Query the database
  $result = db_select('emmaus_community_group','g')
    ->fields('g') 
		->range(0,1)	 								//determine the range of records... range(offset, number of records)
		->condition('group_id' , $group_id)					//define a conditional query
    ->execute();
	//Loop through the result and create an array of rows
	foreach ($result as $row) {		
		$group['group_id'] = $row->group_id;
		$group['group_name'] = $row->group_name;
		$group['notes'] = $row->notes;
		$group['city'] = $row->city;
		$group['contact_name'] = $row->contact_name;
		$group['contact_phone'] = $row->contact_phone;
		$group['contact_email'] = $row->contact_email;
		$group['is_accepting'] = $row->is_accepting;
	}
	//Generate the HTML for the group display
	if (!isset($group)) {
		$markup='Group information could not be located.  Please try again.';
	} else {
		$markup  = '<p><b>Location:</b> ' . $group['city'] . '</p>';
		$markup .= '<p><b>Contact Name:</b> ' . $group['contact_name'] . '<br/><b>Contact Phone:</b> ' . $group['contact_phone'] . '<br/><b>Contact Email:</b> ' . $group['contact_email'] . '</p>';
    //convert 'is_accepting' integer to an image
    $is_accepting = '';
    if ( $group['is_accepting'] == 1 ) { $is_accepting = '<img border="0" alt="Yes" src="/'. drupal_get_path('module','emmaus_community') . '/images/check.png">';  }
		$markup .= '<p><b>Accepting Members?</b> ' . $is_accepting . '</p>';


		$markup .= '<p><b>Notes:</b> ' . $group['notes'] . '</p>';
	}
	//Get a table for the member list
	$members = emmaus_community_group_get_members($group['group_name']);
	$markup .= $members;
	
	return $markup;
}





/*************************************************************************************************************/
/*** Form builder function(s) to add a new group **********************************************************/
/*************************************************************************************************************/
function emmaus_community_group_add() {
	// Define the form elements
  $form['community_group_add'] 	= array( '#type' => 'item', 			'#title' => t('Add a new reunion group.'),  );
  $form['group_name'] 		= array( '#type' => 'textfield',	'#title' => t('Group Name'),		'#required' => TRUE,  '#size' => 50,  '#maxlength' => 50, 	'#description' => t('Enter a name of this group.')   		);
  $form['city'] 					= array( '#type' => 'textfield',	'#title' => t('City'),					'#required' => FALSE, '#size' => 50,  '#maxlength' => 50, 	'#description' => t('Enter the city and state (optionally) in which this group meets.')   		);
  $form['contact_name'] 	= array( '#type' => 'textfield',	'#title' => t('Contact Name'),	'#required' => FALSE, '#size' => 50,  '#maxlength' => 50, 	'#description' => t(''),    '#autocomplete_path' => 'emmaus/community/profile/autocomplete', 		);
  $form['contact_phone'] 	= array( '#type' => 'textfield',	'#title' => t('Contact Phone'),	'#required' => FALSE, '#size' => 20,  '#maxlength' => 20, 	'#description' => t('')   		);
  $form['contact_email'] 	= array( '#type' => 'textfield',	'#title' => t('Contact Email'),	'#required' => FALSE, '#size' => 50,  '#maxlength' => 50, 	'#description' => t('')   		);
  $form['notes'] 					= array( '#type' => 'textarea',		'#title' => t('Notes'),					'#required' => FALSE, '#size' => 150, '#maxlength' => 255,	'#description' => t('')   		);
	$form['is_accepting']  	= array( '#type' => 'checkbox',  	'#title' => t('Accepting?'), 		'#required' => FALSE, '#description' => t('Is this reunion group accepting new members?'), );
	//Submit button
  $form['submit'] = array( '#type' => 'submit', '#value' => 'Submit', );
	//Return the form
  return $form;
}
/****************************/
/*** ADD Validate handler ***/
/****************************/
function emmaus_community_group_add_validate($form, &$form_state) {
}
/**************************/
/*** ADD Submit handler ***/
/**************************/
function emmaus_community_group_add_submit($form, &$form_state) {
	//Generate the insert query
  db_insert('emmaus_community_group')
      ->fields(array(
						'group_name'		=> $form_state['values']['group_name'],
						'city' 					=> $form_state['values']['city'],
						'contact_name' 	=> $form_state['values']['contact_name'],
						'contact_phone' => $form_state['values']['contact_phone'],
						'contact_email' => $form_state['values']['contact_email'],
						'notes' 				=> $form_state['values']['notes'],
						'is_accepting'	=> $form_state['values']['is_accepting'],
					)
				)
      ->execute();
	//Set a confirmation message
  drupal_set_message(t('The reunion group <em>@name</em> has been added.', array('@name' => $form_state['values']['group_name'])));
	//Redirect the form
	$form_state['redirect'] = 'emmaus/community/group';
}




/*************************************************************************************************************/
/*** EDIT: Form builder - function(s) to edit an existing board member entry *********************************/
/*************************************************************************************************************/
function emmaus_community_group_edit($form, &$form_state, $params) {
	//Check to see if we should display the confirmation form
	if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Delete') {
    //Do a quick query to see how many people have this church so we can display the effects on the form
    $num_members = db_select('emmaus_community_profile', 'p')
      ->fields('p', array('profile_id') )
      ->condition('p.group_name', $form_state['values']['group_name'] )
      ->execute()
      ->rowCount();    
    // Define form elements again, prior to final submission
    $form['group_markup']    = array( '#markup'=> t('<p><em>A total of @number community profiles will be affected by deleting this reunion group.</em></p>', array('@number'=>$num_members)  ),   );
    $form['group_affected']  = array( '#type'=>'hidden', '#title'=> t('Affected Profiles'),  '#default_value' => $num_members                         );
    $form['group_id']        = array( '#type'=>'hidden', '#title'=> t('Group ID'),           '#default_value' => $form_state['values']['group_id']    );
    $form['group_name']      = array( '#type'=>'hidden', '#title'=> t('Group Name'),         '#default_value' => $form_state['values']['group_name']  );
    //Display the confirmation form
    return confirm_form($form, t('Are you sure that you want to delete <em>@name</em>?', array('@name'=>$form_state['values']['group_name'])), 'emmaus/community/group/' . $form_state['values']['group_id']   , 'This action can not be undone.  Please proceed with caution.', 'Remove', 'Don\'t Remove'); //Had better luck leaving off last param 'name'  
  } //confirmation form
	
	//Query the database for the record to edit
  $result = db_select( 'emmaus_community_group', 'g' )
  	->fields( 'g')
		->condition( 'g.group_id',$params )
		->execute();
	//Make sure only one entry was returned.  If not, display an error
	//TODO:  Make a prettier form when an error is returned.  Perhaps add a link to the list page.
	if ( !$result->rowcount() == 1 ) {
		drupal_set_message(  t('Unable to locate the group specified in the URL (Param: @param).  Please try your query again. (Rows returned: @number)' , array('@number'=>$result->rowcount(), '@param'=>$params )) ,'error');
		return '';
	}

	//One result confirmed.  Set that row into a variable
	foreach ($result as $row) {	$group = $row; }

	// Define all the form elements
  $form['group_id']				=	array( '#type' => 'hidden', 	 	'#title' => t('ID'), 			'#default_value' => $group->group_id, );
  $form['group_name']			= array( '#type' => 'textfield',	'#title' => t('Group Name'),		'#required' => TRUE,  '#size' => 50,  '#maxlength' => 50, 	'#default_value' => $group->group_name,			'#description' => t('Enter a name of this group.')   		);
  $form['city'] 					= array( '#type' => 'textfield',	'#title' => t('City'),					'#required' => FALSE, '#size' => 50,  '#maxlength' => 50, 	'#default_value' => $group->city,  					'#description' => t('Enter the city and state (optionally) in which this group meets.')   		);
  $form['contact_name'] 	= array( '#type' => 'textfield',	'#title' => t('Contact Name'),	'#required' => FALSE, '#size' => 50,  '#maxlength' => 50, 	'#default_value' => $group->contact_name, 	'#description' => t(''), '#autocomplete_path' => 'emmaus/community/profile/autocomplete',   		);
  $form['contact_phone'] 	= array( '#type' => 'textfield',	'#title' => t('Contact Phone'),	'#required' => FALSE, '#size' => 20,  '#maxlength' => 20, 	'#default_value' => $group->contact_phone,  '#description' => t('')   		);
  $form['contact_email'] 	= array( '#type' => 'textfield',	'#title' => t('Contact Email'),	'#required' => FALSE, '#size' => 50,  '#maxlength' => 50, 	'#default_value' => $group->contact_email,  '#description' => t('')   		);
  $form['notes'] 					= array( '#type' => 'textarea',		'#title' => t('Notes'),					'#required' => FALSE, '#size' => 150, '#maxlength' => 255,	'#default_value' => $group->notes,  				'#description' => t('')   		);
	$form['is_accepting']  	= array( '#type' => 'checkbox',  	'#title' => t('Accepting?'), 		'#required' => FALSE, '#default_value' => $group->is_accepting,  '#description' => t('Is this reunion group accepting new members?'), );
	//Define the Submit buttons
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit'), );
  $form['delete'] = array( '#type' => 'submit', '#value' => t('Delete'), );
  $form['cancel'] = array( '#type' => 'submit', '#value' => t('Cancel'), );
	//Return the form
 	return $form;
}
/***************************/
/* EDIT: Validate handler  */
/***************************/
function emmaus_community_group_edit_validate($form, &$form_state) {
}
/************************/
/* EDIT: Submit handler */
/************************/
function emmaus_community_group_edit_submit($form, &$form_state) {
	//Delete button has been clicked initially.  Set a flag and rebuild the form.
	if (!isset($form_state['storage']['confirm']) && $form_state['values']['op']=='Delete' ) {
		$form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form      
		$form_state['rebuild'] = TRUE; // along with this    
	}
	//Deletion has been confirmed.  Process the queries.
	elseif (isset($form_state['storage']['confirm']) ) {		
		//Remove the group value from all board assignments
		$num_updated = db_update('emmaus_community_profile')  
			->fields(	array( 'group_name'=>NULL	))  
			->condition('group_name', $form_state['values']['group_name'] )  
			->execute();
		var_dump($num_updated);
		//drupal_set_message( t('@number Board of Directors assignments were modified.'), array('@number' => $num_updated     ), 'status'  );
		drupal_set_message( t('@number of community profiles were affected by the deletion of @name', array('@number'=>$num_updated, '@name'=>$form_state['values']['group_name']) ),  'status'  );
		//Delete the group row
		$num_deleted = db_delete('emmaus_community_group')
			->condition('group_id', $form_state['values']['group_id'])
			->execute();
		drupal_set_message( t('<em>@name</em> group was deleted', array('@name' => $form_state['values']['group_name'])), 'status'  );
	}

	//Submit button was pressed.  Write the record.
	elseif ($form_state['values']['op']=='Submit') {
		drupal_write_record('emmaus_community_group', $form_state['values'], 'group_id');
		drupal_set_message(t('@name group has been updated', array('@name' => $form_state['values']['group_name'])));
	}
	//Redirect the form for all functions, including the cancel
	$form_state['redirect'] = 'emmaus/community/group/' . $form_state['values']['group_id']   ;
}





/*********************************************************************************************/
/** BEGIN CUSTOM FUNCTIONS *******************************************************************/
/*********************************************************************************************/
/**
 * Get church name for title callback
 */
function emmaus_community_group_name($group_id) {
	//Query the database
  $result = db_select('emmaus_community_group','g')
    ->fields('g', array('group_name')) 
		->range(0,1)	 								//determine the range of records... range(offset, number of records)
		->condition('group_id' , $group_id)					//define a conditional query
    ->execute();
	//Loop through the result and create an array of rows
	foreach ($result as $row) {		
		$output = $row->group_name;
	}
	//Return the output
	if (isset($output)) {	
		return $output;
	}	
}


/** 
 * Retrieve group members
 */
function emmaus_community_group_get_members($group_name) { 
	$header = array(
		array("data" => "Name", 'field'=>'last_name', 'sort'=>'asc' ),
		array("data" => "City, State" ),
	);
	//Query the database
  $result = db_select('emmaus_community_profile', 'p')
		//->leftJoin('emmaus_community_church', 'c', 'p.church_name = c.church_name');
		->condition('p.group_name' , $group_name)					//define a conditional query
  	->fields('p', array('profile_id', 'first_name', 'last_name', 'city', 'state'))
  	->extend('PagerDefault')				//extend the query to include a pager
  	->limit(20)											//Set the number of items per page
  	->extend('TableSort')						//Extend the query to include table sorting
  	->orderByHeader($header)	//define sort column
		->execute();

	//Loop through the result and create an array of rows
	$rows = array();
	foreach ($result as $row) {		
		//create links
		$name_link = '<a href="/user/' . $row->profile_id . '">' . $row->first_name . ' ' . $row->last_name . '</a>';
		//Build the array of rows
		$rows[] = array( $name_link, $row->city . ', ' . $row->state);
	}
	//output the table
	$tbl_vars = array(  'header'=>$header,	'rows'=>$rows,	'attributes'=>array(),	'caption' => 'Current members of this group',		'colgroups'=>array(),		'sticky'=>TRUE, 	'empty'=>'No members returned.' 	);
	$output = theme_table($tbl_vars);

	//output the pager
	$output .= theme('pager');
	//Return the output
	return $output;

}



/**	
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions for existing groups.
 */
function emmaus_community_group_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $result = db_select('emmaus_community_group', 'g')
			->fields('g', array('group_id', 'group_name' , 'city'))
			->condition('group_name', db_like($string) . '%', 'LIKE')
			->range(0, 10)
			->execute();
		//Loop through the results
    foreach ($result as $group) {
      $matches[$group->group_name] = check_plain($group->group_name . ' in ' . $group->city);
    }
  }
	//return the results in a json object
  drupal_json_output($matches);
}