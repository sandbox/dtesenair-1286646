<?php
// $Id:  
 
 
/**
 * Implements hook_form_FORM_ID_alter().
 * Adds additional fields on the user registration page to collect profile information.
 * Although other modules provide similar functionality, this modules (and other related modules) call these specific fields. 
 */
function emmaus_community_form_user_register_form_alter(&$form, &$form_state, $form_id) {
	//create the options array for gender selection
	$gender_opts = array('M'=>t('Male'), 'F'=>t('Female'));
	
	
	// Form elements
  $form['account']['community_profile'] = array(
    '#type'=>'fieldset',
    '#title'=>t('Emmaus Profile Information')
    );
  $form['account']['community_profile']['first_name'] = array( 
    '#type'=>'textfield', 
    '#title'=>t('First Name'),     
    '#required'=>TRUE,
    '#size'=>25,
    '#maxlength'=>50
    );
  $form['account']['community_profile']['last_name'] = array(
    '#type'=>'textfield',
    '#title'=>t('Last Name'),
    '#required'=>TRUE,
    '#size'=>25,
    '#maxlength'=>50
    );
  $form['account']['community_profile']['nickname'] = array(
    '#type'=>'textfield',
    '#title'=>t('Nickname'),
    '#required'=>FALSE,
    '#size'=>25,
    '#maxlength'=>50   
    );
  $form['account']['community_profile']['address'] = array(
    '#type'=>'textfield',
    '#title'=>t('Address'),
    '#required'=>FALSE,
    '#size'=>50,
    '#maxlength'=>100
    );
  $form['account']['community_profile']['city'] = array(
    '#type'=>'textfield',
    '#title'=>t('City'),
    '#required'=>TRUE,
    '#size'=>25,
    '#maxlength'=>50
    );
  $form['account']['community_profile']['state'] = array(
    '#type'=>'textfield',
    '#title'=>t('State'),
    '#required'=>TRUE,
    '#size'=>5,
    '#maxlength'=>2
    );
  $form['account']['community_profile']['zip'] = array(
    '#type'=>'textfield',
    '#title'=>t('ZIP Code'),
    '#required'=>FALSE,
    '#size'=>8,
    '#maxlength'=>5,
    '#default_value'=>NULL
    );
  $form['account']['community_profile']['home_phone'] = array(
    '#type'=>'textfield',
    '#title'=>t('Home Phone'),
    '#required'=>FALSE,
    '#size'=>15,
    '#maxlength'=>12
    );
  $form['account']['community_profile']['mobile_phone']	= array(
    '#type'=>'textfield',
    '#title'=>t('Mobile Phone'),
    '#required'=>FALSE,
    '#size'=>15,
    '#maxlength'=>12
    );
  $form['account']['community_profile']['gender'] = array(
    '#type'=>'radios',
    '#title'=>t('Gender'),
    '#required'=>TRUE,
    '#options'=>$gender_opts
    );
  $form['account']['community_profile']['spouse_name'] = array(
    '#type'=>'textfield',
    '#title'=>t('Spouse Name'),
    '#required'=>FALSE,
    '#size'=>50,
    '#maxlength'=> 50,
    '#autocomplete_path'=>'emmaus/community/profile/autocomplete'
    );
  $form['account']['community_profile']['church_name'] = array(
    '#type'=>'textfield',
    '#title'=>t('Home Church'),
    '#required'=>FALSE,
    '#size'=>50,
    '#maxlength'=>50,
    '#autocomplete_path'=>'emmaus/community/church/autocomplete'
    );
  $form['account']['community_profile']['group_name'] = array(
    '#type'=>'textfield',
    '#title'=>t('Community Group'),
    '#required'=>FALSE,
    '#size'=>50,
    '#maxlength'=>50,
    '#autocomplete_path'=>'emmaus/community/group/autocomplete'
    );
  
  //check to see if we can include an autocomplete path for the original wekeend form field element
  if (module_exists('emmaus_weekend') == TRUE) {
    $form['account']['community_profile']['original_walk'] = array(
      '#type'=>'textfield',
      '#title'=>t('Original Walk'),
      '#required'=>TRUE,
      '#size'=>50,
      '#maxlength'=>50,
      '#autocomplete_path'=>'emmaus/weekend/weekends/autocomplete'
      );
  }
  else {
    $form['account']['community_profile']['original_walk'] = array(
      '#type'=>'textfield',
      '#title'=>t('Original Walk'),
      '#required'=>TRUE,
      '#size'=>50,
      '#maxlength'=>50
      );      
  }

  $form['account']['community_profile']['sponsor1'] = array(
    '#type'=>'textfield',
    '#title'=>t('Sponsor 1 Name'),
    '#required'=>FALSE,
    '#size'=>50,
    '#maxlength'=>50,
    '#autocomplete_path'=>'emmaus/community/profile/autocomplete'
    );
  $form['account']['community_profile']['sponsor2'] = array(
    '#type'=>'textfield',
    '#title'=>t('Sponsor 2 Name'),
    '#required'=>FALSE,
    '#size'=>50,
    '#maxlength'=>50,
    '#autocomplete_path'=>'emmaus/community/profile/autocomplete'
    );
  $form['account']['community_profile']['special_needs'] = array( 
    '#type'=>'textarea',  
    '#title'=>t('Special Needs'),
    '#required'=>FALSE, 
    '#rows'=>2,  
    '#description'=>t('Please describe any special physical, dietary or medical needs that may pertain to your service (i.e. - Diabetic, poor vision, etc.).  This information will not be publically viewable.')               
    );
  $form['account']['community_profile']['is_clergy'] = array( 
    '#type'=>'checkbox',  
    '#title'=>t('Clergy?'),
    '#description'=>t('Check this box if this person is ordained clergy'),      
    '#required'=>FALSE,                               
    );

	// add a new validate function to the user form to handle the profile information
	$form['#validate'][] = 'emmaus_community_user_form_validate';
	// add a new submit function to the user form to handle the profile information
	$form['#submit'][] = 'emmaus_community_user_form_submit';
}



/**
 * Implements hook_form_FORM_ID_alter().
 * Adds additional fields on the user edit page to modify profile information
 */
function emmaus_community_form_user_profile_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#user_category'] == 'community-profile') {
  	  
  	//Get the user's profile information into an array
  	$profile = user_load($form_state['user']->uid);    
    
  	//create an array for the gendder options
  	$gender_opts = array('M'=>t('Male'), 'F'=>t('Female'));
  	
  	//create the form elements
    $form['account']['community_profile'] = array(
      '#type'=>'fieldset',
      '#title'=>t('Edit Personal Information')
      );
    $form['account']['community_profile']['first_name'] = array(
      '#type'=>'textfield',
      '#title'=>t('First Name'),
      '#required'=>TRUE,
      '#size'=>25,
      '#maxlength'=>50,
      '#default_value'=>$profile->emmaus['profile']['first_name']
      );
    $form['account']['community_profile']['last_name'] = array(
      '#type'=>'textfield',
      '#title'=>t('Last Name'),
      '#required'=>TRUE,
      '#size'=>25,
      '#maxlength'=>50,
      '#default_value'=>$profile->emmaus['profile']['last_name']
      );
    $form['account']['community_profile']['nickname'] = array(
      '#type'=>'textfield', 
      '#title'=>t('Nickname'),
      '#required'=>FALSE,
      '#size'=>25,
      '#maxlength'=>50,
      '#default_value'=>$profile->emmaus['profile']['nickname']
      );
    $form['account']['community_profile']['address'] = array(
      '#type'=>'textfield',
      '#title'=>t('Address'),
      '#required'=>FALSE,
      '#size'=>50,
      '#maxlength'=>100,
      '#default_value'=>$profile->emmaus['profile']['address']
      );
    $form['account']['community_profile']['city'] = array(
      '#type'=>'textfield',
      '#title'=>t('City'),
      '#required'=>TRUE,
      '#size'=>25,
      '#maxlength'=>50,
      '#default_value'=>$profile->emmaus['profile']['city']
      );
    $form['account']['community_profile']['state'] = array(
      '#type'=>'textfield',
      '#title'=>t('State'),
      '#required'=>TRUE,
      '#size'=>5,
      '#maxlength'=>2,
      '#default_value'=>$profile->emmaus['profile']['state']
      );
    $form['account']['community_profile']['zip'] = array(
      '#type'=>'textfield',
      '#title'=>t('ZIP Code'),
      '#required'=>FALSE,
      '#size'=>8,
      '#maxlength'=>5,
      '#default_value'=>$profile->emmaus['profile']['zip']
      );
    $form['account']['community_profile']['home_phone'] = array(
      '#type'=>'textfield',
      '#title'=>t('Home Phone'),
      '#required'=>FALSE,
      '#size'=>15,
      '#maxlength'=>12,
      '#default_value'=>$profile->emmaus['profile']['home_phone']
      );
    $form['account']['community_profile']['mobile_phone'] = array(
      '#type'=>'textfield',
      '#title'=>t('Mobile Phone'),
      '#required'=>FALSE,
      '#size'=>15,
      '#maxlength'=>12,
      '#default_value'=>$profile->emmaus['profile']['mobile_phone']
      );
    $form['account']['community_profile']['gender'] = array(
      '#type'=>'radios',
      '#title'=>t('Gender'),
  	  '#required'=>TRUE,  '#options'=>$gender_opts,
  	  '#default_value'=>$profile->emmaus['profile']['gender']
      );
    $form['account']['community_profile']['spouse_name'] = array(
      '#type'=>'textfield',
      '#title'=>t('Spouse Name'),
      '#required'=>FALSE,
      '#size'=>50,
      '#maxlength'=>50,
      '#default_value'=>$profile->emmaus['profile']['spouse_name'],
      '#autocomplete_path'=>'emmaus/community/profile/autocomplete'
      );
    $form['account']['community_profile']['church_name'] = array(
      '#type'=>'textfield',
      '#title'=>t('Home Church'),
      '#required'=>FALSE,
      '#size'=>50,
      '#maxlength'=>50,
      '#default_value'=>$profile->emmaus['profile']['church_name'],
      '#autocomplete_path'=>'emmaus/community/church/autocomplete'
      );
    $form['account']['community_profile']['group_name'] = array(
      '#type'=>'textfield',
      '#title'=>t('Community Group'),
      '#required'=>FALSE,
      '#size'=>50,
      '#maxlength'=>50,
      '#default_value'=>$profile->emmaus['profile']['group_name'],
      '#autocomplete_path'=>'emmaus/community/group/autocomplete'
      );
    //check to see if we can include an autocomplete path for the original weekend form field element
    if (module_exists('emmaus_weekend') == TRUE) {
      $form['account']['community_profile']['original_walk'] = array(
        '#type'=>'textfield',
        '#title'=>t('Original Walk'),
        '#required'=>TRUE,
        '#size'=>50,
        '#maxlength'=>50,
        '#default_value'=>$profile->emmaus['profile']['original_walk'],
        '#autocomplete_path'=>'emmaus/weekend/weekends/autocomplete'
        );
    } 
    else {
      $form['account']['community_profile']['original_walk'] = array(
        '#type'=>'textfield',
        '#title'=>t('Original Walk'),
        '#required'=>TRUE,
        '#size'=>50,
        '#maxlength'=>50,
        '#default_value'=>$profile->emmaus['profile']['original_walk']
        );      
    }

    $form['account']['community_profile']['sponsor1'] = array(
      '#type'=>'textfield',
      '#title'=>t('Sponsor 1 Name'),
      '#required'=>FALSE, '#size'=>50,
      '#maxlength'=>50,
      '#default_value'=>$profile->emmaus['profile']['sponsor1'],
      '#autocomplete_path'=>'emmaus/community/profile/autocomplete'
      );
    $form['account']['community_profile']['sponsor2'] = array(
      '#type'=>'textfield',
      '#title'=>t('Sponsor 2 Name'),
      '#required'=>FALSE,
      '#size'=>50,
      '#maxlength'=>50,
      '#default_value'=>$profile->emmaus['profile']['sponsor2'],
      '#autocomplete_path'=>'emmaus/community/profile/autocomplete'
      );
    $form['account']['community_profile']['special_needs'] = array( 
      '#type'=>'textarea',  
      '#title'=>t('Special Needs'),  
      '#required'=>FALSE, 
      '#rows'=>2, 
      '#default_value'=>$profile->emmaus['profile']['special_needs'],
      '#description'=>t('Please describe any special physical, dietary or medical needs that may pertain to your service (i.e. - Diabetic, poor vision, etc.).  This information will not be publically viewable.')               
       );
    $form['account']['community_profile']['is_clergy'] = array(
      '#type'=>'checkbox',
      '#title'=>t('Clergy?'),
      '#required'=>FALSE,
      '#default_value'=>$profile->emmaus['profile']['is_clergy']
      );
  	
  	// add a new validate function to the user form to handle the profile information
  	$form['#validate'][] = 'emmaus_community_user_form_validate';
  	// add a new submit function to the user form to handle the profile information
  	$form['#submit'][] = 'emmaus_community_user_form_submit';
	}
}



/**
* Form validation handler for both the user_register_form and user_profile_form modification hooks
*/
function emmaus_community_user_form_validate($form, &$form_state) {
	//If the value is something, then alert user that it should be numeric
	if ( !($form_state['values']['zip'] == NULL) && !is_numeric($form_state['values']['zip']) ) {
		form_set_error('zip', t('ZIP Code must be a valid numeric value.'));
	}
	//Set the value to NULL if it's an empty string
	if ( $form_state['values']['zip']=='' ) {
		$form_state['values']['zip']=NULL;
	}	
}
/* 
* Form submit handler for both the user_register_form and user_profile_form modification hooks
*/
function emmaus_community_user_form_submit($form, &$form_state) {
	//Insert a new row into the church table if no church was found
	if ($form_state['values']['church_name'] == '' ) {
		$form_state['values']['church_name'] = NULL;
	} 
	else {
		db_merge('emmaus_community_church')
			->key( array('church_name' => $form_state['values']['church_name']) )
			->fields(array('church_name' => $form_state['values']['church_name'])	)
			->execute();
	}
	//Insert a new row into the group table if no group was found
	if ($form_state['values']['group_name'] == '' ) {
		$form_state['values']['group_name'] = NULL;
	} 
	else {
		db_merge('emmaus_community_group')
			->key( array('group_name' => $form_state['values']['group_name'] ) )
			->fields( array('group_name' => $form_state['values']['group_name'],	)	)
			->execute();
	}

	// Generate the merge query.  This type of query checks to see if the row exists.  If so, UPDATE.  If not, INSERT.
	db_merge('emmaus_community_profile')
		->key( array('profile_id' => $form_state['user']->uid) )  
		->fields(array(
        'profile_id' => $form_state['user']->uid,
        'uid' => $form_state['user']->uid,
  			'first_name' => $form_state['values']['first_name'],
  			'last_name' => $form_state['values']['last_name'],
  			'full_name' => $form_state['values']['first_name'] . ' ' . $form_state['values']['last_name'],
  			'nickname' => $form_state['values']['nickname'],
  			'address' => $form_state['values']['address'],
  			'city' => $form_state['values']['city'],
  			'state' => $form_state['values']['state'],
  			'zip' => $form_state['values']['zip'],
  			'home_phone' => $form_state['values']['home_phone'],
  			'mobile_phone' => $form_state['values']['mobile_phone'],
  			'gender' => $form_state['values']['gender'],
  			'spouse_name' => $form_state['values']['spouse_name'],
  			'church_name' => $form_state['values']['church_name'],
        'group_name' => $form_state['values']['group_name'],
        'original_walk' => $form_state['values']['original_walk'],
        'sponsor1' => $form_state['values']['sponsor1'],
        'sponsor2' => $form_state['values']['sponsor2'],
        'special_needs' => $form_state['values']['special_needs'],
        'is_clergy' => $form_state['values']['is_clergy'],
			)
		)
		->execute();

	//Redirect the add & edit forms back to the view page
	if ($form['#form_id'] == 'user_profile_form' ) {
		$form_state['redirect'] = 'user/' . $form_state['user']->uid;
	} 
	elseif ($form['#form_id'] == 'user_register_form' ) {
		$form_state['redirect'] = '/admin/people';
	}

}





/*************************************************************************************************************/
/*** PAGE CALLBACK FUNCTIONS *********************************************************************************/
/*************************************************************************************************************/
function emmaus_community_profile_list() { 
	//Set the tag line
  $output = '<h3><p>Current members of the ' . variable_get('emmaus_community_name') . '</p></h3>';

	//Define the table header
	$header = array(
		array('data' => ' ', 'width' => '25'),		//contact
    array('data' => 'First', 'field' => 'first_name', 'sort' => 'asc'),
		array('data' => 'Last', 'field' => 'last_name' ),
		array('data' => 'City', 'field' => 'city'),
		array('data' => 'Home Church', 'field' => 'church_name'),
		//array('data"=>"Clergy?", 'field'=>'is_clergy', 'align'=>'center'),
	);

	//Query the database
  $select = db_select('emmaus_community_profile', 'p');
  //$select->extend('PagerDefault')  //extend the query to include a pager
    //->limit(20);             //Set the number of items per page
  $select->leftJoin('users', 'u', 'p.profile_id = u.uid');
	$select->leftJoin('emmaus_community_church', 'c', 'p.church_name = c.church_name');
	$select->leftJoin('emmaus_community_group', 'g', 'p.group_name = g.group_name');
	$select->fields('p', array('profile_id','first_name','last_name','city','state','church_name','group_name','is_clergy')  )
    //->fields('u', array('uid','status')  )
    ->fields('c', array('church_id')  )
		->fields('g', array('group_id')  )
		->condition('status', '1', '=');
  $select->extend('TableSort')			//Extend the query to include table sorting
  	->orderByHeader($header);	//define sort column
	$result = $select->execute();
	
	//Loop through the result and create an array of rows
	$rows = array();
	foreach ($result as $row) {
		//create links from field results
		$contact_link = '<a href="' . base_path() . 'user/' . $row->profile_id . '/contact"><img border="0" alt="Contact" title="Contact this person" src="'. base_path() . drupal_get_path('module','emmaus_community') . '/images/envelope_12.jpg"></a>';
		$fname_link = '<a href="' . base_path() . 'user/' . $row->profile_id . '">' . $row->first_name . '</a>';
		$lname_link = '<a href="' . base_path() . 'user/' . $row->profile_id . '">' . $row->last_name . '</a>';
		$church_link = '<a href="' . base_path() . 'emmaus/community/church/' . $row->church_id . '">' . $row->church_name . '</a>';
		$group_link = '<a href="' . base_path() . 'emmaus/community/group/' . $row->group_id . '">' . $row->group_name . '</a>';
    
    //Format the city/state view
    $citystate = ($row->city && $row->state ? $row->city . ', ' . $row->state : $row->city);

		//Convert is_clergy integer to image		
		$clergy = array('data'=>'-','align'=>'center');
		if ( $row->is_clergy == 1 ) {
			$clergy = array( 'data'=>'<img border="0" alt="Clergy" src="' . base_path() . drupal_get_path('module','emmaus_community') . '/images/check.png">', 'align'=>'center' );
		}
		
		//Build the array of rows
    //$rows[] = array($contact_link, $fname_link, $lname_link, $citystate, $church_link, $group_link, $clergy);
    $rows[] = array($contact_link, $fname_link, $lname_link, $citystate, $church_link);
	}
	
	//output the table
	$tbl_vars = array(
    'header'=>$header,
	  'rows'=>$rows,
	  'attributes'=>array(),
	  'caption'=>'',
	  'colgroups'=>array(),
	  'sticky'=>TRUE,
	  'empty'=>'No rows returned from query'
    );
	$output .= theme_table($tbl_vars);

	//output the pager
	//$output .= theme('pager');
	
	//Return the output
	return $output;
}





/*********************************************************************************************/
/** HELPER FUNCTIONS *************************************************************************/
/*********************************************************************************************/


/**
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions for existing profiles.
 */
function emmaus_community_profile_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $result = db_select('emmaus_community_profile', 'p')
			->fields('p', array('profile_id', 'full_name', 'city', 'state'))
			->condition('full_name', '%' . db_like($string) . '%', 'LIKE')
			->range(0, 10)
			->execute();
		//Loop through the results
    foreach ($result as $profile) {
      $matches[$profile->full_name] = check_plain($profile->full_name . ' - ' . $profile->city . ', ' . $profile->state);
    }
  }
	//return the results in a json object
  drupal_json_output($matches);
}
