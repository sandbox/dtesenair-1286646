<?php 
// $Id:  


  
/*************************************************************************************************************/
/*** Table/List builder function(s) to display existing churches *********************************************/
/*************************************************************************************************************/

/*************************************************************************************************************/
/*** View builder function(s) to display existing church *****************************************************/
/*************************************************************************************************************/
function emmaus_community_church_view($church_id) { 
	//Query the database
  $result = db_select('emmaus_community_church','c')
    ->fields('c') 
		->range(0,1)	 								//determine the range of records... range(offset, number of records)
		->condition('church_id' , $church_id)					//define a conditional query
    ->execute();
	//Loop through the result and create an array of rows
	foreach ($result as $row) {		
		//Return the output
		if (!isset($row)) {
			$markup='Church information could not be located.  Please try again.';
		} else {
			$markup = '<p>' . $row->address . '<br/>' . $row->city . ', ' . $row->state . ' ' . $row->zip . '<br/>';
			$markup .= '<p>Pastor: ' . $row->pastor_name . '</p>';
			$markup .= '<p>Phone: ' . $row->phone . '</p>';
			$markup .= '<p>Website: <a href="http://'.$row->website.'" target="_new">'. $row->website . '</a></p>';


			//Add a table for the member list
			$members = emmaus_community_church_get_members($row->church_name);
			$markup .= $members;
		}
	}
	//return the marked-up page
	return $markup;
}






/*************************************************************************************************************/
/*** Form builder function(s) to add a new church **********************************************************/
/*************************************************************************************************************/
function emmaus_community_church_add() {
	// Define the form elements
  $form['community_church_add'] 	= array( '#type' => 'item', 			'#title' => t('Add new church.'),  );
  $form['church_name'] 	= array( '#type' => 'textfield',	'#title' => t('Church Name'),	'#required' => TRUE,  '#size' => 50, '#maxlength' => 50, 	'#description' => t('Enter a name of this church.')   		);
  $form['address'] 			= array( '#type' => 'textfield',	'#title' => t('Address'),			'#required' => FALSE, '#size' => 80, '#maxlength' => 255, '#description' => t('Enter the churches physical adress.')   			);
  $form['city'] 				= array( '#type' => 'textfield',	'#title' => t('City'),				'#required' => FALSE, '#size' => 50, '#maxlength' => 50, 	'#description' => t('')   		);
  $form['state'] 				= array( '#type' => 'textfield',	'#title' => t('State'),				'#required' => FALSE, '#size' => 6,  '#maxlength' => 2, 	'#description' => t('')   		);
  $form['zip'] 					= array( '#type' => 'textfield',	'#title' => t('ZIP Code'),		'#required' => FALSE, '#size' => 15, '#maxlength' => 10, 	'#description' => t('')   		);
  $form['phone'] 				= array( '#type' => 'textfield',	'#title' => t('Phone'),				'#required' => FALSE, '#size' => 15, '#maxlength' => 25, 	'#description' => t('')   		);
  $form['pastor_name']	= array( '#type' => 'textfield',	'#title' => t('Pastor Name'),	'#required' => FALSE, '#size' => 50, '#maxlength' => 255, '#description' => t('')   		);
  $form['pastor_email']	= array( '#type' => 'textfield',	'#title' => t('Pastor Email'),'#required' => FALSE, '#size' => 50, '#maxlength' => 255, '#description' => t('')   		);
  $form['website'] 			= array( '#type' => 'textfield',	'#title' => t('Website'),			'#required' => FALSE, '#size' => 50, '#maxlength' => 255, '#description' => t('')   		);
	//Submit button
  $form['submit'] = array( '#type' => 'submit', '#value' => 'Submit', );
	//Return the form
  return $form;
}
/****************************/
/*** ADD Validate handler ***/
/****************************/
function emmaus_community_church_add_validate($form, &$form_state) {
}
/**************************/
/*** ADD Submit handler ***/
/**************************/
function emmaus_community_church_add_submit($form, &$form_state) {
	//Generate the insert query
  db_insert('emmaus_community_church')
      ->fields(array(
						'church_name' 	=> $form_state['values']['church_name'],
						'address'	 			=> $form_state['values']['address'],
						'city' 					=> $form_state['values']['city'],
						'state'		 			=> $form_state['values']['state'],
						'zip' 					=> $form_state['values']['zip'],
						'phone' 				=> $form_state['values']['phone'],
						'pastor_name'		=> $form_state['values']['pastor_name'],
						'pastor_email'	=> $form_state['values']['pastor_email'],
						'website' 			=> $form_state['values']['website'],
					)
				)
      ->execute();
	//Set a confirmation message
  drupal_set_message(t('The church <em>@name</em> has been added to the community.', array('@name' => $form_state['values']['church_name'])));
	//Redirect the form
	$form_state['redirect'] = 'emmaus/churches';
}




/*************************************************************************************************************/
/*** EDIT: Form builder - function(s) to edit an existing board member entry *********************************/
/*************************************************************************************************************/
function emmaus_community_church_edit($form, &$form_state, $params) {
	//Check to see if we should display the confirmation form for deleting the church
	if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Delete') {
    //Do a quick query to see how many people have this church so we can display the effects on the form
    $num_members = db_select('emmaus_community_profile', 'p')
      ->fields('p', array('profile_id') )
      ->condition('p.church_name', $form_state['values']['church_name'] )
      ->execute()
      ->rowCount();    
		// Define form elements again, prior to final submission
    $form['church_markup']    = array( '#markup'=> t('<p><em>A total of @number community profiles will be affected by deleting this church.</em></p>', array('@number'=>$num_members)  ),   );
    $form['church_affected']  = array( '#type'=>'hidden', '#title'=> t('Affected Profiles'),  '#default_value' => $num_members                         );
    $form['church_id']        = array( '#type'=>'hidden', '#title'=> t('Church ID'),          '#default_value' => $form_state['values']['church_id']    );
	  $form['church_name']	    = array( '#type'=>'hidden', '#title'=> t('Church Name'), 	      '#default_value' => $form_state['values']['church_name']  );
		//Display the confirmation form
		return confirm_form($form, t('Are you sure that you want to delete <em>@name</em>?', array('@name'=>$form_state['values']['church_name'])), 'emmaus/community/church/' . $form_state['values']['church_id']   , 'This action can not be undone.  Please proceed with caution.', 'Remove', 'Don\'t Remove'); //Had better luck leaving off last param 'name'  
	}	//confirmation form
	
	//Query the database for the record to edit
  $result = db_select( 'emmaus_community_church', 'c' )
  	->fields( 'c')
		->condition( 'c.church_id',$params )
		->execute();
	//Make sure only one entry was returned.  If not, display an error
	//TODO:  Make a prettier form when an error is returned.  Perhaps add a link to the list page.
	if ( !$result->rowcount() == 1 ) {
		drupal_set_message(  t('Unable to locate the church specified in the URL (Param: @param).  Please try your query again. (Rows returned: @number)' , array('@number'=>$result->rowcount(), '@param'=>$params )) ,'error');
		return '';
	}

	//One result confirmed.  Set that row into a variable
	foreach ($result as $row) {	$church = $row; }

	// Define all the form elements
  $form['church_id']		=	array( '#type' => 'hidden', 	 	'#title' => t('ID'), 					'#default_value' => $church->church_id, );
  $form['church_name']	= array( '#type' => 'textfield',	'#title' => t('Church Name'),	'#required' => TRUE,  '#size' => 50, '#maxlength' => 50, 	'#default_value' => $church->church_name, '#description' => t('Enter a name of this church.')   		);
  $form['address'] 			= array( '#type' => 'textfield',	'#title' => t('Address'),			'#required' => FALSE, '#size' => 80, '#maxlength' => 255, '#default_value' => $church->address,		 	'#description' => t('Enter the churches physical adress.')   			);
  $form['city'] 				= array( '#type' => 'textfield',	'#title' => t('City'),				'#required' => FALSE, '#size' => 50, '#maxlength' => 50, 	'#default_value' => $church->city,  			'#description' => t('')   		);
  $form['state'] 				= array( '#type' => 'textfield',	'#title' => t('State'),				'#required' => FALSE, '#size' => 6,  '#maxlength' => 2, 	'#default_value' => $church->state, 	 		'#description' => t('')   		);
  $form['zip'] 					= array( '#type' => 'textfield',	'#title' => t('ZIP Code'),		'#required' => FALSE, '#size' => 15, '#maxlength' => 10, 	'#default_value' => $church->zip,  				'#description' => t('')   		);
  $form['phone'] 				= array( '#type' => 'textfield',	'#title' => t('Phone'),				'#required' => FALSE, '#size' => 15, '#maxlength' => 25, 	'#default_value' => $church->phone, 		 	'#description' => t('')   		);
  $form['pastor_name'] 	= array( '#type' => 'textfield',	'#title' => t('Pastor Name'),	'#required' => FALSE, '#size' => 50, '#maxlength' => 255, '#default_value' => $church->pastor_name, '#description' => t('')   		);
  $form['pastor_email']	= array( '#type' => 'textfield',	'#title' => t('Pastor Email'),'#required' => FALSE, '#size' => 50, '#maxlength' => 255, '#default_value' => $church->pastor_email,'#description' => t('')   		);
  $form['website'] 			= array( '#type' => 'textfield',	'#title' => t('Website'),			'#required' => FALSE, '#size' => 50, '#maxlength' => 255, '#default_value' => $church->website, 		'#description' => t('')   		);
	//Define the Submit buttons
  $form['submit'] = array( '#type' => 'submit', '#value' => t('Submit'), );
  $form['delete'] = array( '#type' => 'submit', '#value' => t('Delete'), );
  $form['cancel'] = array( '#type' => 'submit', '#value' => t('Cancel'), );
	//Return the form
 	return $form;
}
/***************************/
/* EDIT: Validate handler  */
/***************************/
function emmaus_community_church_edit_validate($form, &$form_state) {
}
/************************/
/* EDIT: Submit handler */
/************************/
function emmaus_community_church_edit_submit($form, &$form_state) {
	//Delete button has been clicked initially.  Set a flag and rebuild the form.
	if (!isset($form_state['storage']['confirm']) && $form_state['values']['op']=='Delete' ) {
		$form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form      
		$form_state['rebuild'] = TRUE; // along with this    
	}
	//Deletion has been confirmed.  Process the queries.
	elseif (isset($form_state['storage']['confirm']) ) {		
		//Remove the church value from all profiles
		//$num_updated = db_update('emmaus_community_profile')  
	//		->fields(	array( 'church_name'=>NULL	))  
	//		->condition('church_name', $form_state['values']['church_name'] )  
	//		->execute();
		//Set a confirmation message
		//drupal_set_message( t('@number of community profiles were affected by the deletion of @name', array('@number'=>$num_updated, '@name'=>$form_state['values']['church_name']) ),  'status'  );
		//Delete the church row
		$num_deleted = db_delete('emmaus_community_church')
			->condition('church_id', $form_state['values']['church_id'])
			->execute();
		drupal_set_message( t('@number of community profiles were affected by the deletion of @name', array('@number'=>$form_state['values']['church_affected'],'@name' => $form_state['values']['church_name']) ),  'status'  );
		drupal_set_message( t('<em>@name</em> church was deleted', array('@name' => $form_state['values']['church_name'])), 'status'  );
		//Redirect the form
		$form_state['redirect'] = 'emmaus/churches' ;
	}

	//Submit button was pressed.  Write the record.
	elseif ($form_state['values']['op']=='Submit') {
		drupal_write_record('emmaus_community_church', $form_state['values'], 'church_id');
		drupal_set_message(t('@name church has been updated', array('@name' => $form_state['values']['church_name'])));
		//Redirect the form for all functions, including the cancel
		$form_state['redirect'] = 'emmaus/churches/' . $form_state['values']['church_id'] ;
}
}
 




/*********************************************************************************************/
/** BEGIN CUSTOM HELPER FUNCTIONS ************************************************************/
/*********************************************************************************************/

/**
 * Get church name for title callback
 */
function emmaus_community_church_name($church_id) {
	//Query the database
  $result = db_select('emmaus_community_church','c')
    ->fields('c', array('church_name')) 
		->range(0,1)	 								//determine the range of records... range(offset, number of records)
		->condition('church_id' , $church_id)					//define a conditional query
    ->execute();
	//Loop through the result and create an array of rows
	foreach ($result as $row) {		
		$output = $row->church_name;
	}
	//Return the output
	if (isset($output)) {	
		return $output;
	}	
}


/** 
 * Retrieve church members
 */
function emmaus_community_church_get_members($church_name) { 
	$header = array(
		array("data" => "Name", 'field'=>'first_name', 'sort'=>'asc' ),
		array("data" => "City, State" ),
	);
	//Query the database
  $result = db_select('emmaus_community_profile', 'p')
		//->leftJoin('emmaus_community_church', 'c', 'p.church_name = c.church_name');
		->condition('p.church_name' , $church_name)					//define a conditional query
  	->fields('p', array('profile_id', 'first_name', 'last_name', 'city', 'state'))
  	->extend('PagerDefault')				//extend the query to include a pager
  	->limit(20)											//Set the number of items per page
  	->extend('TableSort')						//Extend the query to include table sorting
  	->orderByHeader($header)	//define sort column
		->execute();

	//Loop through the result and create an array of rows
	$rows = array();
	foreach ($result as $row) {		
		//create links
		$name_link = '<a href="/user/' . $row->profile_id . '">' . $row->first_name . ' ' . $row->last_name . '</a>';
		//Build the array of rows
		$rows[] = array( $name_link, $row->city . ', ' . $row->state);
	}
	//output the table
	$tbl_vars = array(  'header'=>$header,	'rows'=>$rows,	'attributes'=>array(),	'caption' => 'Current members of this church',		'colgroups'=>array(),		'sticky'=>TRUE, 	'empty'=>'No members returned.' 	);
	$output = theme_table($tbl_vars);

	//output the pager
	$output .= theme('pager');
	//Return the output
	return $output;

}

 
/**	
 * Menu callback; Retrieve a JSON object containing autocomplete suggestions for existing churches.
 */
function emmaus_community_church_autocomplete($string = '') {
  $matches = array();
  if ($string) {
    $result = db_select('emmaus_community_church', 'c')
			->fields('c', array('church_id', 'church_name' , 'city', 'state'))
			->condition('church_name', '%' . db_like($string) . '%', 'LIKE')
			->range(0, 10)
			->execute();
		//Loop through the results
    foreach ($result as $church) {
      $matches[$church->church_name] = check_plain($church->church_name . ' in ' . $church->city . ', ' . $church->state);
			//$matches[$city->cityName . '|' . cityID] = $city->cityName;
    }
  }
	//returnt the results in a json object
  drupal_json_output($matches);
}
