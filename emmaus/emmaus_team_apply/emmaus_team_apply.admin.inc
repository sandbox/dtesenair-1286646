<?php
// $Id:

/*
 * @file
 * Administrative functions for the emmaus_team module
 */


/**
 * Form builder function to configure global settings for the emmaus_team module.
 */
function emmaus_team_apply_settings_form() {
  //define form elements and varianles for team module settings
  $form['emmaus_team_max_applications'] = array(
    '#type'=>'textfield',
    '#title'=>'Maximum simultaneous applications per user',
    '#description'=>'The maximum number of simultaneous team applications that each user can have active at a time (this includes applications that are currently assigned to a team).  For unlimited, enter 0.',
    '#size'=>5,
    '#default_value' => variable_get('emmaus_team_max_applications', 1),       
  );

  //Notification CC  
  $form['emmaus_team_notify_address'] = array(
    '#type'=>'textfield',
    '#title'=>'Team Coordinator Email',
    '#description'=>'A copy of all team notification emails will be sent to this address.',
    '#size'=>50,
    '#default_value' => variable_get('emmaus_team_notify_address', variable_get('site_mail') ),       
  );
  
  //New application emails template
  $form['title'] = array(
    '#type'=>'item',
    '#title'=>'Email Templates'
  );
  $form['email'] = array(
    '#type'=>'vertical_tabs',
    '#title'=>'Email Templates'
  );
  
  $form['email']['new-application'] = array(
    '#type'=>'fieldset',
    '#title'=>'New Application Entered'
  );
  $form['email']['new-application']['descrioption'] = array(
    '#markup'=>'This email template will be set when a new application is entered into the system.'
  );
  $form['email']['new-application']['subject'] = array(
    '#type'=>'textfield',
    '#title'=>'Subject'
  );
  $form['email']['new-application']['body'] = array(
    '#type'=>'textarea',
    '#title'=>'Body'
  );
  
  $form['email']['mod-application'] = array(
    '#type'=>'fieldset',
    '#title'=>'Existing Application Modified'
  );
  $form['email']['mod-application']['subject'] = array(
    '#type'=>'textfield',
    '#title'=>'Subject'
  );
  $form['email']['mod-application']['body'] = array(
    '#type'=>'textarea',
    '#title'=>'Body'
  );
  
  $form['email']['leadership-assigned'] = array(
    '#type'=>'fieldset',
    '#title'=>'Leadership Assignment Made'
  );
  $form['email']['leadership-assigned']['subject'] = array(
    '#type'=>'textfield',
    '#title'=>'Subject'
  );
  $form['email']['leadership-assigned']['body'] = array(
    '#type'=>'textarea',
    '#title'=>'Body'
  );
  // Return the form
  return system_settings_form($form);
}
/***************************/
/* Validate handler  *******/
/***************************/
function emmaus_team_apply_settings_form_validate($form, &$form_state) {
  //make sure the max applications entry is numeric
  if (is_numeric($form_state['values']['emmaus_team_max_applications'])==FALSE ) {
    form_set_error('emmaus_team_max_applications', 'Please enter a numeric value for maximum applications.');
  }
}
/***************************/
/* Submit handler  *********/
/***************************/
function emmaus_team_apply_settings_form_submit($form, $form_state) {
  //TODO:  Find out if this function call is PRE or POST form validation.  I think it's POST.
}


