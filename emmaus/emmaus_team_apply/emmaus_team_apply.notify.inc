<?php

function _emmaus_team_email_new_application($app) {
  //debugging
  //drupal_set_message( '<pre>' . print_r($app, TRUE) . '</pre>'   );   
  
  //load the applicants profile
  $profile = user_load( $app['profile_id'] );

  //construct the message header
  $msg['header'] = array(
    'Reply-To'                  => variable_get('emmaus_team_notify_address'),
    'MIME-Version'              => '1.0',
    'Content-Type'              => 'text/html; charset=UTF-8; format=flowed; delsp=yes',
    'Content-Transfer-Encoding' => '8Bit',
    'X-Mailer'                  => 'Drupal'
    );

  //construct a list of recipients
  $msg['recipients'] = $profile->mail . ',' . variable_get('emmaus_team_notify_address')   ;

  //construct an unordered list of teams worked
  $teams = '<ul>';
  foreach ($profile->emmaus['team']['history']['weekends']['local'] as $team) {
    $teams .= '<li>' . $team['weekend_name'] . '</li>';
  }
  $teams .= '</ul>';
  //construct an unordered list of positions held
  $positions = '<ul>';
  foreach ($profile->emmaus['team']['history']['positions'] as $position) {
    $positions .= '<li>' . $position . '</li>';
  }
  $positions .= '</ul>';
  //construct an unordered list of talks given
  $talks = '<ul>';
  foreach ($profile->emmaus['team']['history']['talks']['primary'] as $talk) {
    $talks .= '<li>' . $talk . '</li>';
  }
  $talks .= '</ul>';

  //construct the subject based on the 'action' form element
  switch ($app['action']) {
    case 'new':
      $msg['subject'] = 'NEW Team Application Submitted for '. $profile->emmaus['profile']['full_name'] . ' on ' . format_date($app['application_date'], 'custom', 'M j, Y');
      break;
    case 'admin':
      $msg['subject'] = 'NEW Team Application Submitted for '. $profile->emmaus['profile']['full_name'] . ' on ' . format_date($app['application_date'], 'custom', 'M j, Y') . ' by ' . $app['submitted_by'];
      break;
    case 'edit':
      $msg['subject'] = 'MODIFIED Team Application Submitted for '. $profile->emmaus['profile']['full_name'] . ' on ' . format_date($app['application_date'], 'custom', 'M j, Y');
      break;
  }

  //construct the body of the message
  $body = '<h2>Team Application Submitted for '. $profile->emmaus['profile']['full_name'] . ' on ' . format_date($app['application_date'], 'custom', 'M j, Y') . '</h2>'
  //Profile Information
  . '<br/><h3>Profile Information</h3>'
  . '<ul>'
  . '<li><b>Name:</b> ' . $profile->emmaus['profile']['full_name'] . '</li>'
  . '<li><b>Address:</b> ' . $profile->emmaus['profile']['address'] . ', ' .$profile->emmaus['profile']['city'] . ', ' . $profile->emmaus['profile']['state']. ' ' . $profile->emmaus['profile']['zip'] . '</td></tr>'
  . '<li><b>Home Phone:</b> ' . $profile->emmaus['profile']['home_phone'] . '</td></tr>'
  . '<li><b>Mobile Phone:</b> ' . $profile->emmaus['profile']['mobile_phone'] . '</td></tr>'
  . '<li><b>Email:</b> ' . $profile->mail . '</td></tr>'
  . '<li><b>Original Walk:</b> ' . $profile->emmaus['profile']['original_walk'] . '</td></tr>'
  . '<li><b>Home Church:</b> ' . $profile->emmaus['profile']['church_name'] . '</td></tr>'
  . '<li><b>Special Needs:</b> ' . $profile->emmaus['profile']['special_needs'] . '</td></tr>'
  . '</ul>'
  //Application Preferences
  . '<br/><h3>Application Preferences</h3>'
  . '<ul>'
  . '<li><b>Weekend Choice #1:</b> ' . $app['weekend_pref1'] . '</li>'
  . '<li><b>Weekend Choice #2:</b> ' . $app['weekend_pref2'] . '</li>'
  . '<li><b>Weekend Choice #3:</b> ' . $app['weekend_pref3'] . '</li>'
  . '<li><b>Position Preference:</b> ' . $app['position_pref'] . '</li>'
  . '<li><b>Notes:</b> ' . $app['notes'] . '</li>'
  . '</ul>'
  //Team History
  . '<br/><h3>Team History</h3>'
  . '<ul>'
  . '<li><b>Weekends Worked</b></li>'
  . $teams
  . '<li><b>Positions Held</b></li>'
  . $positions
  . '<li><b>Talks Given</b></li>'
  . $talks
  . '</ul>'
  ;
  $msg['body'] = array($body);
  //$msg['body'] = $body;
  
  //**Debugging messages
  //drupal_set_message( '<pre>' . print_r($msg, TRUE) . '</pre>'   );   
    
    
  return $msg;   
}
