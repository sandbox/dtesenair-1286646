<?php
//$Id$ 




/*************************************************************************************************************/
/*** Table/List builder function(s) to display weekend summary report ****************************************/
/*************************************************************************************************************/
function emmaus_team_apply_summary() {
	$output = ''; //'<p>This is where a table will show all of the available and assigned applications.</p>';
	//$output .= '<p>TODO:  Change these tag lines to pull from a variable.  Maybe?</p>';
	
	//Define the table header
	$header = array( 'Number', 'Name', 'Date', '1st Choice', '2nd Choice', '3rd Choice', 'Assigned');

	//Get the weekends
	$weekends = _emmaus_weekend_weekends(NULL, 1);

	$rows = array();
	foreach ( $weekends as $key=>$value) {
    //drupal_set_message($key,'status');
    		
		//load the weekend into an array
		$weekend = _emmaus_weekend_load($key);
		//Get the number of applications for each choice and assignment
		$first_choice 	= db_query('SELECT COUNT(*) FROM {emmaus_team_applications} a WHERE a.assigned_weekend IS NULL AND a.weekend_pref1 = :weekend_num', array(':weekend_num' => $weekend['weekend_number']))->fetchField();
    //drupal_set_message('First: ' . $first_choice,'status');
    
		$second_choice 	= db_query('SELECT COUNT(*) FROM {emmaus_team_applications} a WHERE a.assigned_weekend IS NULL AND a.weekend_pref2 = :weekend_num', array(':weekend_num' => $weekend['weekend_number'])) ->fetchField();
    //drupal_set_message('Second: ' . $second_choice,'status');
    
		$third_choice = db_query('SELECT COUNT(*) FROM {emmaus_team_applications} a WHERE a.assigned_weekend IS NULL AND a.weekend_pref3 = :weekend_num', array(':weekend_num' => $weekend['weekend_number'])) ->fetchField();
    //drupal_set_message('Third: ' . $third_choice,'status');

		$assigned 			= db_query('SELECT COUNT(*) FROM {emmaus_team_applications} a WHERE a.assigned_weekend = :weekend_num', array(':weekend_num' => $key)) ->fetchField();
    //drupal_set_message('Assigned: ' . $assigned,'status');
    //drupal_set_message('-----------------------------','status');
    
    
    
		//output the table rows
		$rows[] = array( $weekend['weekend_number'], $weekend['weekend_name'], format_date( $weekend['start_date'], 'custom', 'M j, Y' ), $first_choice, $second_choice, $third_choice, $assigned );
	}
	//output the table
	$tbl_vars = array(  'header'=>$header,	'rows'=>$rows,	'attributes'=>array(),	'caption' => '<h3>Application Summary Report</h3>',		'colgroups'=>array(),		'sticky'=>TRUE, 	'empty'=>'No weekends scheduled' 	);
	$output .= theme_table($tbl_vars);

	return $output;
}


/*************************************************************************************************************/
/*** Table/List builder function(s) to display unassigned applications ***************************************/
/*************************************************************************************************************/
function emmaus_team_applications_available() {
	$output = '';
	$gender_arr = array('M','F');
	foreach ($gender_arr as $gender) {
		//Define the table header
		$header = array(
			array('data'=>'', 'width'=>'30'	 ),		//edit
			array('data'=>'', 'width'=>'30'	 ),		//delete
      array('data'=>'Date',         'field'=>'application_date',  'sort'=>'asc'   ),
      array('data'=>'Name',         'field'=>'full_name',                         ),
			array('data'=>'1st Choice',		'field'=>'weekend_pref1'											),
			array('data'=>'2nd Choice',		'field'=>'weekend_pref2'											),
			array('data'=>'3rd Choice',		'field'=>'weekend_pref3'											),
			array('data'=>'Teams'																										  	),
			array('data'=>'Talks'																											  ),
      array('data'=>'Last Team'                                                   ),    
      array('data'=>'Assigned'                                                    ),    
		);	
		//Set the table caption
		if ($gender == 'M') { $caption = 'Applications for Men\'s Weekends'; }
		if ($gender == 'F') { $caption = 'Applications for Women\'s Weekends'; }
	
		//Query the database for applications
		//OR clause
		$or = db_or();
    $or->condition('a.assigned_weekend', NULL );
    $or->condition('w.active','1');

		//QUERY
		$select = db_select('emmaus_team_applications', 'a'  );
		$select ->leftJoin( 'emmaus_community_profile', 'p', 'a.profile_id = p.profile_id');
    $select ->leftJoin('emmaus_weekend_weekends', 'w', 'a.assigned_weekend = w.weekend_number'  );
		$select ->condition('a.active' , '1' )		//define a conditional query
			      ->condition('p.gender' , $gender )		//define a conditional query
            ->condition($or)
						->fields('a')
			      ->fields('p', array('full_name'));
		$applications = $select->execute();	
	
		//Loop through the result and create an array of rows
		$rows = array();
		foreach ($applications as $app) {
      //format the date
      $date = date( 'm/d/y', $app->application_date );
      //for each application, load the user object to get their history
      $account = user_load($app->profile_id);
       //drupal_set_message('<pre>' . print_r($account, TRUE) . '</pre>');
      $num_teams = $account->emmaus['team']['history']['weekends']['count'] ;
      $num_talks = count( $account->emmaus['team']['history']['talks']['primary'] );
      $last_weekend = end( $account->emmaus['team']['history']['weekends']['local'] );

      //var_dump($account);
	  //drupal_set_message('<pre>' . print_r($account, true) . '</pre>');

	  //create links
			$assign_link = '<a href="/admin/emmaus/team/applications/' . $app->application_id . '/assign"><img border="0" alt="Edit" title="Edit/Assign this application" src="/'. drupal_get_path('module','emmaus_community') . '/images/assign.gif"></a>';
      $edit_link = '<a href="/user/' . $account->uid .  '/edit/applications/' . $app->application_id . '"><img border="0" alt="Del" title="Delete this application" src="/'. drupal_get_path('module','emmaus_community') . '/images/edit.png"></a>';
			$delete_link = '<a href="/emmaus/team/applications/' . $app->application_id . '/delete"><img border="0" alt="Del" title="Delete this application" src="/'. drupal_get_path('module','emmaus_community') . '/images/drop.png"></a>';
 	    $profile_link = '<a href="/user/' . $app->profile_id . '">' . $account->emmaus['profile']['full_name'] . '</a>';
 	    
			//build the view link
			if ( empty($app->weekend_pref1)==FALSE  ) {  $weekend_pref1 = '<a href="/emmaus/weekend/' . $app->weekend_pref1 . '/view">' . $app->weekend_pref1 . '</a>';  } else { $weekend_pref1=''; }
			if ( empty($app->weekend_pref2)==FALSE  ) {  $weekend_pref2 = '<a href="/emmaus/weekend/' . $app->weekend_pref2 . '/view">' . $app->weekend_pref2 . '</a>';  } else { $weekend_pref2=''; }
			if ( empty($app->weekend_pref3)==FALSE  ) {  $weekend_pref3 = '<a href="/emmaus/weekend/' . $app->weekend_pref3 . '/view">' . $app->weekend_pref3 . '</a>';  } else { $weekend_pref3=''; }
		
			//output the table rows
			$rows[] = array( $assign_link, $edit_link, $date, $profile_link, $weekend_pref1, $weekend_pref2, $weekend_pref3, $num_teams, $num_talks, $last_weekend['weekend_number'], $app->assigned_weekend );
		}
		//output the table
		$tbl_vars = array(  'header'=>$header,	'rows'=>$rows,	'attributes'=>array(),	'caption' => '<h3>'.$caption.'</h3>',		'colgroups'=>array(),		'sticky'=>TRUE, 	'empty'=>'No available applications' 	);
		$output .= theme_table($tbl_vars);
	}
	//return the themed table
	return $output;
}



/*************************************************************************************************************/
/*** Form builder function(s) to assign an application to a team *********************************************/
/*************************************************************************************************************/
function emmaus_team_application_assign_form($form, &$form_state, $application) {
  //load user profile
  $profile = user_load($application['profile_id']);

	//set the page title
	drupal_set_title( t('Assign application for @name', array('@name'=>$profile->emmaus['profile']['full_name'] ) ) );
  
  //create arrays for weekends, talks & positions for the form elements
  $weekends = _emmaus_weekend_weekends( $profile->emmaus['profile']['gender'], 1 );
  $talks = array_merge( array(NULL=>'- None -') , _emmaus_team_talks('L') );
  $positions = array_merge( array(NULL=>'- None -') , _emmaus_team_positions('L'));  
  
	//**build the form in separate fieldsets for the app, history and assignment
	$form['app']						= array( '#type'=>'fieldset',	'#title'=>'Application Details'	);
  $form['app']['picture'] = array(
    '#markup' => theme('user_picture', array('account'=>$profile)),
    '#weight' => -10,
  );
  $form['app']['application_date']  = array( '#type'=>'markup', '#markup'=>'<p><b>Application Date:</b> ' . format_date($application['application_date'],'custom', 'M j,Y') . '</p>'  );
	$form['app']['weekend_pref1']	= array( '#type'=>'markup',	'#markup'=>'<b>Weekend Preference #1:</b> <a href="/emmaus/weekend/' . $application['weekend_pref1'] . '/team">'. $application['weekend_pref1'] . '</a><br/>'	);
	$form['app']['weekend_pref2']	= array( '#type'=>'markup',	'#markup'=>'<b>Weekend Preference #2:</b> <a href="/emmaus/weekend/' . $application['weekend_pref2'] . '/team">'. $application['weekend_pref2'] . '</a><br/>'	);
	$form['app']['weekend_pref3']	= array( '#type'=>'markup',	'#markup'=>'<b>Weekend Preference #3:</b> <a href="/emmaus/weekend/' . $application['weekend_pref3'] . '/team">'. $application['weekend_pref3'] . '</a><br/>'	);
	$form['app']['position_pref']	= array( '#type'=>'markup',	'#markup'=>'<p><b>Position Preference:</b> '. $application['position_pref'] . '</p>'	);
  $form['app']['special_needs'] = array( '#markup'=>'<p><b>Special Needs:</b> ' . $profile->emmaus['profile']['special_needs'] . '</p>' );
  $form['app']['notes']         = array( '#markup'=>'<p><b>Notes:</b> ' . $application['notes'] . '</p>'  );

  //**history section
  $last = end($profile->emmaus['team']['history']['weekends']['local']);
  $positions_held = '<ul><li>' . implode( $profile->emmaus['team']['history']['positions'], '</li><li>' ) . '</li></ul>'; 
  $talks_given = '<ul><li>' . implode( $profile->emmaus['team']['history']['talks']['primary'], '</li><li>' ) . '</li></ul>'; 
  //build the header aray
  $header = array( 'Positions Held', 'Talks Given', 'Last Weekend');  
  //output the table rows
  $rows = array();
  $rows[] = array( $positions_held, $talks_given,  $last['weekend_name'] );
  //render the table
  $tbl_vars = array(  'header'=>$header,  'rows'=>$rows,  'attributes'=>array(),  'caption'=>'',   'colgroups'=>array(),   'sticky'=>TRUE,   'empty'=>'No rows returned from query'  );
  $history = theme_table($tbl_vars);
  //output the form elements
  $form['history'] = array( '#type'=>'fieldset', '#title'=>'Team Experience'  );
  $form['history']['table'] = array('#markup'=>$history );    
  
  //**assignment section
  //$form['assign']                       = array( '#type'=>'fieldset', '#title'=>'Weekend Assignment'  );
	$form['assign']['open_table']				= array( '#markup'=>'<table><tr>'  );
  $form['assign']['application_id']   = array( '#type'=>'hidden',   '#default_value'=>$application['application_id']  );
  $form['assign']['profile_id']       = array( '#type'=>'hidden',   '#default_value'=>$application['profile_id']      );
  $form['assign']['assigned_weekend'] = array( '#type'=>'select',   '#title'=>'Assign to Weekend', '#required'=>TRUE, '#default_value'=>$application['assigned_weekend'],  '#options'=>$weekends,  '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );

  $form['assign']['position_code']	= array( '#type'=>'select',	  '#title'=>'Assign Position',	'#required'=>FALSE, '#options'=>$positions, '#prefix'=>'<td valign="top">',	'#suffix'=>'</td>'	);
  $form['assign']['p_talk_code']    = array( '#type'=>'select',   '#title'=>'Primary Talk',     '#options'=>$talks,     '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );
  $form['assign']['b_talk_code']    = array( '#type'=>'select',   '#title'=>'Backup Talk',      '#options'=>$talks,     '#prefix'=>'<td valign="top">',  '#suffix'=>'</td>'  );
	$form['assign']['close_table']	  = array( '#markup'=>'</tr></table>'  );

	//Submit button
  $form['assign']['submit'] = array( '#type' => 'submit', '#value' => 'Submit', );
	//Return the form
  return $form;

}
/****************************/
/*** Validate handler *******/
/****************************/
function emmaus_team_application_assign_form_validate($form, &$form_state) {
}
/**************************/
/*** Submit handler *******/
/**************************/
function emmaus_team_application_assign_form_submit($form, &$form_state) {
  //set any empty string values to NULL to maintain database integrity
  foreach($form_state['values'] as $value) {
    if(empty($value)) {$value=NULL;}
  }

  //Update the application record
  drupal_write_record('emmaus_team_applications', $form_state['values'], 'application_id');


  //retrieve the list of talks and positions so we can easily extract the names based on the form selections
  //@TODO: Replace these with some routine to extract the values from the $form elements similar to 'emmaus_team_user_history_form_validate'.  This will reduce the number of datbase queries in this section 
  $_positions = array_merge(_emmaus_team_positions('C'), _emmaus_team_positions('L') );
  $_talks = array_merge( _emmaus_team_talks('C'),  _emmaus_team_talks('L')) ;

  //validate field values
  if ( $form_state['values']['position_code'] == '' ) {
     $form_state['values']['position_code'] = NULL; 
     $form_state['values']['position_name'] = NULL; 
  } else {
     $form_state['values']['position_name'] = $_positions[$form_state['values']['position_code']]; 
  }
  if ( $form_state['values']['p_talk_code'] == '' ) {
    $form_state['values']['p_talk_code'] = NULL; 
    $form_state['values']['p_talk_name'] = NULL; 
  } else {
     $form_state['values']['p_talk_name'] = $_talks[$form_state['values']['p_talk_code']]; 
  }
  if ( $form_state['values']['b_talk_code'] == '' ) {
    $form_state['values']['b_talk_code'] = NULL; 
    $form_state['values']['b_talk_name'] = NULL; 
  } else {
     $form_state['values']['b_talk_name'] = $_talks[$form_state['values']['b_talk_code']]; 
  }


  
  //Update the weekend team assignment
/*  db_update('emmaus_team_roster')
  ->condition('roster_id', $form_state['values']['roster_id'] )
  ->fields( array(
    'position_code' => $form_state['values']['position_code'],
    'position_name' => $form_state['values']['position_name'],
    'p_talk_code'  => $form_state['values']['p_talk_code'],
    'p_talk_name'  => $form_state['values']['p_talk_name'],
    'b_talk_code'  => $form_state['values']['b_talk_code'],
    'b_talk_name'  => $form_state['values']['b_talk_name'],
    ))
  ->execute();
  */
   //drupal_write_record('emmaus_team_roster', $form_state['values']);
  
  $result = db_insert('emmaus_team_roster')
  ->fields( array(
    'weekend_number' => $form_state['values']['assigned_weekend'],
    'profile_id'  => $form_state['values']['profile_id'],
    'type' => 'L',
    'position_code' => $form_state['values']['position_code'],
    'position_name' => $form_state['values']['position_name'],
    'p_talk_code'  => $form_state['values']['p_talk_code'],
    'p_talk_name'  => $form_state['values']['p_talk_name'],
    'b_talk_code'  => $form_state['values']['b_talk_code'],
    'b_talk_name'  => $form_state['values']['b_talk_name'],
    ))
    ->execute();
 
  
  //drupal_set_message('<pre>' . print_r($form_state, TRUE) . '</pre>');
 
 
  
  //Set a confirmation message
  drupal_set_message(t('Application has been assigned to @weekend', array('@weekend'=>$form_state['values']['assigned_weekend']  )   )   );
  //Redirect the form
  $form_state['redirect'] = 'admin/emmaus/team/applications/available'  ;
}



/*************************************************************************************************************/
/*** Form builder function(s) to apply for a team ************************************************************/
/*************************************************************************************************************/
function emmaus_team_application_form($form, &$form_state, $application=NULL) {
  //Attempt to get current user id and retrieve their gender
  global $user;

  //Check to see if we should display the confirmation form after the delete button has been pressed
  if (isset($form_state['storage']['confirm']) && $form_state['values']['op'] == 'Delete') {
    //Load the profile of the user
    $account = user_load( $form_state['values']['profile_id'] );    
    // Define form elements again, prior to final submission
    $form['application_id'] = array( '#type' => 'hidden', '#default_value' => $form_state['values']['application_id']  );
    $form['profile_id']     = array( '#type' => 'hidden', '#default_value' => $form_state['values']['profile_id']      );
    //Display the confirmation form
    return confirm_form( 
      $form, 
      t('Are you sure that you want to delete application @number for @name?', array( '@name'=>$account->emmaus['profile']['full_name'], '@number'=>$form_state['values']['application_id'] )  ), 
      'user/' .$form_state['values']['profile_id'] . '/edit/team', 
      'This action can not be undone.  Please proceed with caution.', 
      'Remove', 
      'Don\'t Remove'
    );  
  } // end of delete confirmation form

  //Set up the form based on wether or we are submitting a new application (self or admin) or editing an existing one
  switch ($application) {
    case NULL:  //submitting a new application for self
      drupal_set_title(t('Submit Weekend Application'));
      //make sure the user has a valid profile.  If so, set the gender variable
      $profile = user_load($user->uid);
      if ( empty($profile->emmaus['profile']) ) {
        drupal_set_message( t('Your community profile could not be located.  Therefore, you can not apply to work a team at this time.  If you feel that you have received this message in error, please contact the site administrator for further assistance.'), 'warning');
        return;
      } 
      //make sure we aren't exceeding the max number of applications
      if ( variable_get('emmaus_team_max_applications')!=0 && count($profile->emmaus['team']['applications']['open']) >= variable_get('emmaus_team_max_applications')  ) {
        drupal_set_message( t('You have submitted the maximum number of team application permitted by your community.  Therefore, you can not apply to work a team at this time.  If you feel that you have received this message in error, please contact the site administrator for further assistance.'), 'warning');
        return '';         
      }      
      //add a hidden form element with the action of 'new'
      $form['action'] = array( '#type'=>'hidden', '#default_value'=>'new');
      //set default values for the form elements
      $default_values = array ( 'application_id'=>'', 'application_date'=>time(), 'profile_id'=>'', 'full_name'=>'', 'position_pref'=>'', 'dietary_needs'=>'', 'physical_needs'=>'', 'medication_needs'=>'', 'reason'=>'', 'notes'=>'', 'weekend_pref1'=>'',  'weekend_pref2'=>'', 'weekend_pref3'=>'' );    
      $default_values['profile_id'] = $profile->emmaus['profile']['profile_id'];
      break;
    case 'admin':   //submitting a new application for others
      //drupal_set_title(t(''));
      $form['caption'] = array( '#markup'=>'<h3>Submit an application on behalf of a community member.</h3>');
      $form['action'] = array( '#type'=>'hidden', '#default_value'=>'admin');
      //drupal_set_message('The $param variable is ' . $params , 'error');
      $default_values = array ( 'application_id'=>'', 'application_date'=>time(), 'profile_id'=>'', 'full_name'=>'', 'position_pref'=>'', 'dietary_needs'=>'', 'physical_needs'=>'', 'medication_needs'=>'', 'reason'=>'', 'notes'=>'', 'weekend_pref1'=>'',  'weekend_pref2'=>'', 'weekend_pref3'=>'' );    
      break;
    case is_array($application): //editing an existing application
      drupal_set_title( t('Edit Team Application') );
      //Load the user's profile
      $profile = user_load($application['profile_id']);
      //Query the database for the application to edit
      $default_values =$application;
      //add a form element with the action
      $form['action'] = array( '#type'=>'hidden', '#default_value'=>'edit');
      //add a delete button to the bottom of the form
      $form['delete'] = array( '#type'=>'submit', '#value'=>'Delete', '#weight'=>90,  
         //disable the delete button if the application has already been assigned
        '#disabled' => !empty($default_values['assigned_weekend'])  
      );
      break;
    default:
      drupal_set_message('Could not determine the appropriate action to perform.  Exiting team application.', 'error');
      return;
      break;
  }
  
  //Build an array of profiles to select from
  //TODO: Change this to use a helper function in the community module
  $result = db_select( 'emmaus_community_profile', 'p' )
    ->fields('p', array('profile_id', 'first_name', 'last_name', 'city' ) )
    ->orderBy('first_name', 'ASC')
    ->orderBy('last_name', 'ASC')
    ->execute();
  $profiles = array(NULL=>'- Select -');
  $num_profiles = $result->rowCount();
  //Make sure a profile was returned from the query
  if ( $result->rowCount() < 1) {
    drupal_set_message( t('There are no available community user profiles to apply for.  If you feel that you have received this message in error, please contact the site administrator for further assistance.'), 'warning');
    return $form;
  }
  foreach ($result as $row) {
    $profiles[$row->profile_id] =  $row->first_name .' '. $row->last_name . ' (' . $row->city . ')'  ;
  }

  //Build an array of available weekends to select from
  //TODO: Change this to use a helper function in the weekend module
  $result = db_select( 'emmaus_weekend_weekends', 'w' )
    ->fields('w', array('weekend_number', 'weekend_name'))
    ->condition( 'w.start_date', time(), '>' );
    //add an additional condition if not in 'admin' mode
    if ( $application != 'admin') {$result->condition( 'w.gender', $profile->emmaus['profile']['gender'] );  }
    $result = $result->execute();
  //Make sure a weekend was returned
  $num_weekends = $result->rowCount();
  if ( $num_weekends < 1) {
    drupal_set_message( t('We apologize but there are no open/active weekends available to apply for.  If you feel that you have received this message in error, please contact the site administrator for further assistance.'), 'warning');
    //return;
  }
  //Loop through the returned weekends and create an options array
  $weekends = array(NULL=>'- Select -');
  foreach ($result as $row) {
    $weekends[$row->weekend_number] = t($row->weekend_name);
  }

  //Build an array of position preferences to pass as the #options value  
  $positions = array(
    'Any'=>'Any', 
    'Inside'=>'Inside (Conference Room, Music, etc.)', 
    'Outside'=>'Outside (Kitchen, Agape, Chapel, etc.)'
    );
    
  // Define the form elements
  $form['application_id'] = array( 
    '#type'=>'hidden',     
    '#default_value'=>$default_values['application_id']       
    );
  $form['application_date'] = array( 
    '#type'=>'hidden',     
    '#default_value'=>$default_values['application_date']       
    );
  $form['submitted_by'] = array( 
    '#type'=>'hidden', 
    '#default_value'=>$user->name,
    );
  $form['profile_id'] = array( 
    '#type'=>'select',  
    '#title'=>t('Applicant Name'), 
    '#options'=>$profiles, 
    '#default_value'=>$default_values['profile_id'],
    '#disabled' => !($application == 'admin' ),      //enable the select box if this is an 'admin' submission
    );
  $form['position_pref'] = array( 
    '#type'=>'radios',     
    '#title'=>t('Position Preference'),   
    '#options'=>$positions,  
    '#required'=>TRUE,  
    '#default_value'=>$default_values['position_pref']   
    );
  $form['notes'] = array( 
    '#type'=>'textarea',   
    '#title'=>t('Please provide any additional information pertinent to this application'), 
    '#default_value'=>$default_values['notes'], 
    '#rows'=>2, 
    '#resizable'=>FALSE,    
    );
  //Weekend selection
  $form['weekends']  = array( 
    '#type'=>'fieldset',  
    '#title'=>t('Weekend Selection'),   
    );
  if ( $num_weekends >= 1 ) {
    $form['weekends']['weekend_pref1']  = array( 
      '#type'=>'select',     
      '#title'=>t('Weekend First Choice'),  
      '#default_value'=>$default_values['weekend_pref1'], 
      '#options'=>$weekends,
      //disable the weekend selection box if the application has already been assigned
      //'#disabled' => !empty($default_values['assigned_weekend'])  
      );
  }
  if ( $num_weekends >= 2 ) {
    $form['weekends']['weekend_pref2'] = array( 
      '#type'=>'select',     
      '#title'=>t('Weekend Second Choice'), 
      '#default_value'=>$default_values['weekend_pref2'], 
      '#options'=>$weekends,  
      //disable the weekend selection box if the application has already been assigned
      //'#disabled' => !empty($default_values['assigned_weekend'])  
      );
  }
  if ( $num_weekends >= 3 ) {
    $form['weekends']['weekend_pref3'] = array( 
      '#type'=>'select',     
      '#title'=>t('Weekend Third Choice'),  
      '#default_value'=>$default_values['weekend_pref3'], 
      '#options'=>$weekends,  
      //disable the weekend selection box if the application has already been assigned
      //'#disabled' => !empty($default_values['assigned_weekend'])  
      );
  }
  //Submit button
  $form['submit'] = array( 
    '#type' => 'submit', 
    '#value' => 'Submit', 
    );
  //Return the form
  return $form;
}
/****************************/
/*** ADD Validate handler ***/
/****************************/
function emmaus_team_application_form_validate($form, &$form_state) {
  //Skip validation if we are confirming deletion
  if (isset($form_state['storage']['confirm']) ) {    return;    }
  
  //Otherwise, check weekend_pref1 and set error if not selected
  if ($form_state['values']['weekend_pref1'] == NULL) {
    form_set_error('weekend_pref1', 'You must select at least one weekend to apply to.');
  }
  //check the weekend prefernces and make sure values and set to null if empty
  if ( empty($form_state['values']['weekend_pref2'])  ) {
    $form_state['values']['weekend_pref2'] = NULL;
  }
  if ( empty($form_state['values']['weekend_pref3'])  ) {
    $form_state['values']['weekend_pref3'] = NULL;
  }
  //set the special needs fields to NULL if the special_check checkbox was 0
  /*
  if ( $form_state['values']['special_check']==0 ) {
    $form_state['values']['dietary_needs'] = NULL;
    $form_state['values']['physical_needs'] = NULL;
    $form_state['values']['medication_needs'] = NULL;
  } 
  */
}
/**************************/
/*** Submit handler *******/
/**************************/
function emmaus_team_application_form_submit($form, &$form_state) {
  global $user;

  //Delete button has been clicked initially.  Set a flag and rebuild the form.
  if (!isset($form_state['storage']['confirm']) && $form_state['values']['op']=='Delete' ) {
    $form_state['storage']['confirm'] = TRUE; // this will cause the form to be rebuilt, entering the confirm part of the form      
    $form_state['rebuild'] = TRUE; // along with this    
  }
  //Deletion has been confirmed.  Process the queries.
  elseif (isset($form_state['storage']['confirm']) ) {    
    //Delete the history row
    $num_deleted = db_delete('emmaus_team_applications')
      ->condition('application_id', $form_state['values']['application_id'])
      ->execute();
    drupal_set_message( t('Application was deleted.'), 'status'  );
    //Redirect the form
    $form_state['redirect'] = 'user/' .$form_state['values']['profile_id'] .'/edit/applications' ;      
  }

  //Submit button was pressed.  Write the record.
  elseif ($form_state['values']['op']=='Submit') {
    //make sure any form values that are empty strings are set to NULL for database consistency, especially those that are foreign keys
    foreach ($form_state['values'] as $value) {
      if (empty($value)) { $value=NULL;}
    }
    //if ( empty( $form_state['values']['weekend_pref2'] ) )  { $form_state['values']['weekend_pref2'] = NULL; }
    //if ( empty( $form_state['values']['weekend_pref3'] ) )  { $form_state['values']['weekend_pref3'] = NULL; }
  
    if ( $form_state['values']['application_id'] == '' ) {
      //update any remaining fields
      drupal_write_record('emmaus_team_applications', $form_state['values'] );
      drupal_set_message(t('Thank you for applying to work a team on an upcoming weekend in the @community_name. An email address will be sent as confirmation of this choice.', array('@community_name'=>variable_get('emmaus_community_name') ) ) );
    } else {
      drupal_write_record('emmaus_team_applications', $form_state['values'], 'application_id');
      drupal_set_message( t('Application was updated.' ) );    
    }
  
    //drupal_set_message( '$form_state[\'values\']<pre>' . print_r($form_state['values'], TRUE) . '</pre>'   );   
  
    //Send an email
    module_load_include('inc', 'emmaus_team_apply', 'emmaus_team_apply.notify');
    $message = _emmaus_team_email_new_application($form_state['values']);    
    //drupal_mail($module, $key, $to, $language, $params = array(), $from = NULL, $send = TRUE)
    $mail_result = drupal_mail('emmaus_team', 'new_app', $message['recipients'] , language_default(), $message  );
    //drupal_set_message('Mail Result: <pre>' . print_r($mail_result, TRUE) . '</pre>' );

    //Redirect the form
    if ($form_state['values']['action'] != 'admin') {
      $form_state['redirect'] = 'user/' .$form_state['values']['profile_id'] .'/edit/applications' ;      
    }
  }

}





